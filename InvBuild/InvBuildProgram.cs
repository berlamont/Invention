﻿/*! 9 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public static class BuildProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 800;
        Inv.WpfShell.DefaultWindowHeight = 1000;
        Inv.WpfShell.Run(A =>
        {
          var BuildProcedure = new InventionProcedure();

          new BuildApplication(A).Launch(BuildProcedure);
        });
      });
    }
  }

  public sealed class InventionProcedure : Inv.Mimic<Inv.BuildScript>
  {
    public InventionProcedure()
    {
      this.Base = new BuildScript("Invention Build");

      const string InvMountPath = @"C:\Development\Forge\Inv\";
      const string InvManualPath = InvMountPath + @"InvManual\";
      const string InvDeploymentPath = @"C:\Deployment\Inv\";

      var InvSolution = Base.SelectSolution(InvMountPath + "Inv.sln");
      var InvManualTimestamp = Base.SelectTimestamp(InvManualPath + @"InvManual\Resources\Texts\Version.txt");
      var InvManualProject = Base.SelectProject(InvManualPath + @"InvManual\InvManual.csproj");
      var InvManualATarget = InvSolution.SelectTarget("Manual", "InvManualA");
      var InvManualITarget = InvSolution.SelectTarget("Manual", "InvManualI");
      var InvManualUTarget = InvSolution.SelectTarget("Manual", "InvManualU");
      var InvManualAProject = Base.SelectProject(InvManualPath + @"InvManualA\InvManualA.csproj");
      var InvManualIProject = Base.SelectProject(InvManualPath + @"InvManualI\InvManualI.csproj");
      var InvManualUProject = Base.SelectProject(InvManualPath + @"InvManualU\InvManualU.csproj");
      var InvPlayTarget = InvSolution.SelectTarget("Tool", "InvPlay");
      var InvGenTarget = InvSolution.SelectTarget("Tool", "InvGen");
      var InvExtensionTarget = InvSolution.SelectTarget("Extension", "InvExtension");
      var InvExtensionProject = Base.SelectProject(InvMountPath + @"InvExtension\InvExtension.csproj");
      var InvLibraryProject = Base.SelectProject(InvMountPath + @"InvLibrary\InvLibrary.csproj");
      var InvPlatformProject = Base.SelectProject(InvMountPath + @"InvPlatform\InvPlatform.csproj");
      var InvPlatformAProject = Base.SelectProject(InvMountPath + @"InvPlatformA\InvPlatformA.csproj");
      var InvPlatformIProject = Base.SelectProject(InvMountPath + @"InvPlatformI\InvPlatformI.csproj");
      var InvPlatformUProject = Base.SelectProject(InvMountPath + @"InvPlatformU\InvPlatformU.csproj");
      var InvPlatformSProject = Base.SelectProject(InvMountPath + @"InvPlatformS\InvPlatformS.csproj");
      var InvPlatformWProject = Base.SelectProject(InvMountPath + @"InvPlatformW\InvPlatformW.csproj");
      var InvMacServer = Base.SelectMacServer("Calmac", "callanh", "");
      var InvNugetVersion = Base.SelectNugetVersion(InvMountPath + "InvNugetVersion.txt");
      var InvManualAndroidManifest = InvManualAProject.SelectAndroidManifest();
      var InvManualiOSManifest = InvManualIProject.SelectiOSManifest();
      var InvManualUwaManifest = InvManualUProject.SelectUwaManifest();
      var InvExtensionManifest = InvExtensionProject.SelectVsixManifest();

      var InvManualProcedure = Base.AddProcedure("Invention Manual");

      InvManualProcedure.AddTask("Android", T =>
      {
        T.CleanAndroidTarget(InvManualATarget);
        T.UpdateTimestamp('a', InvManualTimestamp);
        T.GenerateResources(InvManualProject);
        T.UpdateAndroidVersion(InvManualAndroidManifest);
        T.BuildAndroidTarget(InvManualATarget);
        T.SignAndroidProject(InvManualAProject);
        T.DeployAndroidPackage(InvManualPath + @"InvManualA\bin\Release\com.x10host.invention.manual-Signed.apk", InvDeploymentPath + "Invention.Manual", InvManualAndroidManifest);
      });

      InvManualProcedure.AddTask("iOS", T =>
      {
        T.CleaniOSTarget(InvManualITarget, InvMacServer);
        T.UpdateTimestamp('i', InvManualTimestamp);
        T.GenerateResources(InvManualProject);
        T.UpdateiOSVersion(InvManualiOSManifest);
        T.BuildiOSTarget(InvManualITarget, InvMacServer);
        T.DeployiOSPackage(InvManualPath + @"InvManualI\bin\iPhone\Ad-Hoc\InvManualI", InvDeploymentPath + "Invention.Manual", InvManualiOSManifest);
      });

      InvManualProcedure.AddTask("Uwa", T =>
      {
        T.CleanUwaTarget(InvManualUTarget);
        T.UpdateTimestamp('u', InvManualTimestamp);
        T.GenerateResources(InvManualProject);
        T.UpdateUwaVersion(InvManualUwaManifest);
        T.BuildUwaTarget(InvManualUTarget);
        T.DeployUwaPackage(InvManualPath + @"InvManualU\AppPackages\InvManualU", InvDeploymentPath + "Invention.Manual", InvManualUwaManifest);
      });

      var InvNugetProcedure = Base.AddProcedure("Invention Nuget");

      InvNugetProcedure.AddTask("Prep", T =>
      {
        T.CleanTarget(InvGenTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.BuildTarget(InvGenTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.CleanTarget(InvPlayTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.BuildTarget(InvPlayTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.UpdateNugetVersion(InvNugetVersion);
      });

      InvNugetProcedure.AddTask("Pack", T =>
      {
        T.PackNugetProject(InvLibraryProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.PackNugetProject(InvPlatformProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.PackNugetProject(InvPlatformWProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.PackNugetProject(InvPlatformAProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.PackNugetProject(InvPlatformIProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU, InvMacServer);
        T.PackNugetProject(InvPlatformUProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.x86);
        //T.MoveFile(InvDeploymentPath + "Inv.PlatformU.1.0.0.nupkg", InvDeploymentPath + "Invention.Platform.U.{0}.nupkg");
        T.PackNugetProject(InvPlatformSProject, InvDeploymentPath, InvNugetVersion, BuildConfiguration.Release, BuildPlatform.AnyCPU);
      });

      InvNugetProcedure.AddTask("Push", T =>
      {
        T.PushNugetPackage(InvDeploymentPath, "Invention.Library", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.W", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.A", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.I", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.U", InvNugetVersion);
        T.PushNugetPackage(InvDeploymentPath, "Invention.Platform.S", InvNugetVersion);
      });

      var InvExtensionProcedure = Base.AddProcedure("Invention Extension");

      InvExtensionProcedure.AddTask("Vsix", T =>
      {
        T.CleanTarget(InvExtensionTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.UpdateVsixVersion(InvExtensionManifest);
        T.BuildTarget(InvExtensionTarget, BuildConfiguration.Release, BuildPlatform.AnyCPU);
        T.DeployVsixFile(InvMountPath + @"InvExtension\bin\Release\Invention.Extension.vsix", InvDeploymentPath + "Invention.Extension", InvExtensionManifest);
      });

      // TODO: instructions to manually upload to VisualStudioGallery website?
    }
  }
}
