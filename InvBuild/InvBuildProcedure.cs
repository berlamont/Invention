﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class BuildScript
  {
    public BuildScript(string Title)
    {
      this.Title = Title;
      this.ScriptList = new DistinctList<BuildProcedure>();
      this.ProcedureSet = new HashSet<BuildProcedure>();
      this.TaskSet = new HashSet<BuildTask>();
      this.StepSet = new HashSet<BuildStep>();
    }

    public string Title { get; private set; }
    public HashSet<BuildProcedure> ProcedureSet { get; private set; }
    public HashSet<BuildTask> TaskSet { get; private set; }
    public HashSet<BuildStep> StepSet { get; private set; }

    public BuildProcedure AddProcedure(string Name)
    {
      var Result = new BuildProcedure(Name);

      ScriptList.Add(Result);

      return Result;
    }
    public IEnumerable<BuildProcedure> GetProcedures()
    {
      return ScriptList;
    }
    public BuildSolution SelectSolution(string FilePath)
    {
      return new BuildSolution(FilePath);
    }
    public BuildProject SelectProject(string FilePath)
    {
      return new BuildProject(FilePath);
    }
    public BuildTimestamp SelectTimestamp(string FilePath)
    {
      return new BuildTimestamp(FilePath);
    }
    public BuildNugetVersion SelectNugetVersion(string FilePath)
    {
      return new BuildNugetVersion(FilePath);
    }
    public BuildMacServer SelectMacServer(string Address, string Username, string Password)
    {
      return new BuildMacServer(Address, Username, Password);
    }

    private Inv.DistinctList<BuildProcedure> ScriptList;
  }

  public sealed class BuildSolution
  {
    internal BuildSolution(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".sln", StringComparison.InvariantCultureIgnoreCase), "Solution file must be a .sln: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Solution file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string FilePath { get; set; }

    public BuildTarget SelectTarget(params string[] PathArray)
    {
      return new BuildTarget(this, PathArray);
    }
  }

  public sealed class BuildTarget
  {
    internal BuildTarget(BuildSolution Solution, string[] PathArray)
    {
      this.Solution = Solution;
      this.PathArray = PathArray;
    }

    public BuildSolution Solution { get; private set; }
    public string[] PathArray { get; private set; }

    public BuildTarget SelectTarget(params string[] PathArray)
    {
      return new BuildTarget(Solution, this.PathArray.Union(PathArray).ToArray());
    }
  }

  public sealed class BuildProject
  {
    internal BuildProject(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".csproj", StringComparison.InvariantCultureIgnoreCase), "Project file must be a .csproj: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Project file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string FilePath { get; set; }

    public BuildAndroidManifest SelectAndroidManifest()
    {
      return new BuildAndroidManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Properties", "AndroidManifest.xml"));
    }
    public BuildiOSManifest SelectiOSManifest()
    {
      return new BuildiOSManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Info.plist"));
    }
    public BuildUwaManifest SelectUwaManifest()
    {
      return new BuildUwaManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Package.appxmanifest"));
    }
    public BuildVsixManifest SelectVsixManifest()
    {
      return new BuildVsixManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "source.extension.vsixmanifest"));
    }
  }

  public enum BuildConfiguration
  {
    Debug,
    Release,
    Adhoc
  }

  public enum BuildPlatform
  {
    x86,
    x64,
    AnyCPU,
    iPhone
  }

  public sealed class BuildMacServer
  {
    internal BuildMacServer(string Address, string Username, string Password)
    {
      this.Address = Address;
      this.Username = Username;
      this.Password = Password;
    }

    public string Address { get; private set; }
    public string Username { get; private set; }
    public string Password { get; private set; }
  }

  public sealed class BuildTimestamp
  {
    internal BuildTimestamp(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".txt", StringComparison.InvariantCultureIgnoreCase), "Timestamp file must be a .txt: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Timestamp file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public char Prefix { get; internal set; }
    public string Version { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildNugetVersion
  {
    internal BuildNugetVersion(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".txt", StringComparison.InvariantCultureIgnoreCase), "Nuget version file must be a .txt: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Nuget version file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Text { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildAndroidManifest
  {
    internal BuildAndroidManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".xml", StringComparison.InvariantCultureIgnoreCase), "Android manifest must be a .xml: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Android manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildiOSManifest
  {
    internal BuildiOSManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".plist", StringComparison.InvariantCultureIgnoreCase), "iOS manifest must be a .plist: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "iOS manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildUwaManifest
  {
    internal BuildUwaManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".appxmanifest", StringComparison.InvariantCultureIgnoreCase), "Uwa manifest must be a .appxmanifest: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Uwa manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildVsixManifest
  {
    internal BuildVsixManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".vsixmanifest", StringComparison.InvariantCultureIgnoreCase), "Vsix manifest must be a .vsixmanifest: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Vsix manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; private set; }
  }

  public sealed class BuildProcedure
  {
    internal BuildProcedure(string Name)
    {
      this.Name = Name;
      this.TaskList = new DistinctList<BuildTask>();
    }

    public string Name { get; private set; }

    public BuildTask AddTask(string Name, Action<BuildTask> Action = null)
    {
      var Result = new BuildTask(Name);

      TaskList.Add(Result);

      if (Action != null)
        Action(Result);

      return Result;
    }

    internal IEnumerable<BuildTask> GetTasks()
    {
      return TaskList;
    }

    private Inv.DistinctList<BuildTask> TaskList;
  }

  public sealed class BuildTask
  {
    internal BuildTask(string Name)
    {
      this.Name = Name;
      this.StepList = new DistinctList<BuildStep>();
    }

    public string Name { get; private set; }

    public void UpdateTimestamp(char WritePrefix, BuildTimestamp Timestamp)
    {
      AddStep("Update Timestamp", WritePrefix + " yyyy.MMdd.HHmm", Context =>
      {
        Timestamp.Prefix = WritePrefix;
        Timestamp.Version = DateTime.Now.ToString("yyyy.MMdd.HHmm");

        if (System.IO.File.Exists(Timestamp.FilePath))
          Inv.Support.FileHelper.SetReadOnly(Timestamp.FilePath, false);

        System.IO.File.WriteAllText(Timestamp.FilePath, Timestamp.Prefix + Timestamp.Version);
      });
    }
    public void UpdateNugetVersion(BuildNugetVersion Version)
    {
      AddStep("Update Version", System.IO.Path.GetFileName(Version.FilePath), Context =>
      {
        Version.Text = System.IO.File.ReadAllText(Version.FilePath).Trim();

        var VersionArray = Version.Text.Split('.');
        var VersionCode = VersionArray[VersionArray.Length - 1];

        int VersionNumber;
        if (int.TryParse(VersionCode, out VersionNumber))
        {
          VersionNumber++;
          VersionArray[VersionArray.Length - 1] = VersionNumber.ToString();
          Version.Text = VersionArray.AsSeparatedText(".");

          Inv.Support.FileHelper.SetReadOnly(Version.FilePath, false);
          System.IO.File.WriteAllText(Version.FilePath, Version.Text);
        }
      });
    }
    public void CleanTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Clean Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Build Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void GenerateResources(BuildProject Project)
    {
      AddStep("Generate Resources", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        var Engine = new Inv.GenEngine();
        Engine.Execute(Project.FilePath);
      });
    }
    public void CleanAndroidTarget(BuildTarget Target)
    {
      AddStep("Clean Android Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), GetArgument(BuildPlatform.AnyCPU), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildAndroidTarget(BuildTarget Target)
    {
      AddStep("Build Android Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), GetArgument(BuildPlatform.AnyCPU), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void UpdateAndroidVersion(BuildAndroidManifest Manifest)
    {
      AddStep("Update Android Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var XmlDocument = new Inv.XmlDocument();
        XmlDocument.ReadFromFile(Manifest.FilePath);
        var XmlRoot = XmlDocument.AccessRoot("manifest", "");

        Manifest.Version = XmlRoot.AccessAttribute("android", "versionCode");

        int VersionCode;
        if (int.TryParse(Manifest.Version, out VersionCode))
        {
          VersionCode++;

          Manifest.Version = VersionCode.ToString();
          XmlRoot.UpdateAttribute("android", "versionCode", Manifest.Version);

          Inv.Support.FileHelper.SetReadOnly(Manifest.FilePath, false);
          XmlDocument.WriteToFile(Manifest.FilePath);
        }
      });
    }
    public void SignAndroidProject(BuildProject Project)
    {
      AddStep("Sign Android Project", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:SignAndroidPackage", Project.FilePath, GetArgument(BuildConfiguration.Release), GetArgument(BuildPlatform.AnyCPU).StripWhitespace());

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployAndroidPackage(string SourcePath, string TargetPath, BuildAndroidManifest Manifest)
    {
      AddStep("Deploy Android Apk", TargetPath + "-v.apk", Context =>
      {
        var AndroidZipAlignArguments =
          string.Format("-f -v 4 \"{0}\" \"{1}\"", SourcePath, TargetPath + "-" + Manifest.Version + ".apk");

        ExecuteProcess(Context, AndroidZipAlignPath, AndroidZipAlignArguments);
      });
    }
    public void UpdateiOSVersion(BuildiOSManifest Manifest)
    {
      AddStep("Update iOS Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var XmlDocument = new Inv.XmlDocument();
        XmlDocument.ReadFromFile(Manifest.FilePath);

        var XmlRoot = XmlDocument.AccessRoot("plist", "");

        var XmlDict = XmlRoot.AccessChildElement("dict").Single();

        var FoundVersion = false;
        var FoundElement = (XmlElement)null;

        foreach (var ChildElement in XmlDict.GetChildElements())
        {
          if (FoundVersion)
          {
            if (ChildElement.LocalName != "string")
              throw new Exception("Version string not found after key element.");

            FoundElement = ChildElement;
            break;
          }

          if (ChildElement.LocalName == "key" && ChildElement.Content == "CFBundleVersion")
            FoundVersion = true;
        }

        if (FoundElement == null)
          throw new Exception("CFBundleVersion was not found.");

        Manifest.Version = FoundElement.Content;

        int VersionCode;
        if (int.TryParse(FoundElement.Content, out VersionCode))
        {
          VersionCode++;
          Manifest.Version = VersionCode.ToString();

          FoundElement.Content = Manifest.Version;

          Inv.Support.FileHelper.SetReadOnly(Manifest.FilePath, false);
          XmlDocument.WriteToFile(Manifest.FilePath);
        }
      });
    }
    public void CleaniOSTarget(BuildTarget Target, BuildMacServer MacServer)
    {
      AddStep("Clean iOS Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean /p:ServerAddress={4};ServerUser={5};ServerPassword={6}",
          Target.Solution.FilePath, GetArgument(BuildConfiguration.Adhoc), GetArgument(BuildPlatform.iPhone), Target.PathArray.AsSeparatedText(@"\"), MacServer.Address, MacServer.Username, MacServer.Password);

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildiOSTarget(BuildTarget Target, BuildMacServer MacServer)
    {
      AddStep("Build iOS Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3} /p:ServerAddress={4};ServerUser={5};ServerPassword={6}",
          Target.Solution.FilePath, GetArgument(BuildConfiguration.Adhoc), GetArgument(BuildPlatform.iPhone), Target.PathArray.AsSeparatedText(@"\"), MacServer.Address, MacServer.Username, MacServer.Password);

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployiOSPackage(string SourcePath, string TargetPath, BuildiOSManifest Manifest)
    {
      AddStep("Deploy iOS Package", TargetPath + "-v.ipa", Context =>
      {
        System.IO.File.Copy(SourcePath + "-" + Manifest.Version + ".ipa", TargetPath + "-" + Manifest.Version + ".ipa", true);
      });
    }
    public void UpdateUwaVersion(BuildUwaManifest Manifest)
    {
      AddStep("Update Uwa Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var XmlDocument = new Inv.XmlDocument();
        XmlDocument.ReadFromFile(Manifest.FilePath);
        var XmlRoot = XmlDocument.AccessRoot("Package", "http://schemas.microsoft.com/appx/manifest/foundation/windows10");
        var IdentityXmlElement = XmlRoot.AccessChildElement("Identity").Single();

        Manifest.Version = IdentityXmlElement.AccessAttribute("Version");

        var VersionArray = Manifest.Version.Split('.');
        var VersionCode = VersionArray[VersionArray.Length - 2]; // major.minor.build.revision
        int VersionNumber;
        if (int.TryParse(VersionCode, out VersionNumber))
        {
          VersionNumber++;
          VersionArray[VersionArray.Length - 2] = VersionNumber.ToString();

          Manifest.Version = VersionArray.AsSeparatedText(".");
          IdentityXmlElement.UpdateAttribute("Version", Manifest.Version);

          Inv.Support.FileHelper.SetReadOnly(Manifest.FilePath, false);
          XmlDocument.WriteToFile(Manifest.FilePath);
        }
      });
    }
    public void CleanUwaTarget(BuildTarget Target)
    {
      AddStep("Clean Uwa Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        foreach (var CleanPlatform in new[] { "x86", "x64", "ARM" })
        {
          var MSBuildArguments =
            string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean",
            Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), CleanPlatform, Target.PathArray.AsSeparatedText(@"\"));

          ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
        }
      });
    }
    public void BuildUwaTarget(BuildTarget Target)
    {
      AddStep("Build Uwa Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};AppxBundle=Always;AppxBundlePlatforms=\"x86|x64|ARM\";BuildAppxUploadPackageForUap=true /t:{2}",
          Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployUwaPackage(string SourcePath, string TargetPath, BuildUwaManifest Manifest)
    {
      AddStep("Deploy Uwa Package", TargetPath + "-v.appxupload", Context =>
      {
        System.IO.File.Copy(SourcePath + "_" + Manifest.Version + "_x86_x64_ARM_bundle.appxupload", TargetPath + "-" + Manifest.Version + ".appxupload", true);
      });
    }
    public void CleanWpfTarget(BuildTarget Target)
    {
      AddStep("Clean Wpf Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), GetArgument(BuildPlatform.AnyCPU), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildWpfTarget(BuildTarget Target)
    {
      AddStep("Build Wpf Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(BuildConfiguration.Release), GetArgument(BuildPlatform.AnyCPU), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void PackNugetProject(BuildProject Project, string DeploymentPath, BuildNugetVersion Version, BuildConfiguration Configuration, BuildPlatform Platform, BuildMacServer MacServer = null)
    {
      AddStep("Pack Nuget Project", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        // set the version in assemblyinfo.
        var AssemblyInfoPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "Properties", "AssemblyInfo.cs");
        var AssemblyInfoTimestamp = System.IO.File.GetLastWriteTimeUtc(AssemblyInfoPath);
        var AssemblyInfoOriginalArray = System.IO.File.ReadAllLines(AssemblyInfoPath);
        var AssemblyInfoUpdateArray = (string[])AssemblyInfoOriginalArray.Clone();

        for (var AssemblyInfoIndex = 0; AssemblyInfoIndex < AssemblyInfoOriginalArray.Length; AssemblyInfoIndex++)
        {
          var AssemblyInfoLine = AssemblyInfoOriginalArray[AssemblyInfoIndex].Trim();

          if (AssemblyInfoLine.StartsWith("[assembly: AssemblyVersion") || AssemblyInfoLine.StartsWith("[assembly: AssemblyFileVersion") || AssemblyInfoLine.StartsWith("[assembly: AssemblyInformationalVersion"))
          {
            var AssemblyInfoTextStart = AssemblyInfoLine.IndexOf('"') + 1;
            var AssemblyInfoTextEnd = AssemblyInfoLine.IndexOf('"', AssemblyInfoTextStart);

            if (AssemblyInfoTextStart < AssemblyInfoTextEnd)
              AssemblyInfoUpdateArray[AssemblyInfoIndex] = AssemblyInfoLine.Remove(AssemblyInfoTextStart, AssemblyInfoTextEnd - AssemblyInfoTextStart).Insert(AssemblyInfoTextStart, Version.Text);
          }
        }

        Inv.Support.FileHelper.SetReadOnly(AssemblyInfoPath, false);
        System.IO.File.WriteAllLines(AssemblyInfoPath, AssemblyInfoUpdateArray);

        // build the project.
        var NugetMacServer = MacServer == null ? "" : string.Format(";ServerAddress={0};ServerUser={1};ServerPassword={2}", MacServer.Address, MacServer.Username, MacServer.Password);
        var NugetArguments =
          string.Format("pack \"{0}\" -OutputDirectory \"{1}\" -Build -Properties InventionVersion={2};Configuration={3};Platform={4};Constants=\"NUGET\"{5}", Project.FilePath, DeploymentPath.ExcludeAfter(System.IO.Path.DirectorySeparatorChar.ToString()), Version.Text, GetArgument(Configuration), GetArgument(Platform).StripWhitespace(), NugetMacServer);
        ExecuteProcess(Context, NugetPath, NugetArguments);

        // revert the assemblyinfo.cs
        System.IO.File.WriteAllLines(AssemblyInfoPath, AssemblyInfoOriginalArray);
        System.IO.File.SetLastWriteTimeUtc(AssemblyInfoPath, AssemblyInfoTimestamp);
        Inv.Support.FileHelper.SetReadOnly(AssemblyInfoPath, true);
      });
    }
    public void PushNugetPackage(string DeploymentPath, string PackageName, BuildNugetVersion Version)
    {
      AddStep("Push Nuget Package", PackageName, Context =>
      {
        var NugetArguments =
          string.Format("push \"{0}{1}.{2}.nupkg\" -Source https://www.nuget.org/api/v2/package", DeploymentPath, PackageName, Version.Text);

        ExecuteProcess(Context, NugetPath, NugetArguments);
      });
    }
    public void UpdateVsixVersion(BuildVsixManifest Manifest)
    {
      AddStep("Update Vsix Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var XmlDocument = new Inv.XmlDocument();
        XmlDocument.ReadFromFile(Manifest.FilePath);
        var XmlRoot = XmlDocument.AccessRoot("PackageManifest", "http://schemas.microsoft.com/developer/vsx-schema/2011");
        var MetadataXmlElement = XmlRoot.AccessChildElement("Metadata").Single();
        var IdentityXmlElement = MetadataXmlElement.AccessChildElement("Identity").Single();
        Manifest.Version = IdentityXmlElement.AccessAttribute("Version");

        var VersionArray = Manifest.Version.Split('.');
        var VersionCode = VersionArray[VersionArray.Length - 1];
        int VersionNumber;
        if (int.TryParse(VersionCode, out VersionNumber))
        {
          VersionNumber++;
          VersionArray[VersionArray.Length - 1] = VersionNumber.ToString();

          Manifest.Version = VersionArray.AsSeparatedText(".");
          IdentityXmlElement.UpdateAttribute("Version", Manifest.Version);

          Inv.Support.FileHelper.SetReadOnly(Manifest.FilePath, false);
          XmlDocument.WriteToFile(Manifest.FilePath);
        }
      });
    }
    public void DeployVsixFile(string SourcePath, string TargetPath, BuildVsixManifest Manifest)
    {
      AddStep("Deploy Vsix File", TargetPath + "-v.vsix", Context =>
      {
        System.IO.File.Copy(SourcePath, TargetPath + "-" + Manifest.Version + ".vsix", true);
      });
    }
    public void RunProgram(string FilePath, Func<string> ArgumentFunc = null)
    {
      AddStep("Run Program", System.IO.Path.GetFileName(FilePath), Context =>
      {
        ExecuteProcess(Context, FilePath, ArgumentFunc != null ? (ArgumentFunc() ?? "") : "");
      });
    }
    public void DeleteFile(string FileName, Func<string> FileFunc = null)
    {
      AddStep("Delete File", System.IO.Path.GetFileName(FileName), Context =>
      {
        var FilePath = FileFunc != null ? FileFunc() : FileName;

        if (System.IO.File.Exists(FilePath))
          System.IO.File.Delete(FilePath);
      });
    }
    public void CopyFile(string FileName, Func<string> SourceFunc, Func<string> TargetFunc)
    {
      AddStep("Copy File", System.IO.Path.GetFileName(FileName), Context =>
      {
        var SourceFilePath = SourceFunc();
        var TargetFilePath = TargetFunc();

        if (System.IO.File.Exists(SourceFilePath))
          System.IO.File.Copy(SourceFilePath, TargetFilePath, true);
      });
    }
    public void WriteTextFile(string FilePath, Func<string> ArgumentFunc)
    {
      AddStep("Write Text File", System.IO.Path.GetFileName(FilePath), Context =>
      {
        System.IO.File.WriteAllText(FilePath, ArgumentFunc != null ? (ArgumentFunc() ?? "") : "");
      });
    }
    public void Sleep(TimeSpan TimeSpan)
    {
      AddStep("Sleep", TimeSpan.FormatTimeSpanShort(), Context =>
      {
        System.Threading.Thread.Sleep(TimeSpan);
      });
    }

    internal IEnumerable<BuildStep> GetSteps()
    {
      return StepList;
    }

    private void ExecuteProcess(BuildContext Context, string Command, string Arguments)
    {
      Context.Log("EXECUTE: \"" + Command + "\" " + Arguments);

      var ProcessStart = new System.Diagnostics.ProcessStartInfo();
      ProcessStart.FileName = Command;
      ProcessStart.Arguments = Arguments;
      ProcessStart.RedirectStandardOutput = true;
      ProcessStart.RedirectStandardError = true;
      ProcessStart.UseShellExecute = false;
      ProcessStart.CreateNoWindow = true;
      //ProcessStart.EnvironmentVariables["VSSDK140Install"] = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VSSDK\";
      //ProcessStart.EnvironmentVariables["VS140COMNTOOLS"] = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\";
      //ProcessStart.EnvironmentVariables["GYP_MSVS_VERSION"] = "2015";
      ProcessStart.EnvironmentVariables["VisualStudioVersion"] = "15.0";
      ProcessStart.EnvironmentVariables["MSBuildExtensionsPath"] = @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild"; 

      System.Diagnostics.Process ProcessResult;
      try
      {
        ProcessResult = System.Diagnostics.Process.Start(ProcessStart);
      }
      catch (Exception Exception)
      {
        throw new Exception("Unable to execute process: \"" + Command + "\" " + Arguments, Exception);
      }

      var StreamLine = "";

      while (!ProcessResult.StandardOutput.EndOfStream)
      {
        if (Context.IsCancelled)
        {
          ProcessResult.Kill();
          return;
        }

        StreamLine = ProcessResult.StandardOutput.ReadLine().Trim();

        Context.Log(StreamLine);
      }

      while (!ProcessResult.WaitForExit(500))
      {
        if (Context.IsCancelled)
        {
          ProcessResult.Kill();
          return;
        }
      }

      if (ProcessResult.ExitCode != 0)
      {
        while (!ProcessResult.StandardError.EndOfStream)
        {
          StreamLine = ProcessResult.StandardError.ReadLine().Trim();
          Context.Log(StreamLine);
        }

        Context.Fail();
      }
    }
    private BuildStep AddStep(string Name, string Description, Action<BuildContext> Action)
    {
      var Result = new BuildStep(Name, Description, Action);

      StepList.Add(Result);

      return Result;
    }
    private string GetArgument(BuildConfiguration Configuration)
    {
      switch (Configuration)
      {
        case BuildConfiguration.Adhoc:
          return "Ad-hoc";

        case BuildConfiguration.Debug:
          return "Debug";

        case BuildConfiguration.Release:
          return "Release";

        default:
          throw new Exception("BuildConfiguration not handled: " + Configuration);
      }
    }
    private string GetArgument(BuildPlatform Platform)
    {
      switch (Platform)
      {
        case BuildPlatform.AnyCPU:
          return "\"Any CPU\"";

        case BuildPlatform.x86:
          return "x86";

        case BuildPlatform.x64:
          return "x64";

        case BuildPlatform.iPhone:
          return "iPhone";

        default:
          throw new Exception("BuildPlatform not handled: " + Platform);
      }
    }

    private Inv.DistinctList<BuildStep> StepList;
    private const string NugetPath = @"C:\Kestral\Nuget.exe";
    private const string MSBuildPath = @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe"; // MSBUILD 2017
    private const string AndroidZipAlignPath = @"C:\Program Files (x86)\Android\android-sdk\build-tools\23.0.2\zipalign.exe";
  }

  public sealed class BuildStep
  {
    internal BuildStep(string Name, string Description, Action<BuildContext> Action)
    {
      this.Name = Name;
      this.Description = Description;
      this.Action = Action;
    }

    public string Name { get; private set; }
    public string Description { get; private set; }
    public Action<BuildContext> Action { get; private set; }
  }

  public sealed class BuildContext
  {
    internal BuildContext()
    {
      this.LogBuilder = new StringBuilder();
    }

    public void Log(string Line)
    {
      LogBuilder.AppendLine(Line);

      if (LogEvent != null)
        LogEvent(Line);
    }

    public bool IsCancelled
    {
      get { return IsInterrupted || IsFailed || Exception != null; }
    }
    public bool IsFailed { get; private set; }
    public bool IsInterrupted { get; private set; }
    public Exception Exception { get; private set; }

    internal event Action<string> LogEvent;

    internal void Clear()
    {
      LogBuilder.Clear();
    }
    internal void Interupt()
    {
      this.IsInterrupted = true;
    }
    internal void Fail()
    {
      this.IsFailed = true;
    }
    internal void Caught(Exception Exception)
    {
      this.Exception = Exception;
    }
    internal string GetLog()
    {
      return LogBuilder.ToString();
    }
    internal bool HasLog()
    {
      return LogBuilder.Length > 0;
    }

    private StringBuilder LogBuilder;
  }
}
