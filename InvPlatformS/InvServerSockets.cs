﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Inv.Support;

namespace Inv
{
  internal sealed class ServerQueue
  {
    internal ServerQueue(string Name, ServerConnection Connection)
    {
      this.Name = Name;
      this.Connection = Connection;
      
      this.SendCriticalSection = new ExclusiveCriticalSection("ServerQueue-Send" + Name);
      this.SendReadyWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
      this.SendQueue = new Queue<ServerPacket>();
      this.SendTask = new Task(SendPackets);

      this.ReceiveCriticalSection = new ExclusiveCriticalSection("ServerQueue-Receive" + Name);
      this.ReceiveReadyWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
      this.ReceiveQueue = new Queue<ServerPacket>();
      this.ReceiveTask = new Task(ReceivePackets);
    }

    public ulong SentBytes { get; private set; }
    public ulong ReceivedBytes { get; private set; }

    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        ReceiveTask.Start();
        SendTask.Start();
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        CancelTasks();

        ReceiveTask.Wait();
        SendTask.Wait();
      }
    }

    public void SendPacket(ServerPacket Packet)
    {
      using (SendCriticalSection.Lock())
      {
        SendQueue.Enqueue(Packet);

        SendReadyWaitHandle.Set();
      }
    }
    public ServerPacket ReceivePacket()
    {
      if (IsActive && ReceiveReadyWaitHandle.WaitOne())
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadyWaitHandle.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }
    public ServerPacket TryReceivePacket()
    {
      if (IsActive && ReceiveReadyWaitHandle.WaitOne(0))
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadyWaitHandle.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }

    private void ReceivePackets()
    {
      while (IsActive)
      {
        var Packet = Connection.TryReceivePacket();

        if (Packet == null)
        {
          CancelTasks();
        }
        else
        {
          using (ReceiveCriticalSection.Lock())
          {
            ReceiveQueue.Enqueue(Packet);

            ReceiveReadyWaitHandle.Set();
          }

          ReceivedBytes += (uint)Packet.Buffer.Length;
        }
      }
    }
    private void SendPackets()
    {
      while (IsActive)
      {
        while (SendReadyWaitHandle.WaitOne() && IsActive)
        {
          ServerPacket Result;

          using (SendCriticalSection.Lock())
          {
            Result = SendQueue.Dequeue();

            if (SendQueue.Count > 0)
              SendReadyWaitHandle.Set();
          }

          if (!Connection.TrySendPacket(Result))
            IsActive = false;
          else
            SentBytes += (uint)Result.Buffer.Length;
        }
      }
    }
    private void CancelTasks()
    {
      this.IsActive = false;

      ReceiveReadyWaitHandle.Set(); // allow receivepackets to shut down.
      SendReadyWaitHandle.Set(); // allow sendpackets to shutdown.
    }

    private ServerConnection Connection;
    private ExclusiveCriticalSection SendCriticalSection;
    private EventWaitHandle SendReadyWaitHandle;
    private Queue<ServerPacket> SendQueue;
    private Task SendTask;
    private ExclusiveCriticalSection ReceiveCriticalSection;
    private EventWaitHandle ReceiveReadyWaitHandle;
    private Queue<ServerPacket> ReceiveQueue;
    private Task ReceiveTask;
    private bool IsActive;
    private string Name;
  }

  internal sealed class ServerException : Exception
  {
    public ServerException(string Message)
      : base(Message)
    {
    }
    public ServerException(Exception Exception)
      : base(Exception.Message)
    {
      this.Type = Exception.GetType().FullName;
    }
    public ServerException(string Type, string Message, string Source, string StackTrace)
      : base(Message)
    {
      this.Type = Type;
      this.SourceField = Source;
      this.StackTraceField = StackTrace;
    }

    public string Type { get; private set; }
    public override string Source
    {
      get { return SourceField; }
    }
    public override string StackTrace
    {
      get { return StackTraceField; }
    }

    private string SourceField;
    private string StackTraceField;
  }

  internal sealed class ServerConnection
  {
    internal ServerConnection(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    public System.IO.Stream InputStream { get; private set; }
    public System.IO.Stream OutputStream { get; private set; }

    public void SendPacket(ServerPacket RoutePacket)
    {
      if (!TrySendPacket(RoutePacket))
        throw new ServerException("Send connection lost.");
    }
    public bool TrySendPacket(ServerPacket RoutePacket)
    {
      var Buffer = RoutePacket.Buffer;
      return WriteByteArray(Buffer, 0, Buffer.Length);
    }
    public ServerPacket ReceivePacket()
    {
      var Result = TryReceivePacket();

      if (Result == null)
        throw new ServerException("Receive connection lost.");

      return Result;
    }
    public ServerPacket TryReceivePacket()
    {
      // TODO: more efficient buffered socket reading?

      var DataBuffer = new byte[4];

      if (!ReadByteArray(DataBuffer, 0, 4))
        return null;

      var DataLength = BitConverter.ToInt32(DataBuffer, 0);

      if (DataLength > 0)
      {
        Array.Resize(ref DataBuffer, DataLength);

        if (!ReadByteArray(DataBuffer, 4, DataLength))
          return null;

        return new ServerPacket(DataBuffer);
      }
      else
      {
        return null;
      }
    }

    [DebuggerNonUserCode]
    private bool WriteByteArray(byte[] Buffer, int Offset, int Length)
    {
      try
      {
        OutputStream.Write(Buffer, Offset, Length);
      }
      catch
      {
        return false;
      }

      return true;
    }
    [DebuggerNonUserCode]
    private bool ReadByteArray(byte[] Buffer, int Offset, int Length)
    {
      // NOTE: don't break on exception in here as socket disconnections are expected.
      //       downstream code must cope with ReceivePacket returning null.
      try
      {
        var Index = Offset;
        while (Index < Length)
        {
          var Actual = InputStream.Read(Buffer, Index, Length - Index);

          if (Actual == 0)
            return false;

          Index += Actual;
        }
      }
      catch
      {
        return false;
      }

      return true;
    }
  }

  internal sealed class ServerPacket
  {
    public ServerPacket(byte[] Buffer)
    {
      this.Buffer = Buffer;
    }

    public byte[] Buffer { get; private set; }

    public ServerReader ToReader()
    {
      return new ServerReader(this);
    }
  }

  internal sealed class ServerWriter : Inv.Mimic<CompactWriter>, IDisposable
  {
    public ServerWriter()
    {
      this.MemoryStream = new MemoryStream();

      this.Base = new CompactWriter(MemoryStream);

      Base.WriteInt32(0); // reserve four bytes for the packet length.
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public long Size
    {
      get { return MemoryStream.Position - 4; }
    }

    public void Reset()
    {
      MemoryStream.Position = 0;
      MemoryStream.SetLength(0);
      Base.WriteInt32(0);
    }
    public ServerPacket ToPacket()
    {
      MemoryStream.Position = 0;
      Base.WriteInt32((int)MemoryStream.Length);

      var Result = MemoryStream.ToArray();

      Debug.Assert(Result.Length == MemoryStream.Length);

      return new ServerPacket(Result);
    }
    public void WriteMessage(ClientMessage Message)
    {
      Base.WriteMessage(Message);
    }

    private MemoryStream MemoryStream;
  }

  internal sealed class ServerReader : Inv.Mimic<CompactReader>, IDisposable
  {
    public ServerReader(ServerPacket Packet)
    {
      this.MemoryStream = new MemoryStream(Packet.Buffer);
      this.Base = new CompactReader(MemoryStream);

      var PacketLength = Base.ReadInt32();
      if (PacketLength != Packet.Buffer.Length)
        throw new Exception("Packet length is invalid.");
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public bool EndOfPacket
    {
      get { return MemoryStream.Position >= MemoryStream.Length; }
    }

    public void Read(Action<CompactReader> ReadAction)
    {
      ReadAction(this);
    }
    public ServerMessage ReadServerMessage()
    {
      return Base.ReadServerMessage();
    }
    public ClientMessage ReadClientMessage()
    {
      return Base.ReadClientMessage();
    }
    internal long ReadClientVersion()
    {
      return Base.ReadInt64();
    }
    public Guid ReadClientIdentity()
    {
      return Base.ReadGuid();
    }

    private MemoryStream MemoryStream;
  }
}
