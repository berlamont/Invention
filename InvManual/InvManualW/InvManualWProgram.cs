﻿/*! 9 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Manual
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.iPad_Mini;
        Inv.WpfShell.DeviceEmulationRotated = false;
        Inv.WpfShell.DeviceEmulationFramed = false;
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 1920;
        Inv.WpfShell.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(Inv.Manual.Shell.Install);
      });
    }
  }
}