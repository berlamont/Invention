﻿/*! 4 !*/
using Inv.Support;

namespace Inv.Manual
{
  internal static class Theme
  {
    public static readonly Inv.Colour NavigateGutterColour = Inv.Colour.DimGray.Opacity(0.25F);
    public const int NavigateGutterSize = 5;

    public static readonly Inv.Colour CodeFontColour = Inv.Colour.White;
    public const int CodeFontSize = 14;
    public static string CodeFontName { get; set; }

    public static readonly Inv.Colour HyperlinkFontColour = Inv.Colour.SpringGreen;
    public const int HyperlinkFontSize = 14;
    public static string HyperlinkFontName { get; set; }

    public static readonly Inv.Colour SubjectColour = Inv.Colour.DeepSkyBlue;
    public static readonly Inv.Colour SheetColour = Inv.Colour.DimGray.Darken(0.50F);
    public static readonly Inv.Colour KeyColour = Inv.Colour.HotPink.Darken(0.25F);

    public const int DocumentGap = 20;
    public const int DocumentFontSize = 20;
    public const Inv.FontWeight DocumentFontWeight = Inv.FontWeight.Light;
    public static readonly Inv.Colour DocumentFontColour = Inv.Colour.WhiteSmoke;

    public const int NavigateWidth = 160;

    public const int ControlWidth = 140;
    public const int ControlGap = 1;
    public static readonly Inv.Colour ControlSelectColour = Inv.Colour.DodgerBlue;
    public static readonly Inv.Colour ControlNormalColour = Inv.Colour.DimGray;

    public const int BookGap = 10;
    public const int BookSize = 200;
    public const int BookPadding = 20;
    public const int BookCorner = 2;
    public const int BookFontSize = 24;

    public const int TopicGap = 5;
    public const int TopicCorner = 2;
    public const int TopicPadding = 10;
    public const int TopicSize = 200;

    public static Inv.Button NewLaunchButton(this Surface Surface)
    {
      return new TapButton(Surface, TapType.Launch);
    }
    public static Inv.Button NewBackButton(this Surface Surface)
    {
      return new TapButton(Surface, TapType.Back);
    }
    public static Inv.Button NewNextButton(this Surface Surface)
    {
      return new TapButton(Surface, TapType.Next);
    }
    public static Inv.Button NewEscapeButton(this Surface Surface)
    {
      return new TapButton(Surface, TapType.Escape);
    }
    public static Inv.Button NewSearchButton(this Surface Surface)
    {
      return new TapButton(Surface, TapType.Escape);
    }
  }
}