/*! 7 !*/

namespace Inv.Manual
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvManual.InvResourcePackage.rs");
    }

    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
    public static readonly ResourcesTexts Texts;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>0.8 KB</Summary>
    public readonly global::Inv.Image ClipboardWhite;
    ///<Summary>41.5 KB</Summary>
    public readonly global::Inv.Image Logo;
    ///<Summary>1.8 KB</Summary>
    public readonly global::Inv.Image SearchBlack;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Image SearchWhite;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>4.0 KB</Summary>
    public readonly global::Inv.Sound BackTap;
    ///<Summary>3.3 KB</Summary>
    public readonly global::Inv.Sound EscapeTap;
    ///<Summary>1.9 KB</Summary>
    public readonly global::Inv.Sound LaunchTap;
    ///<Summary>4.9 KB</Summary>
    public readonly global::Inv.Sound NextTap;
  }

  public sealed class ResourcesTexts
  {
    public ResourcesTexts() { }

    ///<Summary>0.3 KB</Summary>
    public readonly string Updates;
    ///<Summary>0.0 KB</Summary>
    public readonly string Version;
  }
}