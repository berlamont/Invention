﻿/*! 29 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Manual
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      var App = new Application(Application);
      App.Title = "Invention Manual";
      App.BuildEvent += () =>
      {
        Theme.CodeFontName = Application.Device.MonospacedFontName;
        Theme.HyperlinkFontName = Application.Device.MonospacedFontName;

        var DevEmail = new Uri("mailto:hodgskin.callan@gmail.com");
        var InvGitlab = new Uri("https://gitlab.com/hodgskin-callan/Invention");
        const string FakeAustralianMobileNumber = "+61 491 570 156";

        App.Introduction.Document.AddParagraph("Invention is how you build cross-platform, native apps with a single C# codebase.");
        App.Introduction.Document.AddBreak();
        App.Introduction.Document.AddParagraph("This app is an interactive manual for learning how to develop apps in Invention. The topics can be read in sequence by tapping the pink button > in the bottom right corner.");

        App.Conclusion.Document.AddParagraph("You have reached the end of the " + Application.Title + ". Thank you for reading and if you have any feedback please feel free to contact by email.");
        App.Conclusion.Document.AddBreak();
        App.Conclusion.Document.AddHyperlink(DevEmail);

        App.About.Document.AddParagraph("Invention is an open source project started by Callan Hodgskin.");
        App.About.Document.AddHyperlink(DevEmail);
        App.About.Document.AddHyperlink(InvGitlab);
        App.About.Document.AddBreak();
        App.About.Document.AddParagraph("With special thanks to Kestral for significant contributions to this project.");
        App.About.Document.AddHyperlink(new Uri("http://www.kestral.com.au"));
        App.About.Document.AddBreak();
        App.About.Document.AddParagraph("Pathos is a dungeon adventure game and the first app built on Invention.");
        App.About.Document.AddHyperlink(new Uri("https://pathos.azurewebsites.net"));
        App.About.Document.AddBreak();
        App.About.Document.AddParagraph("The Invention logo was adapted from an icon by Chameleon Design and is attributed under Creative Commons.");

        var DarkColourArray = new[]
        {
          Inv.Colour.DodgerBlue,
          Inv.Colour.DarkSlateBlue,
          Inv.Colour.HotPink,
          Inv.Colour.DarkSlateGray,
          Inv.Colour.Purple,
          Inv.Colour.DarkOrange,
          Inv.Colour.DarkGreen,
          Inv.Colour.DarkRed,
          Inv.Colour.SteelBlue,
          Inv.Colour.Black,
        };

        var TheoryBook = App.AddBook("Theory", B =>
        {
          B.Document.AddParagraph("The following is a primer to introduce the key concepts of Invention.");

          #region Overview.
          var OverviewTopic = B.AddTopic(1, "Overview", T =>
          {
            T.Document.AddParagraph("Invention is an open source project started by Callan Hodgskin in 2015. With Invention you can develop cross-platform native apps with 100% code sharing using Visual Studio and C#.NET.");
            T.Document.AddBreak();
            T.Document.AddBulletList
            (
              "Code-first API with no XML files",
              "Layout is responsive",
              "Compiles to a native app",
              "Rapid development in Visual Studio",
              "Live editing with Roslyn"
            );
            T.Document.AddBreak();
            T.Document.AddParagraph("Write your app code in portable class libraries (PCLs) using the Invention API. Your app code runs consistently on each platform without additional effort. Write your app once and deliver to the major app stores.");

            if (App.IsIOS && (App.IsAndroid || App.IsUniversalWindows))
            {
              T.Document.AddBreak();
              T.Document.AddBulletList
              (
                App.IsAndroid ? "Google Play (Android)" : null,
                App.IsIOS ? "Apple iTunes (iOS)" : null,
                App.IsUniversalWindows ? "Windows Store (WinRT)" : null
              );
            }
          });
          #endregion

          #region Environment.
          var EnvironmentTopic = B.AddTopic(1, "Environment", T =>
          {
            T.Document.AddParagraph("The Invention stack is free for non-commercial development thanks to Microsoft. To get started you will need Microsoft Visual Studio and Xamarin installed. Invention is supported on Visual Studio 2013 Community Edition and above.");
            T.Document.AddBreak();
            T.Document.AddParagraph("It is strongly recommended to have access to at least one device of each platform. A combination of desktop, tablet and phone is recommended.");

            if (App.IsIOS)
            {
              T.Document.AddBreak();
              T.Document.AddParagraph("Please note that to target iOS you will also need a Mac. This is a required part of the Xamarin iOS build-chain.");
            }

            if (App.IsWindowsDesktop)
            {
              T.Document.AddBreak();
              T.Document.AddParagraph("You can rapidly develop your app using the Windows Desktop platform. This platform is based on the Windows Presentation Foundation (WPF) and is fast to compile and run in Visual Studio. There are modes to emulate devices so you can easily refine the experience across all form-factors.");
            }

            T.Document.AddBreak();
            T.Document.AddParagraph("Invention is installed via the NuGet package manager:");
            T.Document.AddBreak();
            T.Document.AddCode(
              "Invention.Library    // Foundation Classes",
              "Invention.Platform   // Portable App",
              App.IsIOS ? "Invention.Platform.I // iOS App" : null,
              App.IsAndroid ? "Invention.Platform.A // Android App" : null,
              App.IsWindowsDesktop ? "Invention.Platform.W // Windows Desktop App" : null,
              App.IsUniversalWindows ? "Invention.Platform.U // Windows Store App" : null,
              "Invention.Platform.S // Server App",
              "");
            T.Document.AddBreak();
            T.Document.AddParagraph("Invention is also available from the public GitLab repository:");
            T.Document.AddBreak();
            T.Document.AddHyperlink(new Uri("https://gitlab.com/hodgskin-callan/Invention"));
          });
          #endregion

          #region Layout.
          var LayoutTopic = B.AddTopic(1, "Layout", T =>
          {
            T.Document.AddParagraph("User interface can be thought as an arrangement of rectangular panels. There are panels for layout such as stacks, docks and overlays. There are panels for function such as buttons, labels, graphics, edits and memos. There is even a panel for custom rendering at 60 fps. All panels have a background, border, corner, margin, padding, size, alignment, elevation, opacity and visibility.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Invention maps these panels to the appropriate native control on each platform. Default styling is stripped from each platform and you have full artistic control of your app. Invention also consolidates common services across the platforms.");
            T.Document.AddBreak();
            T.Document.AddParagraph("For code sharing and reuse, we need user interface that is composable. This means composite panels can be placed anywhere in the visual tree and will behave in a consist way.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Writing for multiple devices and orientations requires user interface that is arrangeable. This means we create our shared panels and simply rearrange them as the device dimensions are changed.");
          });
          #endregion

          #region CodeFirstAPI.
          var CodeFirstAPITopic = B.AddTopic(1, "Code-First API", T =>
          {
            T.Document.AddParagraph("The Invention API is designed for programmers. There are no XML files or requirement to use the Model-View-Controller pattern.");
            T.Document.AddBreak();
            T.Document.AddParagraph("The APIs are discoverable using the code-complete function in Visual Studio. The public members of each class are minimal because they are staged instead of flattened. This is best explained by comparing the WPF TextBlock to the Inv Label.");
            T.Document.AddBreak();
            T.Document.AddCode(
    @"WpfTextBlock.FontFamily = new FontFamily(""Consolas"");
WpfTextBlock.FontSize = 20;
WpfTextBlock.FontWeight = FontWeights.Bold;
WpfTextBlock.Foreground = Brushes.Black;

InvLabel.Font.Name = Application.Device.MonospacedFontName;
InvLabel.Font.Size = 20;
InvLabel.Font.Bold();
InvLabel.Font.Colour = Colour.Black;");
            T.Document.AddBreak();
            T.Document.AddParagraph("WPF TextBlock has all the members at the top level, whereas Inv Label has a .Font level where all the font members can be accessed. Due to this API design, when you code-complete on a WPF TextBlock there are a thousand members in the search list.");
          });
          #endregion

          #region HelloWorld.
          var HelloWorldTopic = B.AddTopic(1, "Hello World", T =>
          {
            T.Document.AddParagraph("This is a 'Hello World' app that runs on Windows (WPF):");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Inv.WpfShell.Run(Application =>
{
  var Surface = Application.Window.NewSurface();
  Surface.Background.Set(Colour.WhiteSmoke);

  var Label = Surface.NewLabel();
  Surface.Content = Label;
  Label.Alignment.Center();
  Label.Font.Size = 20;
  Label.Text = ""Hello World"";

  Surface.Transition();
});");
            T.Document.AddBreak();
            T.Document.AddParagraph("This is what you would see when the app is run:");
            T.Document.AddBreak();

            var X = T.Document.AddPreview().SetExampleLabel();
            X.Background.Set(null);
            X.Alignment.Center();
            X.Font.Size = 20;
            X.Font.Colour = Inv.Colour.Black;
            X.Text = "Hello World";
          });
          #endregion

          #region Colours
          var ColourTopic = B.AddTopic(1, "Colour", T =>
          {
            T.Document.AddParagraph("Invention supports 32 bit ARGB colours. There are " + Inv.Colour.List.Count + " named colours equivalent to the WPF declarations.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Colours can be darkened or lightened with a percentage:");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Colour.SteelBlue.Darken(0.50F);
Colour.SteelBlue.Lighten(0.50F);");
            T.Document.AddBreak();
            T.Document.AddParagraph("The alpha channel can also be adjusted with a percentage:");
            T.Document.AddBreak();
            T.Document.AddCode("Colour.SteelBlue.Opacity(0.50F);");
            T.Document.AddBreak();
            T.Document.AddParagraph("Custom colours can be declared with explicit ARGB parameters:");
            T.Document.AddBreak();
            T.Document.AddCode("Colour.FromArgb(0xFF, 0x33, 0x44, 0x55);");

            var PalettePanel = T.AddPalettePanel();
            PalettePanel.SelectEvent += (C) =>
            {
              var FlyoutPanel = T.Dialog.NewFlyoutPanel();

              Inv.Surface Surface = T;

              var ColourDock = Surface.NewVerticalDock();
              FlyoutPanel.Content = ColourDock;
              ColourDock.Alignment.Center();
              ColourDock.Background.Colour = Inv.Colour.White;
              ColourDock.Padding.Set(20);
              ColourDock.Elevation.Set(10);

              var NameLabel = Surface.NewLabel();
              ColourDock.AddHeader(NameLabel);
              NameLabel.Alignment.BottomStretch();
              NameLabel.JustifyCenter();
              NameLabel.Font.Size = 30;
              NameLabel.Font.Weight = Theme.DocumentFontWeight;
              NameLabel.Font.Colour = Inv.Colour.Black;
              NameLabel.LineWrapping = false;
              NameLabel.Text = C.Name.PascalCaseToTitleCase();

              var HexDataLabel = Surface.NewLabel();
              ColourDock.AddFooter(HexDataLabel);
              HexDataLabel.Alignment.TopStretch();
              HexDataLabel.JustifyCenter();
              HexDataLabel.Font.Name = Theme.CodeFontName;
              HexDataLabel.Font.Size = 25;
              HexDataLabel.Font.Colour = Inv.Colour.Black;
              HexDataLabel.LineWrapping = false;
              HexDataLabel.Text = C.Hex;

              var HexHeadLabel = Surface.NewLabel();
              ColourDock.AddFooter(HexHeadLabel);
              HexHeadLabel.Alignment.TopStretch();
              HexHeadLabel.JustifyCenter();
              HexHeadLabel.Font.Name = Theme.CodeFontName;
              HexHeadLabel.Font.Size = 25;
              HexHeadLabel.Font.Colour = Inv.Colour.LightGray;
              HexHeadLabel.LineWrapping = false;
              HexHeadLabel.Text = "aa rr gg bb";

              var ColourFrame = Surface.NewFrame();
              ColourDock.AddClient(ColourFrame);
              ColourFrame.Border.Set(1);
              ColourFrame.Border.Colour = Inv.Colour.Black;
              ColourFrame.Background.Colour = C;
              ColourFrame.Margin.Set(20);
              ColourFrame.Size.Set(256, 256);

              FlyoutPanel.Show();
            };
          });
          #endregion

          #region Device Points.
          var DPTopic = B.AddTopic(1, "Device Points", T =>
          {
            T.Document.AddParagraph("User interface design for multiple devices needs to consider the different form-factors and orientations. In Windows development we are typically thinking in terms of pixels. However, when designing user interface, the physical size of a device is far more important that the pixel resolution. These days there are 9” tablets with more pixels than some 24” monitors. When laying out a user interface we need to use a measurement unit that corresponds to physical size.");
            T.Document.AddBreak();
            T.Document.AddParagraph(
              "Invention uses device points (dp) as a density-independent unit of measurement. " + 
              "One device point is ~1/160th of an inch on smart devices. " +
              "In Windows, one device point is exactly one logical pixel. This logical pixel is determined by the resolution of a screen and the display scaling factor (eg. 150%).");
            T.Document.AddBreak();
            T.Document.AddParagraph("Font size is also measured in device points. The point size is almost equal to the sum of the ascent and descent of a particular font.");
          });
          #endregion

          #region InventionPlay.
          var LiveEditingTopic = B.AddTopic(3, "Live Editing", T =>
          {
            T.Document.AddParagraph("Invention Play is a live runtime environment based on the Rosyln compiler. This means your code editor and app are side-by-side and instantly compile and re-run when you save a file. This is perfect for rapid prototyping and layout of user interface. The elimination of the manual compile-and-run step can be a significant improvement for your productivity.");
            T.Document.AddBreak();
            T.Document.AddParagraph("This feature can be access by installing the Invention Extension from the Visual Studio Gallery.");
            T.Document.AddBreak();
            T.Document.AddHyperlink(new Uri("https://marketplace.visualstudio.com/items?itemName=CallanHodgskin.InventionExtension"));
            T.Document.AddBreak();
            T.Document.AddParagraph("Once this extension is installed you can access with right click on your Portable app and select Invention Play. Please note you will need to provide an entry point static class in your portable project. E.g.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"namespace MyProject
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      // application logic goes here.
    }
  }
}");
          });
          #endregion

          #region Resource Packages
          var ResourcePackageTopic = B.AddTopic(1, "Resource Packages", T =>
          {
            T.Document.AddParagraph("Resource packages are much like the Visual Studio .resx files except they can be used inside portable projects. These packages are used for organising file resources such as text documents, png images and mp3 sound effects.");
            T.Document.AddBreak();
            T.Document.AddParagraph("A resource package can be easily added to your portable project if you have installed the Invention Extension from the Visual Studio Gallery. Right click on your project and select 'Add | New Item'. Find the Invention Resource Package item template and give it a name. This will create a subfolder called Resources with three files. Subfolders can also be used to organise your resources.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"MyProject.InvResourcePackage
MyProject.InvResourcePackage.cs
MyProject.InvResourcePackage.rs");
            T.Document.AddBreak();
            T.Document.AddParagraph("The .InvResourcePackage is a blank text file but you can override with your preferred namespace for the generated classes. The .cs and .rs files are automatically generated when you build your project. You can access the resources from the code generated static Resources class. For example:");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Label = Surface.NewLabel();
Label.Text = Resources.Texts.Credits;

var Graphic = Surface.NewGraphic();
Graphic.Image = Resources.Images.MyLogo;

Application.Audio.Play(Resources.Sounds.Click);");
            T.Document.AddBreak();
            T.Document.AddParagraph("");
          });
          #endregion
        });

        var APIBook = App.AddBook("Features", B =>
        {
          B.Document.AddParagraph("The following are core features which encapsulate the major aspects of app development.");

          #region Application
          var ApplicationTopic = B.AddTopic(1, "Application", T =>
          {
            T.Document.AddParagraph("This is the root API for the application features. For example:");
            T.Document.AddBreak();
            T.Document.AddCode(string.Format(
@"Application.Title = ""My Project"";

Application.Window.Background.Colour = Colour.Black;

Application.Directory.RootFolder.NewFile(""Bookmark.txt"").AsText().WriteAll(""Hello World"");

Application.Audio.Play(Resources.Sounds.Click);

Application.Phone.SMS(""{0}"");", FakeAustralianMobileNumber));
            T.Document.AddBreak();
            T.Document.AddParagraph("There are several lifetime events that can be handled:");
            T.Document.AddBreak();
            T.Document.AddBulletList
            (
              "StartEvent is guaranteed to be run the first time the app is started.",
              "StopEvent is intended to run when the app is terminated but this is not guaranteed on all platforms.",
              "SuspendEvent is run when the app switches from foreground to background.",
              "ResumeEvent is run when the app switches from background to foreground.",
              "ExitQuery is only supported on Windows Desktop and allows you to ask for confirmation when the window is closed."
            );
            T.Document.AddBreak();
            T.Document.AddCode(
@"Application.StartEvent += () => { };
Application.StopEvent += () => { };
Application.SuspendEvent += () => { };
Application.ResumeEvent += () => { };
Application.ExitQuery += () => true;");
            T.Document.AddBreak();
            T.Document.AddParagraph("The recommended practice is to place your lifetime logic in Start, Suspend and Resume. Stop and ExitQuery are not guaranteed to run on all platforms and scenerios.");
            T.Document.AddBreak();
            T.Document.AddParagraph("There is also an Exit method which terminates the app on Windows Desktop. On smart devices it will only put the app into the background.");
            T.Document.AddBreak();
            T.Document.AddParagraph("For brevity, the following feature topics omit the Application reference in the examples.");
          });          
          #endregion

          #region Window.
          var WindowTopic = B.AddTopic(2, "Window", T =>
          {
            T.Document.AddParagraph("This is the primary API for user interface. The window represents the entire screen of your device and can be used to create surfaces for layout.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Surface = Window.NewSurface();");
            T.Document.AddBreak();
            T.Document.AddParagraph("The surface is a layout for the entire screen of your device. The application can have many surfaces and transition between using effects such as carousel and cross-fade.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Surface.Transition().Fade();");
            T.Document.AddBreak();
            T.Document.AddParagraph("The surface creates new panels which are owned. This means the panels cannot be placed on other surfaces.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Label = Surface.NewLabel();
Surface.Content = Label;");
            T.Document.AddBreak();
            T.Document.AddParagraph("The arrange event is fired when the form-factor of the surface changes. E.g. rotation of a device of resizing of a window such as split-screen in iOS.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Surface.ArrangeEvent += () =>;
{
  if (Window.Width < Window.Height)
  {
    // portrait
  }
  else
  {
    // landscape
  }
}");
            T.Document.AddBreak();
            T.Document.AddParagraph("The compose event is fired once per display frame. This means the event is called up to 60 times per second (60 FPS) or every ~16 milliseconds. This is used for custom animations or game-loops.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Surface.ComposeEvent += () =>;
{
  // custom animations.
};");
            T.Document.AddBreak();
          });
          #endregion

          #region Directory.
          var DirectoryTopic = B.AddTopic(1, "Directory", T =>
          {
            T.Document.AddParagraph("This API manages files and folders in local storage. This is used for local persistent state and settings.");
            T.Document.AddBreak();
            T.Document.AddParagraph("There is a single root folder and allows a single level of named subfolders.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var BookmarkFile = Directory.RootFolder.NewFile(""Bookmark.txt"");
BookmarkFile.AsText().WriteAll(""Features > Directory"");

var LogsFolder = Directory.NewFolder(""Logs"");
var TodayLogFile = LogsFolder.NewFile(Inv.Date.Now + "".log"");
using (var TodayLogWriter = TodayLogFile.Create())
  TodayLogWriter.WriteLine(""Starting the log file ..."");");
          });
          #endregion

          #region Audio
          var AudioTopic = B.AddTopic(1, "Audio", T =>
          {
            T.Document.AddParagraph("Invention can play mp3 sound effects. The volume (loudness) of the playback can be adjusted as a percentage between 0 and 1. The rate of playback (pitch) can also be adjusted as a percentage between 0.5 (twice as slow) and 2.0 (twice as fast).");
            T.Document.AddBreak();
            T.Document.AddParagraph("A technique to add variance to otherwise repetitive sound effects is called pitch shifting. Simply by increasing or decreasing the rate of playback by 6% can make a big difference to the sound experience. Pitch shifting can also be used to achieve a crescendo effect from a single sound file.");

            Inv.Surface Surface = T;

            var X = Surface.NewButton();
            T.AddPanel(X);
            X.Corner.Set(2);
            X.Padding.Set(10);
            X.Background.Colour = Inv.Colour.OrangeRed;

            var Label = Surface.NewLabel();
            X.Content = Label;
            Label.JustifyCenter();
            Label.Font.Size = 20;
            Label.Font.Colour = Inv.Colour.White;
            Label.Text = "PLAY SOUND EFFECT";

            var Code = T.SetCodePanel();

            var Sounds = Resources.Sounds;
            var FileArray = new[]
            {
              new { Name = "Plop", Sound = Sounds.LaunchTap },
              new { Name = "Click", Sound = Sounds.NextTap },
              new { Name = "Clunk", Sound = Sounds.BackTap },
              new { Name = "Whip", Sound = Sounds.EscapeTap },
            };

            var PlayFile = FileArray[0];
            var PlayVolume = 1.0F;
            var PlayRate = 1.0F;
            var PlayVariance = false;
            var PlayLooped = false;
            var PlayRandom = new Random();
            var AudioClip = (Inv.AudioClip)null;

            Action<Action> UpdatePlay = (A) =>
            {
              if (A != null)
              {
                A();

                if (AudioClip != null)
                {
                  AudioClip.Stop();
                  AudioClip = null;
                }

                AudioClip = Application.Audio.NewClip(PlayFile.Sound, PlayVolume, PlayRate + (PlayVariance ? PlayRandom.Next(-6, +6) / 100.0F : 0.0F), PlayLooped);
                AudioClip.Play();
              }

              Code.Text = string.Format("Audio.Play({0}, {1:F2}F, {2:F2}F);", PlayFile.Name, PlayVolume, PlayRate);
            };

            X.SingleTapEvent += () => UpdatePlay(() => { });

            UpdatePlay(null);

            var SoundPanel = T.AddControlPanel("File");

            foreach (var File in FileArray)
            {
              var SoundButton = SoundPanel.AddButton(File.Name);
              SoundButton.SelectEvent += () => UpdatePlay(() => PlayFile = File);
              SoundButton.SelectQuery += () => PlayFile.Sound == File.Sound;
            }

            var VolumePanel = T.AddControlPanel("Volume");

            foreach (var Volume in new[] { 1.0F, 0.75F, 0.50F, 0.25F, 0.00F })
            {
              var VolumeButton = VolumePanel.AddButton("vol. " + (Volume * 100F).ToString() + "%");
              VolumeButton.SelectEvent += () => UpdatePlay(() => PlayVolume = Volume);
              VolumeButton.SelectQuery += () => PlayVolume == Volume;
            }

            var RatePanel = T.AddControlPanel("Rate");

            foreach (var Rate in new[] { 1.0F, 0.95F, 0.75F, 0.50F, 0.25F, 2.0F, 1.75F, 1.5F, 1.25F, 1.05F })
            {
              var RateButton = RatePanel.AddButton("rate " + (Rate * 100F).ToString() + "%");
              RateButton.SelectEvent += () => UpdatePlay(() => PlayRate = Rate);
              RateButton.SelectQuery += () => PlayRate == Rate;
            }

            var VariancePanel = T.AddControlPanel("Variance");
            var InvariantButton = VariancePanel.AddButton("Pitch Invariant");
            InvariantButton.SelectEvent += () => UpdatePlay(() => PlayVariance = false);
            InvariantButton.SelectQuery += () => PlayVariance == false;

            var VariantButton = VariancePanel.AddButton("Pitch Shift" + Environment.NewLine + "-6% .. +6%");
            VariantButton.SelectEvent += () => UpdatePlay(() => PlayVariance = true);
            VariantButton.SelectQuery += () => PlayVariance == true;

            var CustomRate = (float?)null;

            var CustomRateButton = VariancePanel.AddButton("Custom Rate" + Environment.NewLine + (CustomRate * 100F).ToString() + "%");
            CustomRateButton.SelectEvent += () =>
            {
              var CustomFlyout = T.Dialog.NewFlyoutPanel();

              var CustomDock = Surface.NewVerticalDock();
              CustomFlyout.Content = CustomDock;
              CustomDock.Border.Set(30);
              CustomDock.Border.Colour = Inv.Colour.DarkGray;
              CustomDock.Padding.Set(10);
              CustomDock.Background.Colour = Inv.Colour.WhiteSmoke;
              CustomDock.Alignment.Center();
              CustomDock.Elevation.Set(4);

              var CustomLabel = Surface.NewLabel();
              CustomDock.AddHeader(CustomLabel);
              CustomLabel.Padding.Set(4);
              CustomLabel.Font.Size = 18;
              CustomLabel.Font.Colour = Inv.Colour.Black;
              CustomLabel.Font.Weight = Theme.DocumentFontWeight;
              CustomLabel.Text = "Custom Rate (eg. 105.5%)";

              var CustomEdit = Surface.NewDecimalEdit();
              CustomDock.AddClient(CustomEdit);
              CustomEdit.Border.Set(2);
              CustomEdit.Border.Colour = Inv.Colour.DarkGray;
              CustomEdit.Background.Colour = Inv.Colour.White;
              CustomEdit.Padding.Set(4);
              CustomEdit.Font.Size = 36;
              CustomEdit.ReturnEvent += () =>
              {
                CustomFlyout.Hide();

                float EditRate;
                if (CustomEdit.Text != null && float.TryParse(CustomEdit.Text.Strip('%'), out EditRate))
                {
                  CustomRate = EditRate / 100F;
                  UpdatePlay(() => PlayRate = CustomRate.Value);
                }
                else
                {
                  CustomRate = null;
                  UpdatePlay(() => PlayRate = 1.00F);
                }

                CustomRateButton.Text = "Custom Rate" + Environment.NewLine + (CustomRate * 100F).ToString() + "%";

                T.Refresh();
              };

              var CustomButton = Surface.NewButton();
              CustomDock.AddFooter(CustomButton);
              CustomButton.Background.Colour = Theme.ControlSelectColour;
              CustomButton.Margin.Set(0, 10, 0, 0);
              CustomButton.Padding.Set(10);
              CustomButton.SingleTapEvent += () =>
              {
                CustomEdit.Return();
              };

              var CustomSelectLabel = Surface.NewLabel();
              CustomButton.Content = CustomSelectLabel;
              CustomSelectLabel.JustifyCenter();
              CustomSelectLabel.Font.Size = 16;
              CustomSelectLabel.Font.Colour = Inv.Colour.White;
              CustomSelectLabel.Font.Weight = Theme.DocumentFontWeight;
              CustomSelectLabel.Text = "ACCEPT";

              Surface.SetFocus(CustomEdit);
              CustomFlyout.Show();
            };
            CustomRateButton.SelectQuery += () => PlayRate == CustomRate;

            var LoopedPanel = T.AddControlPanel("Looped");
            var NotLoopedButton = VariancePanel.AddButton("Play Once");
            NotLoopedButton.SelectEvent += () => UpdatePlay(() => PlayLooped = false);
            NotLoopedButton.SelectQuery += () => PlayLooped == false;

            var LoopedButton = VariancePanel.AddButton("Play Looped");
            LoopedButton.SelectEvent += () => UpdatePlay(() => PlayLooped = true);
            LoopedButton.SelectQuery += () => PlayLooped == true;
          });
          #endregion

          #region Clipboard.
          var ClipboardTopic = B.AddTopic(1, "Clipboard", T =>
          {
            T.Document.AddParagraph("Copy and paste from the shared application clipboard.");
            T.Document.AddBreak();

            var Preview = T.SetPreviewPanel();

            var Code = T.AddCodePanel();
            
            var Label = Preview.SetExampleLabel();
            Label.Text = "Copy me to the clipboard!";

            var ManagePanel = T.AddControlPanel("Manage");

            var CopyButton = ManagePanel.AddButton("Copy");
            CopyButton.SelectEvent += () =>
            {
              Application.Clipboard.Text = Label.Text;
              Code.Text = "Clipboard.Text = Label.Text;";
            };

            var PasteButton = ManagePanel.AddButton("Paste");
            PasteButton.SelectEvent += () =>
            {
              Label.Text = Application.Clipboard.Text;
              Code.Text = "Label.Text = Clipboard.Text;";
            };
          });
          #endregion

          #region Calendar.
          var CalendarTopic = B.AddTopic(1, "Calendar", T =>
          {
            T.Document.AddParagraph("Access the time zone for this device and invoke platform-specific pickers for date and time. The following method returns the time zone name as configured on the device:");
            T.Document.AddBreak();
            T.Document.AddCode(@"Calendar.GetTimeZoneName(); // " + Application.Calendar.GetTimeZoneName() + "\r\n");
            T.Document.AddBreak();
            T.Document.AddParagraph("The calendar is used to launch platform-specfiic date and time pickers. There are three pickers: Date, Time and Date/Time. SelectEvent is run when the user selects a new date/time and CancelEvent when the picker is dismissed.");
            T.Document.AddBreak();

            var Preview = T.SetPreviewPanel();
            var Label = Preview.SetExampleLabel();

            var CodePanel = T.AddCodePanel();
            CodePanel.Text =
@"var Picker = Calendar.NewDateTimePicker();
Picker.Value = DateTime.Now;
Picker.SelectEvent += () => { };
Picker.CancelEvent += () => { };
Picker.Show();";

            var DemoValue = DateTime.Now;

            Action<DateTime> UpdateAction = (V) =>
            {
              DemoValue = V;
              Label.Text = DemoValue.AsDate().ToString() + Environment.NewLine + DemoValue.AsTime().ToString();
            };

            var ManagePanel = T.AddControlPanel("Manage");

            var DateButton = ManagePanel.AddButton("Date");
            DateButton.SelectEvent += () =>
            {
              var Picker = Application.Calendar.NewDatePicker();
              Picker.Value = DemoValue;
              Picker.SelectEvent += () => UpdateAction(Picker.Value);
              Picker.Show();
            };

            var TimeButton = ManagePanel.AddButton("Time");
            TimeButton.SelectEvent += () =>
            {
              var Picker = Application.Calendar.NewTimePicker();
              Picker.Value = DemoValue;
              Picker.SelectEvent += () => UpdateAction(Picker.Value);
              Picker.Show();
            };

            var DateTimeButton = ManagePanel.AddButton("Date/Time");
            DateTimeButton.SelectEvent += () =>
            {
              var Picker = Application.Calendar.NewDateTimePicker();
              Picker.Value = DemoValue;
              Picker.SelectEvent += () => UpdateAction(Picker.Value);
              Picker.Show();
            };

            UpdateAction(DemoValue);
          });
          #endregion

          #region Email.
          var EmailTopic = B.AddTopic(1, "Email", T =>
          {
            T.Document.AddParagraph("Start an email message in the default mail app.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Message = Email.NewMessage();
Message.To(""Callan Hodgskin"", ""hodgskin.callan@gmail.com"");
Message.Attach(""Fault Log"", Directory.RootFolder.NewFile(""Fault.log""));
Message.Subject = ""My Project Fault Report"";
Message.Body = ""<Please provide any clarification>"";

if (Message.Send())
{
  // we know the user was prompted to send
  // but not if the email was actually sent
}
else
{
  // we know the user cancelled
  // and the email was not sent
}");

            var EmailPanel = T.AddControlPanel("Email");

            var SendEmailButton = EmailPanel.AddButton("Send email");
            SendEmailButton.SelectEvent += () =>
            {
              var Message = Application.Email.NewMessage();
              Message.Subject = "Invention Manual Feedback";
              Message.To("Callan Hodgskin", "hodgskin.callan@gmail.com");
              Message.Body = "<Please offer any feedback or suggestions>";
              Message.Send();
            };
          });
          #endregion

          #region Phone.
          var PhoneTopic = B.AddTopic(1, "Phone", T =>
          {
            T.Document.AddParagraph("Dial a phone number or start sending a text message. The mobile phone number used below is a fake Australian mobile number thanks to:");
            T.Document.AddBreak();
            T.Document.AddHyperlink(new Uri("https://fakenumber.org/australia/mobile"));
            T.Document.AddBreak();
            T.Document.AddCode(string.Format(
@"Phone.Dial(""{0}"");

Phone.SMS(""{0}"");", FakeAustralianMobileNumber));
            T.Document.AddBreak();
            if (!Application.Phone.IsSupported)
              T.Document.AddParagraph("NOTE: Phone API is not supported on this device.");

            var TestPanel = T.AddControlPanel("Test");

            var SMSButton = TestPanel.AddButton("SMS");
            SMSButton.SelectEvent += () => Application.Phone.SMS(FakeAustralianMobileNumber);

            var DialButton = TestPanel.AddButton("Dial");
            DialButton.SelectEvent += () => Application.Phone.Dial(FakeAustralianMobileNumber);
          });
          #endregion

          #region Location.
          var LocationTopic = B.AddTopic(2, "Location", T =>
          {
            T.Document.AddParagraph("Reverse geocoding is the lookup of a location by a global position expressed in latitude and longitude. Latitude is the angular distance of a place north or south of the earth's equator. Longitude is the angular distance of a place east or west of the Greenwich meridian. In code, south of the equator and east of the Greenwich meridian is expressed as a negative number.");
            T.Document.AddBreak();

            // TODO: global position changes API needs further development before release.
            /*
                        T.Document.AddParagraph("Suscribe to the ChangeEvent to observe to changes to your position.");
                        T.Document.AddBreak();
                        T.Document.AddCSharp(
            @"Location.ChangeEvent += (Coordinate) =>
            {
               // Coordinate.Latitude;
               // Coordinate.Longitude;
               // Coordinate.Altitude;
            };");
                        T.Document.AddBreak();*/
            T.Document.AddParagraph("Reverse geocoding is requested using the asychronous Lookup method. There may be more than one placemark that matches the coordinate.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Location.Lookup(Coordinate, Result =>
{
  foreach (var Placemark in Result.GetPlacemarks())
  {
    // Placemark.Name;
    // Placemark.Locality;
    // Placemark.SubLocality;
    // Placemark.PostalCode;
    // Placemark.AdministrativeArea;
    // Placemark.SubAdministrativeArea;
    // Placemark.CountryName;
    // Placemark.CountryCode;
    // Placemark.Latitude;
    // Placemark.Longitude;
  }
});");
            if (!Application.Location.IsSupported)
              T.Document.AddParagraph("NOTE: Location API is not supported on this device.");

            var Preview = T.SetPreviewPanel().SetExampleLabel();

            var Code = T.AddCodePanel();

            var LookupPanel = T.AddControlPanel("Lookup");

            Action<Coordinate> LookupAction = (C) =>
            {
              Code.Text = string.Format("Location.Lookup(new Coordinate({0}, {1}, {2}), R => {{ ... }});", C.Latitude, C.Longitude, C.Altitude);

              if (!Application.Location.IsSupported)
              {
                Preview.Text = "Location API is not supported on this device.";
              }
              else
              {
                Preview.Text = "Looking up coordinate:" + Environment.NewLine + Environment.NewLine + C.ToPosition().Replace(",", Environment.NewLine);

                Application.Location.Lookup(C, Result =>
                {
                  Application.Window.Post(() =>
                  {
                    var Placemark = Result.GetPlacemarks().FirstOrDefault();
                    if (Placemark != null)
                      Preview.Text = Placemark.ToCanonical().Replace(",", Environment.NewLine);
                    else
                      Preview.Text = "Location not found.";
                  });
                });
              }
            };

            Preview.Text = "Lookup a coordinate!";

            var LocationArray = new[]
            {
              new { Name = "Apple HQ", Coordinate = new Coordinate(37.3318, -122.0312, 0) },
              new { Name = "Google HQ", Coordinate = new Coordinate(37.3861, -122.0839, 0) },
              new { Name = "Kestral HQ", Coordinate = new Coordinate(-31.900982, 115.828734, 0) },
              new { Name = "Microsoft HQ", Coordinate = new Coordinate(47.6740, -122.1215, 0) },
            };

            foreach (var Location in LocationArray)
            {
              var LookupButton = LookupPanel.AddButton(Location.Name + Environment.NewLine + Location.Coordinate.ToPosition().Replace(",", Environment.NewLine));
              LookupButton.SelectEvent += () => LookupAction(Location.Coordinate);
            }
          });
          #endregion

          #region Web.
          var WebTopic = B.AddTopic(2, "Web", T =>
          {
            T.Document.AddParagraph("Interop with the web using RESTful APIs and Json.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Launch a website in the default web browser app:");
            T.Document.AddBreak();
            T.Document.AddCode(@"Web.Launch(new Uri(""http://www.google.com""));");
            T.Document.AddBreak();
            T.Document.AddParagraph("Download a file using a stream:");
            T.Document.AddBreak();
            T.Document.AddCode(
@"using (var Download = Web.GetDownload(new Uri(""http://www.example.com/test.doc""))
using (var FileStream = Directory.RootFolder.NewFile(""test.doc"").Create())
  Download.Stream.CopyTo(FileStream);");
            T.Document.AddBreak();
            T.Document.AddParagraph("The broker allows simple GET and POST requests to a website:");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Broker = Web.NewBroker(""http://www.example.com"");

Broker.GetPlainText(""get-version"");

Broker.PostCsvTextLine(""insert-record"", new [] { ""One"", ""Two"", ""Three"" });

Broker.PostTextJsonObject(""query?arg=1"", new { Input = ""Callan"", Enabled = true });");
            T.Document.AddBreak();
            T.Document.AddParagraph("The client and server is used for raw TCP/IP connections. The following illustrates how to host a server and join a client. The server is given a channel in the AcceptEvent when the client connects. The same channel is passed to the RejectEvent when the client disconnects. This channel and the client can can exchange data using the InputStream and OutputStream.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Server = Web.NewServer(""127.0.0.1"", 3717, null);
Server.AcceptEvent += (Channel) => 
{ 
  // Channel.InputStream
  // Channel.OutputStream
};
Server.RejectEvent += (Channel) => { };
Server.Connect();

var Client = Web.NewClient(""127.0.0.1"", 3717, null);
Client.Connect();

// Client.InputStream
// Client.OutputStream

Client.Disconnect();

Server.Disconnect();");
            T.Document.AddBreak();
          });
          #endregion

          #region Vault.
          var VaultTopic = B.AddTopic(2, "Vault", T =>
          {
            T.Document.AddParagraph("Storage and recovery of sensitive information from the device's key-chain. This is often used for managing account credentials such as username/password. The idea is this information is stored securely but can be recovered in plain text.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Secret = Vault.NewSecret(""MySecret"");
Secret.Properties[""Username""] = ""admin"";
Secret.Properties[""Password""] = ""pwd1234!"";
Secret.Save();

// ... //

var Secret = Vault.NewSecret(""MySecret"");
Secret.Load();
var Username = Secret.Properties[""Username""];
var Password = Secret.Properties[""Password""];");
            T.Document.AddBreak();
          });
          #endregion

          #region Device.
          var DeviceTopic = B.AddTopic(1, "Device", T =>
          {
            T.Document.AddParagraph("Query what is supported on this device such as keyboard, mouse and touch screen.");
            T.Document.AddBreak();

            T.Document.AddCode
            (
              "Device.Name // " + Application.Device.Name,
              "Device.Model // " + Application.Device.Model,
              "Device.System // " + Application.Device.System,
              "Device.Keyboard // " + Application.Device.Keyboard,
              "Device.Mouse // " + Application.Device.Mouse,
              "Device.Touch // " + Application.Device.Touch,
              "Device.ProportionalFontName // " + Application.Device.ProportionalFontName,
              "Device.MonospacedFontName // " + Application.Device.MonospacedFontName,
              ""
            );
          });
          #endregion

          #region Market.
          var MarketTopic = B.AddTopic(1, "Market", T =>
          {
            T.Document.AddParagraph("Open the app listing in the current platform's app store. The following example is for the Invention Manual app.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Market.Browse
(
  AppleiTunesID: ""1195335633"", 
  GooglePlayID: ""com.x10host.invention.manual"", 
  WindowsStoreID: ""808CallanHodgskin.InventionManual_ax6pb6c2q0eqa""
);");

            T.Document.AddBreak();
          });
          #endregion

          #region Process.
          var ProcessTopic = B.AddTopic(1, "Process", T =>
          {
            T.Document.AddParagraph("Query the memory usage of the running process.");
            T.Document.AddBreak();
            T.Document.AddCode("var BytesUsed = Process.GetMemoryUsage().TotalBytes;");
            T.Document.AddBreak();
            T.Document.AddParagraph("Request a reclamation of cached resources such as platform instances of images and sounds. This is not required unless you have an unusually large turnover of resources.");
            T.Document.AddBreak();
            T.Document.AddCode("Process.MemoryReclamation();");
            T.Document.AddBreak();
          });
          #endregion
        });

        var ElementsBook = App.AddBook("Elements", B =>
        {
          B.Document.AddParagraph("The following are the layout elements common to all panels.");

          #region Background.
          var BackgroundTopic = B.AddTopic(1, "Background", T =>
          {
            T.Document.AddParagraph("Each panel has a background colour. Colour opacity is supported to allow the full ARGB colour range.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();

            var ColourPanel = T.AddControlPanel("Colour");

            foreach (var Colour in DarkColourArray)
            {
              var ControlButton = ColourPanel.AddButton(Colour.Name.PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Background.Colour = Colour;
              ControlButton.SelectQuery += () => X.Background.Colour == Colour;
            }
          });
          #endregion

          #region Alignment.
          var AlignmentTopic = B.AddTopic(1, "Alignment", T =>
          {
            T.Document.AddParagraph("The relative placement of a panel within its parent container. It is a key technique for laying out panels.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Stretch();

            var PlacementPanel = T.AddControlPanel("Placement");

            foreach (var Placement in Inv.Support.EnumHelper.GetEnumerable<Inv.Placement>())
            {
              var ControlButton = PlacementPanel.AddButton(Placement.ToString().PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Alignment.Set(Placement);
              ControlButton.SelectQuery += () => X.Alignment.Get() == Placement;
            }
          });
          #endregion

          #region Size.
          var SizeTopic = B.AddTopic(1, "Size", T =>
          {
            T.Document.AddParagraph("The width and height of panel can be explicitly set. Minimum and maximum constraints can also be set.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();

            var SizeArray = new[] 
        {
          new { Name = "No size", Width = (int?)null, Height = (int?)null },
          new { Name = "Size 100", Width = (int?)100, Height = (int?)100 },
          new { Name = "Width 100", Width = (int?)100, Height = (int?)null },
          new { Name = "Height 100", Width = (int?)null, Height = (int?)100 },
        };

            var SizePanel = T.AddControlPanel("Size");

            foreach (var Size in SizeArray)
            {
              var ControlButton = SizePanel.AddButton(Size.Name);
              ControlButton.SelectEvent += () => X.Size.Set(Size.Width, Size.Height);
              ControlButton.SelectQuery += () => X.Size.Is(Size.Width, Size.Height);
            }
          });
          #endregion

          #region Margin.
          var MarginTopic = B.AddTopic(1, "Margin", T =>
          {
            T.Document.AddParagraph("The outside spacing for a panel on all four sides. This is for gaps between adjacent panels.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Stretch();
            X.Margin.Clear();

            var MarginArray = new[]
        {
          new { Name = "No margin", Left = 0, Top = 0, Right = 0, Bottom = 0 },
          new { Name = "Uniform 50", Left = 50, Top = 50, Right = 50, Bottom = 50 },
          new { Name = "Horizontal 50", Left = 50, Top = 0, Right = 50, Bottom = 0 },
          new { Name = "Vertical 50", Left = 0, Top = 50, Right = 0, Bottom = 50 },
          new { Name = "Top 50", Left = 0, Top = 50, Right = 0, Bottom = 0 },
          new { Name = "Left 50", Left = 50, Top = 0, Right = 0, Bottom = 0 },
          new { Name = "Right 50", Left = 0, Top = 0, Right = 50, Bottom = 0 },
          new { Name = "Bottom 50", Left = 0, Top = 0, Right = 0, Bottom = 50 },
          //new { Name = "Top Left 50", Left = 50, Top = 50, Right = 0, Bottom = 0 },
        };

            var MarginPanel = T.AddControlPanel("Margin");

            foreach (var Margin in MarginArray)
            {
              var ControlButton = MarginPanel.AddButton(Margin.Name);
              ControlButton.SelectEvent += () => X.Margin.Set(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
              ControlButton.SelectQuery += () => X.Margin.Is(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
            }
          });
          #endregion

          #region Padding.
          var PaddingTopic = B.AddTopic(1, "Padding", T =>
          {
            T.Document.AddParagraph("The inside spacing for a panel on all four sides. This is used to indent the content inside a panel.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();
            X.Padding.Clear();

            var PaddingArray = new[]
        {
          new { Name = "No padding", Left = 0, Top = 0, Right = 0, Bottom = 0 },
          new { Name = "Uniform 50", Left = 50, Top = 50, Right = 50, Bottom = 50 },
          new { Name = "Horizontal 50", Left = 50, Top = 0, Right = 50, Bottom = 0 },
          new { Name = "Vertical 50", Left = 0, Top = 50, Right = 0, Bottom = 50 },
          new { Name = "Top 50", Left = 0, Top = 50, Right = 0, Bottom = 0 },
          new { Name = "Left 50", Left = 50, Top = 0, Right = 0, Bottom = 0 },
          new { Name = "Right 50", Left = 0, Top = 0, Right = 50, Bottom = 0 },
          new { Name = "Bottom 50", Left = 0, Top = 0, Right = 0, Bottom = 50 },
          //new { Name = "Top Left 50", Left = 50, Top = 50, Right = 0, Bottom = 0 },
        };

            var PaddingPanel = T.AddControlPanel("Padding");

            foreach (var Padding in PaddingArray)
            {
              var ControlButton = PaddingPanel.AddButton(Padding.Name);
              ControlButton.SelectEvent += () => X.Padding.Set(Padding.Left, Padding.Top, Padding.Right, Padding.Bottom);
              ControlButton.SelectQuery += () => X.Padding.Is(Padding.Left, Padding.Top, Padding.Right, Padding.Bottom);
            }
          });
          #endregion

          #region Corner.
          var CornerTopic = B.AddTopic(1, "Corner", T =>
          {
            T.Document.AddParagraph("Each corner of the panel can be rounded. This is used for style reasons such as circular panels.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();
            X.Corner.Clear();
            X.Size.Set(100, 100);

            var CornerArray = new[]
        {
          new { Name = "No corners", TopLeft = 0, TopRight = 0, BottomRight = 0, BottomLeft = 0 },
          new { Name = "Uniform 50", TopLeft = 50, TopRight = 50, BottomRight = 50, BottomLeft = 50 },
          new { Name = "Forward 50", TopLeft = 50, TopRight = 0, BottomRight = 50, BottomLeft = 0 },
          new { Name = "Backward 50", TopLeft = 0, TopRight = 50, BottomRight = 0, BottomLeft = 50 },
          new { Name = "Top Left 50", TopLeft = 50, TopRight = 0, BottomRight = 0, BottomLeft = 0 },
          new { Name = "Top Right 50", TopLeft = 0, TopRight = 50, BottomRight = 0, BottomLeft = 0 },
          new { Name = "Bottom Right 50", TopLeft = 0, TopRight = 0, BottomRight = 50, BottomLeft = 0 },
          new { Name = "Bottom Left 50", TopLeft = 0, TopRight = 0, BottomRight = 0, BottomLeft = 50 },
          //new { Name = "Top Left 50", TopLeft = 50, TopRight = 50, BottomRight = 0, BottomLeft = 0 },
        };

            var CornerPanel = T.AddControlPanel("Corner");

            foreach (var Corner in CornerArray)
            {
              var ControlButton = CornerPanel.AddButton(Corner.Name);
              ControlButton.SelectEvent += () => X.Corner.Set(Corner.TopLeft, Corner.TopRight, Corner.BottomRight, Corner.BottomLeft);
              ControlButton.SelectQuery += () => X.Corner.Is(Corner.TopLeft, Corner.TopRight, Corner.BottomRight, Corner.BottomLeft);
            }
          });
          #endregion

          #region Elevation.
          var ElevationTopic = B.AddTopic(1, "Elevation", T =>
          {
            T.Document.AddParagraph("This is the relative depth between panels along the z-axis. It is used to indicate distances by the depth of the shadow.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();
            X.Padding.Set(50);
            X.Elevation.Clear();

            var ElevationArray = new[]
            {
              new { Name = "No elevation", Depth = 0 },
              new { Name = "1dp", Depth = 1 },
              new { Name = "2dp", Depth = 2 },
              new { Name = "3dp", Depth = 3 },
              new { Name = "4dp", Depth = 4 },
              new { Name = "5dp", Depth = 5 },
              new { Name = "10dp", Depth = 10 },
              new { Name = "20dp", Depth = 20 },
            };

            var ElevationPanel = T.AddControlPanel("Elevation");

            foreach (var Elevation in ElevationArray)
            {
              var ControlButton = ElevationPanel.AddButton(Elevation.Name);
              ControlButton.SelectEvent += () => X.Elevation.Set(Elevation.Depth);
              ControlButton.SelectQuery += () => X.Elevation.Get() == Elevation.Depth;
            }
          });
          #endregion

          #region Border.
          var BorderTopic = B.AddTopic(1, "Border", T =>
          {
            T.Document.AddParagraph("The outer edge of the panel can have a solid and coloured line. The border is drawn on inside of the margin and the outside of the padding.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();
            X.Padding.Set(50);
            X.Border.Colour = Inv.Colour.Black;
            X.Border.Clear();

            var BorderArray = new[]
            {
              new { Name = "No border", Left = 0, Top = 0, Right = 0, Bottom = 0 },
              new { Name = "Uniform 5", Left = 5, Top = 5, Right = 5, Bottom = 5 },
              new { Name = "Horizontal 5", Left = 5, Top = 0, Right = 5, Bottom = 0 },
              new { Name = "Vertical 5", Left = 0, Top = 5, Right = 0, Bottom = 5 },
              new { Name = "Top 5", Left = 0, Top = 5, Right = 0, Bottom = 0 },
              new { Name = "Left 5", Left = 5, Top = 0, Right = 0, Bottom = 0 },
              new { Name = "Right 5", Left = 0, Top = 0, Right = 5, Bottom = 0 },
              new { Name = "Bottom 5", Left = 0, Top = 0, Right = 0, Bottom = 5 },
            };

            var BorderPanel = T.AddControlPanel("Border");

            foreach (var Border in BorderArray)
            {
              var ControlButton = BorderPanel.AddButton(Border.Name);
              ControlButton.SelectEvent += () => X.Border.Set(Border.Left, Border.Top, Border.Right, Border.Bottom);
              ControlButton.SelectQuery += () => X.Border.Is(Border.Left, Border.Top, Border.Right, Border.Bottom);
            }

            var ColourArray = new[]
            {
              Inv.Colour.Black,
              Inv.Colour.SteelBlue,
              Inv.Colour.HotPink,
              Inv.Colour.DarkRed,
            };

            var ColourPanel = T.AddControlPanel("Colour");

            foreach (var Colour in ColourArray)
            {
              var ControlButton = ColourPanel.AddButton(Colour.Name.PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Border.Colour = Colour;
              ControlButton.SelectQuery += () => X.Border.Colour == Colour;
            }
          });
          #endregion

          #region Opacity.
          var OpacityTopic = B.AddTopic(1, "Opacity", T =>
          {
            T.Document.AddParagraph("The percentage transparency of the panel and its children between 0 and 1.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();
            X.Padding.Set(20);

            var OpacityArray = new[]
            {
              new { Name = "Opaque", Depth = 1.00F },
              new { Name = "90%", Depth = 0.90F },
              new { Name = "75%", Depth = 0.75F },
              new { Name = "60%", Depth = 0.60F },
              new { Name = "50%", Depth = 0.50F },
              new { Name = "40%", Depth = 0.40F },
              new { Name = "25%", Depth = 0.25F },
              new { Name = "Clear", Depth = 0.00F },
            };

            var OpacityPanel = T.AddControlPanel("Opacity");

            foreach (var Opacity in OpacityArray)
            {
              var ControlButton = OpacityPanel.AddButton(Opacity.Name);
              ControlButton.SelectEvent += () => X.Opacity.Set(Opacity.Depth);
              ControlButton.SelectQuery += () => X.Opacity.Get() == Opacity.Depth;
            }
          });
          #endregion

          #region Visibility.
          var VisibilityTopic = B.AddTopic(1, "Visibility", T =>
          {
            T.Document.AddParagraph("A panel can be collapsed which removes them from the layout. Hidden panels that reserve their space are not supported.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Alignment.Center();

            var VisibilityArray = new[]
            {
              new { Name = "Visible", IsVisible = true },
              new { Name = "Collapsed", IsVisible = false },
            };

            var VisibilityPanel = T.AddControlPanel("Visibility");

            foreach (var Visibility in VisibilityArray)
            {
              var ControlButton = VisibilityPanel.AddButton(Visibility.Name);
              ControlButton.SelectEvent += () => X.Visibility.Set(Visibility.IsVisible);
              ControlButton.SelectQuery += () => X.Visibility.Get() == Visibility.IsVisible;
            }
          });
          #endregion
        });

        var PanelBook = App.AddBook("Panels", B =>
        {
          B.Document.AddParagraph("The following are the layout and function panels offered by Invention. They are used together to compose and arrange a modern user interface. Each panel is transalted to the appropriate native control on each platform.");

          #region Label.
          var LabelTopic = B.AddTopic(1, "Label", T =>
          {
            T.Document.AddParagraph("Labels are plain-text blocks that can be styled with a font. When using font names it is important to check that it is available on all required platforms.");

            var X = T.SetPreviewPanel().SetExampleLabel();
            T.SetCodePanel();
            X.Background.Colour = null;
            X.Font.Colour = Inv.Colour.Black;

            var LineWrappingPanel = T.AddControlPanel("Line Wrapping");

            foreach (var LineWrapping in new[] { true, false })
            {
              var ControlButton = LineWrappingPanel.AddButton(LineWrapping ? "Line wrap" : "No wrap");
              ControlButton.SelectEvent += () => X.LineWrapping = LineWrapping;
              ControlButton.SelectQuery += () => X.LineWrapping == LineWrapping;
            }

            var JustificationPanel = T.AddControlPanel("Justification");

            foreach (var Justification in Inv.Support.EnumHelper.GetEnumerable<Inv.Justification>())
            {
              var ControlButton = JustificationPanel.AddButton(Justification.ToString().PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Justify(Justification);
              ControlButton.SelectQuery += () => X.Justification == Justification;
            }

            var FontNamePanel = T.AddControlPanel("Font Name");

            foreach (var FontName in new[] { Application.Device.ProportionalFontName, Application.Device.MonospacedFontName })
            {
              var ControlButton = FontNamePanel.AddButton(FontName);
              ControlButton.SelectEvent += () => X.Font.Name = FontName;
              ControlButton.SelectQuery += () => X.Font.Name == FontName;
            }

            var FontWeightPanel = T.AddControlPanel("Font Weight");

            foreach (var Weight in Inv.Support.EnumHelper.GetEnumerable<Inv.FontWeight>())
            {
              var ControlButton = FontWeightPanel.AddButton(Weight.ToString().PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Font.Weight = Weight;
              ControlButton.SelectQuery += () => X.Font.Weight == Weight;
            }

            var FontSizePanel = T.AddControlPanel("Font Size");

            foreach (var FontSize in new[] { 72, 64, 48, 32, 24, 20, 18, 16, 14, 12, 10, 8 })
            {
              var ControlButton = FontSizePanel.AddButton("Size " + FontSize.ToString());
              ControlButton.SelectEvent += () => X.Font.Size = FontSize;
              ControlButton.SelectQuery += () => X.Font.Size == FontSize;
            }

            var ColourPanel = T.AddControlPanel("Colour");

            foreach (var Colour in DarkColourArray)
            {
              var ControlButton = ColourPanel.AddButton(Colour.Name.PascalCaseToTitleCase());
              ControlButton.SelectEvent += () => X.Font.Colour = Colour;
              ControlButton.SelectQuery += () => X.Font.Colour == Colour;
            }

          });
          #endregion

          #region Graphic.
          var GraphicTopic = B.AddTopic(2, "Graphic", T =>
          {
            T.Document.AddParagraph("Display an image such as PNG. The image is uniformly scaled to fit the layout.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewGraphic();
            Preview.SetExamplePanel(X);
            T.SetCodePanel();
            X.Image = Resources.Images.Logo;
          });
          #endregion

          #region Button.
          var ButtonTopic = B.AddTopic(2, "Button", T =>
          {
            T.Document.AddParagraph("This is a simple push-button which contains a content panel. The background colour is lightened for hover and darkened when pressed. Context tap is a long press on a touch screen or a right mouse click. The following code illustrates the use and interaction of the tap events.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Label = Surface.NewLabel();
Button.Content = Label;
Label.JustifyCenter();
Label.Font.Size = 20;
Label.Font.Colour = Colour.White;
Label.Text = ""TOUCH ME!"";

Button.Background.Colour = Colour.DodgerBlue;
Button.SingleTapEvent += () => Label.Text = ""SINGLE TAP"";
Button.ContextTapEvent += () => Label.Text = ""CONTEXT TAP"";");
            T.Document.AddBreak();
            
            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var Button = Surface.NewButton();
            Preview.SetExamplePanel(Button);

            var Label = Surface.NewLabel();
            Button.Content = Label;
            Label.JustifyCenter();
            Label.Font.Size = 20;
            Label.Font.Colour = Inv.Colour.White;
            Label.Text = "TOUCH ME!";

            Button.Background.Colour = Inv.Colour.DodgerBlue;
            Button.SingleTapEvent += () => Label.Text = "SINGLE TAP";
            Button.ContextTapEvent += () => Label.Text = "CONTEXT TAP";
          });
          #endregion

          #region Stack.
          var StackTopic = B.AddTopic(1, "Stack", T =>
          {
            T.Document.AddParagraph("Stacks are for horizontal and vertical linear layout of panels. Stacked panels are arranged in the order they were added.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewVerticalStack();
            Preview.SetExamplePanel(X);
            T.SetCodePanel();

            var ManagePanel = T.AddControlPanel("Manage");

            foreach (var StackOrientation in Inv.Support.EnumHelper.GetEnumerable<Inv.StackOrientation>())
            {
              var OrientationButton = ManagePanel.AddButton(StackOrientation.ToString());
              OrientationButton.SelectEvent += () =>
              {
                if (StackOrientation != X.Orientation)
                {
                  var LastPanelArray = X.GetPanels().ToArray();
                  X.RemovePanels();

                  X = Surface.NewStack(StackOrientation);
                  Preview.SetExamplePanel(X);

                  foreach (var LastPanel in LastPanelArray)
                    X.AddPanel(LastPanel);
                }
              };
              OrientationButton.SelectQuery += () => X.Orientation == StackOrientation;
            }

            var AddPanelButton = ManagePanel.AddButton("Add panel");
            AddPanelButton.SelectEvent += () =>
            {
              var StackIndex = X.GetPanels().Count();

              var StackLabel = Surface.NewLabel();
              X.AddPanel(StackLabel);
              StackLabel.JustifyCenter();
              StackLabel.Font.Name = Theme.CodeFontName;
              StackLabel.Font.Size = 20;
              StackLabel.Font.Colour = Inv.Colour.White;
              StackLabel.Text = "P" + (StackIndex + 1);
              StackLabel.Background.Colour = DarkColourArray[StackIndex % DarkColourArray.Length];
            };

            var RemovePanelButton = ManagePanel.AddButton("Remove panel");
            RemovePanelButton.SelectEvent += () =>
            {
              var LastPanel = X.GetPanels().LastOrDefault();
              if (LastPanel != null)
                X.RemovePanel(LastPanel);
            };

            // show five panels by default.
            AddPanelButton.Select();
            AddPanelButton.Select();
            AddPanelButton.Select();
            AddPanelButton.Select();
            AddPanelButton.Select();
          });
          #endregion

          #region Dock.
          var DockTopic = B.AddTopic(1, "Dock", T =>
          {
            T.Document.AddParagraph("Docks are for horizontal and vertical linear layout of panels. Docked panels are arranged in the order they were added.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewVerticalDock();
            Preview.SetExamplePanel(X);
            T.SetCodePanel();

            var OrientationPanel = T.AddControlPanel("Orientation");

            var AddPanel = T.AddControlPanel("Add");
            var RemovePanel = T.AddControlPanel("Remove");

            foreach (var DockOrientation in Inv.Support.EnumHelper.GetEnumerable<Inv.DockOrientation>())
            {
              var OrientationButton = OrientationPanel.AddButton(DockOrientation.ToString());
              OrientationButton.SelectEvent += () =>
              {
                if (DockOrientation != X.Orientation)
                {
                  var HeaderPanelArray = X.GetHeaders().ToArray();
                  X.RemoveHeaders();

                  var ClientPanelArray = X.GetClients().ToArray();
                  X.RemoveClients();

                  var FooterPanelArray = X.GetFooters().ToArray();
                  X.RemoveFooters();

                  X = Surface.NewDock(DockOrientation);
                  Preview.SetExamplePanel(X);

                  foreach (var HeaderPanel in HeaderPanelArray)
                    X.AddHeader(HeaderPanel);

                  foreach (var ClientPanel in ClientPanelArray)
                    X.AddClient(ClientPanel);

                  foreach (var FooterPanel in FooterPanelArray)
                    X.AddFooter(FooterPanel);
                }
              };
              OrientationButton.SelectQuery += () => X.Orientation == DockOrientation;
            }

            var AddHeaderButton = AddPanel.AddButton("Add Header");
            AddHeaderButton.SelectEvent += () =>
            {
              var DockIndex = X.GetHeaders().Count();

              var DockLabel = Surface.NewLabel();
              X.AddHeader(DockLabel);
              DockLabel.JustifyCenter();
              DockLabel.Font.Name = Theme.CodeFontName;
              DockLabel.Font.Size = 20;
              DockLabel.Font.Colour = Inv.Colour.White;
              DockLabel.Text = "H" + (DockIndex + 1);
              DockLabel.Background.Colour = DarkColourArray[DockIndex % DarkColourArray.Length].Darken(0.25F);
            };

            var RemoveHeaderButton = RemovePanel.AddButton("Remove Header");
            RemoveHeaderButton.SelectEvent += () =>
            {
              var LastPanel = X.GetHeaders().LastOrDefault();
              if (LastPanel != null)
                X.RemoveHeader(LastPanel);
            };

            var AddClientButton = AddPanel.AddButton("Add Client");
            AddClientButton.SelectEvent += () =>
            {
              var DockIndex = X.GetClients().Count();

              var DockLabel = Surface.NewLabel();
              X.AddClient(DockLabel);
              DockLabel.JustifyCenter();
              DockLabel.Font.Name = Theme.CodeFontName;
              DockLabel.Font.Size = 20;
              DockLabel.Font.Colour = Inv.Colour.White;
              DockLabel.Text = "C" + (DockIndex + 1);
              DockLabel.Background.Colour = DarkColourArray[DockIndex % DarkColourArray.Length];
            };

            var RemoveClientButton = RemovePanel.AddButton("Remove Client");
            RemoveClientButton.SelectEvent += () =>
            {
              var LastPanel = X.GetClients().LastOrDefault();
              if (LastPanel != null)
                X.RemoveClient(LastPanel);
            };

            var AddFooterButton = AddPanel.AddButton("Add Footer");
            AddFooterButton.SelectEvent += () =>
            {
              var DockIndex = X.GetFooters().Count();

              var DockLabel = Surface.NewLabel();
              X.AddFooter(DockLabel);
              DockLabel.JustifyCenter();
              DockLabel.Font.Name = Theme.CodeFontName;
              DockLabel.Font.Size = 20;
              DockLabel.Font.Colour = Inv.Colour.White;
              DockLabel.Text = "F" + (DockIndex + 1);
              DockLabel.Background.Colour = DarkColourArray[DockIndex % DarkColourArray.Length].Darken(0.50F);
            };

            var RemoveFooterButton = RemovePanel.AddButton("Remove Footer");
            RemoveFooterButton.SelectEvent += () =>
            {
              var LastPanel = X.GetFooters().LastOrDefault();
              if (LastPanel != null)
                X.RemoveFooter(LastPanel);
            };

            AddHeaderButton.Select();
            AddClientButton.Select();
            AddFooterButton.Select();
          });
          #endregion

          #region Overlay.
          var OverlayTopic = B.AddTopic(1, "Overlay", T =>
          {
            T.Document.AddParagraph("This layout is Z-order stack where the panels are placed on top of each other. Alignment can be used to layout the panels inside the overlay.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewOverlay();
            Preview.SetExamplePanel(X);
            T.SetCodePanel();

            var ManagePanel = T.AddControlPanel("Manage");

            var AddPanelButton = ManagePanel.AddButton("Add panel");
            AddPanelButton.SelectEvent += () =>
            {
              var OverlayIndex = X.GetPanels().Count();
              if (OverlayIndex >= 6)
                return;

              var OverlayFrame = Surface.NewFrame();
              X.AddPanel(OverlayFrame);
              OverlayFrame.Margin.Set(OverlayIndex * 20);
              OverlayFrame.Background.Colour = DarkColourArray[OverlayIndex % DarkColourArray.Length];

              var OverlayLabel = Surface.NewLabel();
              OverlayFrame.Content = OverlayLabel;
              OverlayLabel.Alignment.TopLeft();
              OverlayLabel.Font.Name = Theme.CodeFontName;
              OverlayLabel.Padding.Set(3);
              OverlayLabel.Font.Size = 14;
              OverlayLabel.Font.Colour = Inv.Colour.White;
              OverlayLabel.Text = "P" + (OverlayIndex + 1);
            };

            var RemovePanelButton = ManagePanel.AddButton("Remove panel");
            RemovePanelButton.SelectEvent += () =>
            {
              var LastPanel = X.GetPanels().LastOrDefault();
              if (LastPanel != null)
                X.RemovePanel(LastPanel);
            };

            // show three panels by default.
            AddPanelButton.Select();
            AddPanelButton.Select();
            AddPanelButton.Select();
          });
          #endregion

          #region Table.
          var TableTopic = B.AddTopic(2, "Table", T =>
          {
            T.Document.AddParagraph("This layout has rows and columns that can be sized as auto, star or fixed. Row and column spanning is not supported.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Rows and columns can be manually added and then used to set the individual cells.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var Row = Table.AddRow();
Row.Star();

var Column = Table.AddRow();
Column.Auto();

var Label = Surface.NewLabel();
Table.GetCell(Row, Column).Content = Label;");
            T.Document.AddBreak();
            T.Document.AddParagraph("Alternatively, the table is composed from panels before the rows and columns are accessed.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"var L1 = Surface.NewLabel();
var L2 = Surface.NewLabel();
var L3 = Surface.NewLabel();
var L4 = Surface.NewLabel();

Table.Compose(new[,]
{
  { L1, L2 },
  { L3, L4 },
});

Table.GetRow(0).Auto();
Table.GetRow(1).Star();
Table.GetColumn(0).Auto();
Table.GetRow(1).Star();");
            T.Document.AddBreak();

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewTable();
            Preview.SetExamplePanel(X);
            T.SetCodePanel();

            Action ComposeAction = () =>
            {
              foreach (var Cell in X.GetCells())
              {
                var CellIndex = (Cell.Row.Index * X.RowCount) + Cell.Column.Index;

                var OverlayLabel = Surface.NewLabel();
                Cell.Content = OverlayLabel;
                OverlayLabel.JustifyCenter();
                OverlayLabel.Background.Colour = DarkColourArray[CellIndex % DarkColourArray.Length];
                OverlayLabel.Font.Name = Theme.CodeFontName;
                OverlayLabel.Padding.Set(3);
                OverlayLabel.Font.Size = 14;
                OverlayLabel.Font.Colour = Inv.Colour.White;
                OverlayLabel.Text = Cell.Column.Index + "," + Cell.Row.Index;
              }
            };

            var RowPanel = T.AddControlPanel("Rows");

            var AddAutoRowButton = RowPanel.AddButton("Auto row");
            AddAutoRowButton.SelectEvent += () =>
            {
              X.AddRow().Auto();
              ComposeAction();
            };

            var AddStarRowButton = RowPanel.AddButton("Star row");
            AddStarRowButton.SelectEvent += () =>
            {
              X.AddRow().Star();
              ComposeAction();
            };

            var RemoveRowButton = RowPanel.AddButton("Remove row");
            RemoveRowButton.SelectEvent += () =>
            {
              var LastRow = X.GetRows().LastOrDefault();
              if (LastRow != null)
              {
                X.RemoveRow(LastRow);
                ComposeAction();
              }
            };

            var ColumnPanel = T.AddControlPanel("Columns");

            var AddAutoColumnButton = ColumnPanel.AddButton("Auto column");
            AddAutoColumnButton.SelectEvent += () =>
            {
              X.AddColumn().Auto();
              ComposeAction();
            };

            var AddStarColumnButton = ColumnPanel.AddButton("Star column");
            AddStarColumnButton.SelectEvent += () =>
            {
              X.AddColumn().Star();
              ComposeAction();
            };

            var RemoveColumnButton = ColumnPanel.AddButton("Remove column");
            RemoveColumnButton.SelectEvent += () =>
            {
              var LastColumn = X.GetColumns().LastOrDefault();
              if (LastColumn != null)
              {
                X.RemoveColumn(LastColumn);
                ComposeAction();
              }
            };

            // show three panels by default.
            AddAutoRowButton.Select();
            AddStarRowButton.Select();
            AddAutoRowButton.Select();
            AddAutoColumnButton.Select();
            AddStarColumnButton.Select();
            AddAutoColumnButton.Select();

            ComposeAction();
          });
          #endregion

          #region Board.
          var BoardTopic = B.AddTopic(1, "Board", T =>
          {
            T.Document.AddParagraph("The board is for pinning panels in any location and permits overlapping panels. Z-order is determined by the order the panels are added to the board.");
            T.Document.AddBreak();

            Inv.Surface Surface = T;
            
            var Preview = T.SetPreviewPanel();

            var X = Surface.NewBoard();
            Preview.SetExamplePanel(X);
            var Code = T.AddCodePanel();

            var PinArray = new[] 
            {
              new { Colour = Inv.Colour.Red, Rect = new Inv.Rect(20, 20, 80, 80) },
              new { Colour = Inv.Colour.Blue, Rect = new Inv.Rect(40, 40, 80, 80) },
              new { Colour = Inv.Colour.Green, Rect = new Inv.Rect(60, 60, 80, 80) },
              new { Colour = Inv.Colour.Purple, Rect = new Inv.Rect(20, 160, 80, 80) },
              new { Colour = Inv.Colour.Orange, Rect = new Inv.Rect(160, 160, 80, 80) },
              new { Colour = Inv.Colour.HotPink, Rect = new Inv.Rect(160, 20, 80, 80) },
            };

            var CodeList = new List<string>();

            foreach (var Pin in PinArray)
            {
              var Button = Surface.NewButton();
              X.AddPin(Button, Pin.Rect);
              Button.Background.Colour = Pin.Colour;

              CodeList.Add(string.Format("Board.AddPin({0}Button, new Rect({1}, {2}, {3}, {4}));", Pin.Colour.Name, Pin.Rect.Left, Pin.Rect.Top, Pin.Rect.Right, Pin.Rect.Bottom));
            }

            Code.Text = CodeList.AsSeparatedText("\r\n");
          });
          #endregion

          #region Frame.
          var FrameTopic = B.AddTopic(1, "Frame", T =>
          {
            T.Document.AddParagraph("The frames is container for another panel and has no other behaviour. It can be useful for switching content inside another layout such as a stack or dock.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var Frame = Surface.NewFrame();
            Preview.SetExamplePanel(Frame);
            var CodePanel = T.AddCodePanel();

            Frame.Border.Set(20);
            Frame.Border.Colour = Inv.Colour.SlateBlue;

            var Label = Surface.NewLabel();
            Frame.Content = Label;
            Label.Background.Colour = Inv.Colour.Tan;
            Label.Border.Set(20);
            Label.Border.Colour = Inv.Colour.Orchid;

            CodePanel.Text =
@"Frame.Border.Set(20);
Frame.Border.Colour = Colour.SlateBlue;

var Label = Surface.NewLabel();
Frame.Content = Label;
Label.Background.Colour = Colour.Tan;
Label.Border.Set(20);
Label.Border.Colour = Colour.Orchid;";
          });
          #endregion

          #region Scroll.
          var ScrollTopic = B.AddTopic(1, "Scroll", T =>
          {
            T.Document.AddParagraph("Scrolls are vertical or horizontal scrolling regions when the panels exceed the layout space. The native scrolling control is used for each platform and gives the expected bounce and feel.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewScroll(ScrollOrientation.Vertical);
            Preview.SetExamplePanel(X);
            T.SetCodePanel();

            var Y = Surface.NewVerticalStack();
            X.Content = Y;

            var ManagePanel = T.AddControlPanel("Manage");

            foreach (var Orientation in Inv.Support.EnumHelper.GetEnumerable<Inv.ScrollOrientation>())
            {
              var OrientationButton = ManagePanel.AddButton(Orientation.ToString());
              OrientationButton.SelectEvent += () =>
              {
                if (Orientation != X.Orientation)
                {
                  var LastPanelArray = Y.GetPanels().ToArray();
                  Y.RemovePanels();

                  X = Surface.NewScroll(Orientation);
                  Preview.SetExamplePanel(X);

                  Y = Surface.NewStack(Orientation == Inv.ScrollOrientation.Vertical ? Inv.StackOrientation.Vertical : Inv.StackOrientation.Horizontal);
                  X.Content = Y;

                  foreach (var LastPanel in LastPanelArray)
                    Y.AddPanel(LastPanel);
                }
              };
              OrientationButton.SelectQuery += () => X.Orientation == Orientation;
            }

            var AddPanelButton = ManagePanel.AddButton("Add panel");
            AddPanelButton.SelectEvent += () =>
            {
              var StackIndex = Y.GetPanels().Count();

              var StackLabel = Surface.NewLabel();
              Y.AddPanel(StackLabel);
              StackLabel.JustifyCenter();
              StackLabel.Font.Name = Theme.CodeFontName;
              StackLabel.Font.Size = 20;
              StackLabel.Font.Colour = Inv.Colour.White;
              StackLabel.Text = "P" + (StackIndex + 1);
              StackLabel.Background.Colour = DarkColourArray[StackIndex % DarkColourArray.Length];
            };

            var RemovePanelButton = ManagePanel.AddButton("Remove panel");
            RemovePanelButton.SelectEvent += () =>
            {
              var LastPanel = Y.GetPanels().LastOrDefault();
              if (LastPanel != null)
                Y.RemovePanel(LastPanel);
            };

            for (var Index = 0; Index < 20; Index++)
              AddPanelButton.Select();
          });
          #endregion

          #region Edit.
          var EditTopic = B.AddTopic(1, "Edit", T =>
          {
            T.Document.AddParagraph("Single-line text editor with several modes.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();
            T.SetCodePanel();

            var InputPanel = T.AddControlPanel("Input");

            Inv.Edit Edit = null;

            foreach (var Input in Inv.Support.EnumHelper.GetEnumerable<Inv.EditInput>())
            {
              var InputButton = InputPanel.AddButton(Input.ToString());
              InputButton.SelectEvent += () =>
              {
                if (Edit == null || Input != Edit.Input)
                {
                  var Text = Edit == null ? "Tap to start editing" : "";

                  Edit = Surface.NewEdit(Input);
                  Preview.SetExamplePanel(Edit);
                  Edit.Margin.Set(10);
                  Edit.Padding.Set(5);
                  Edit.Border.Set(2);
                  Edit.Border.Colour = Inv.Colour.Red;
                  Edit.Alignment.CenterStretch();
                  Edit.Background.Colour = Inv.Colour.White;
                  Edit.Font.Colour = Inv.Colour.Black;
                  Edit.Font.Size = 20;
                  Edit.Text = Text;
                }
              };
              InputButton.SelectQuery += () => Edit.Input == Input;

              if (Input == EditInput.Text)
                InputButton.Select();
            }
          });
          #endregion

          #region Memo.
          var MemoTopic = B.AddTopic(1, "Memo", T =>
          {
            T.Document.AddParagraph("Multi-line text editor with optional markup.");
            T.Document.AddBreak();
            T.Document.AddParagraph("Markup can be used to change the font of range of text in the memo. The range is expressed as a starting index in the text string and a length in characters. Markup can be used to display rich text such as syntax highlighting. The code samples in this manual are a marked up memo.");
            T.Document.AddBreak();
            T.Document.AddCode(
@"Memo.Text = ""Hello World"";

// 'World' is thin red 40pt font.
var RedMarkup = Memo.AddMarkup();
RedMarkup.Font.Thin();
RedMarkup.Font.Colour = Inv.Colour.Red;
RedMarkup.Font.Size = 40;
RedMarkup.AddRange(6, 5);");
            T.Document.AddBreak();

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var Memo = Surface.NewMemo();
            Preview.SetExamplePanel(Memo);
            T.SetCodePanel();

            Memo.Margin.Set(10);
            Memo.Padding.Set(5);
            Memo.Border.Set(2);
            Memo.Border.Colour = Inv.Colour.Red;
            Memo.Alignment.Stretch();
            Memo.Background.Colour = Inv.Colour.White;
            Memo.Font.Colour = Inv.Colour.Black;
            Memo.Font.Size = 20;
            Memo.Text = "Multiple lines" + Environment.NewLine + "of text" + Environment.NewLine + "as you can see." + Environment.NewLine + Environment.NewLine + "Tap to start editing.";
          });
          #endregion

          #region Flow.
          var FlowTopic = B.AddTopic(1, "Flow", T =>
          {
            T.Document.AddParagraph("The flow is a virtualised list of panels. It can be thought of as a combination of a vertical scroll and stack. However, the flow only queries for the panels that are immediately visible to the user.");
            T.Document.AddBreak();
            T.Document.AddParagraph("The flow can have multiple sections with a header and footer panel. There are several ways to request a section to upate. Setting the item count or calling reload on a section will force the re-query of the visible panels. Calling refresh on the flow will start a refresh animation in addition to reloading the data.");
            T.Document.AddBreak();

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewFlow();
            Preview.SetExamplePanel(X);
            var Code = T.AddCodePanel();
            Code.Text =
@"var Flow = Surface.NewFlow();

var Section = Flow.AddSection();

var HeaderLabel = Surface.NewLabel();
Section.SetHeader(HeaderLabel);
HeaderLabel.Text = ""HEADER"";

var FooterLabel = Surface.NewLabel();
Section.SetFooter(FooterLabel);
FooterLabel.Text = ""FOOTER"";

Section.ItemQuery += (Index) =>
{
  var Label = Surface.NewLabel();

  Label.Background.Colour = Index % 2 == 0 ? Colour.LightGray : Colour.DarkGray;
  Label.Text = ""Item "" + Index;

  return Label;
};

Section.SetItemCount(100);";

            var Section = X.AddSection();

            var HeaderLabel = Surface.NewLabel();
            Section.SetHeader(HeaderLabel);
            HeaderLabel.Text = "HEADER";

            var FooterLabel = Surface.NewLabel();
            Section.SetFooter(FooterLabel);
            FooterLabel.Text = "FOOTER";

            Section.ItemQuery += (Index) =>
            {
              var Label = Surface.NewLabel();

              Label.Background.Colour = Index % 2 == 0 ? Colour.LightGray : Colour.DarkGray;
              Label.Text = "Item " + Index;

              return Label;
            };

            Section.SetItemCount(100);
          });
          #endregion

          #region Canvas.
          var CanvasTopic = B.AddTopic(2, "Canvas", T =>
          {
            // TODO: polygons are not properly supported.

            T.Document.AddParagraph("This is for custom drawing using primitives (lines, rectangles, ellipses, text and images). ");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewCanvas();
            Preview.SetExamplePanel(X);
            
            var CodePanel = T.AddCodePanel();

            X.PressEvent += (P) =>
            {
            };
            X.ReleaseEvent += (P) =>
            {
            };
            X.SingleTapEvent += (P) =>
            {
            };
            X.ContextTapEvent += (P) =>
            {
            };

            var Lines = true;
            var Ellipses = true;
            var Rectangles = true;
            var Polygons = false;
            var Texts = true;
            var Images = true;

            X.DrawEvent += (DC) =>
            {
              var CodeList = new List<string>();
              CodeList.Add("Canvas.DrawEvent += (DC) =>");
              CodeList.Add("{");

              if (Lines)
              {
                DC.DrawLine(Colour.Red, 4, new Point(20, 20), new Point(230, 20));
                CodeList.Add("  DC.DrawLine(Colour.Red, 4, new Point(20, 20), new Point(230, 20));");
              }

              if (Ellipses)
              {
                DC.DrawEllipse(Colour.Turquoise, Colour.OliveDrab, 4, new Point(128, 60), new Point(20, 20));
                CodeList.Add("  DC.DrawEllipse(Colour.Turquoise, Colour.OliveDrab, 4, new Point(128, 60), new Point(20, 20));");
              }

              if (Rectangles)
              {
                DC.DrawRectangle(Colour.Thistle, Colour.Violet, 4, new Rect(64, 96, 128, 32));
                CodeList.Add("  DC.DrawRectangle(Colour.Thistle, Colour.Violet, 4, new Rect(64, 96, 128, 32));");
              }

              if (Polygons)
              {
                DC.DrawPolygon(Colour.Orange, Colour.DarkOrange, 4, LineJoin.Round, new Point(64, 148), new Point(32, 172), new Point(222, 172), new Point(124, 148));
                CodeList.Add("  DC.DrawPolygon(Colour.Orange, Colour.DarkOrange, 4, LineJoin.Round, new Point(64, 148), new Point(32, 172), new Point(222, 172), new Point(124, 148));");
              }

              if (Texts)
              {
                DC.DrawText("Hello Canvas!", null, 30, FontWeight.Regular, Colour.Black, new Point(128, 192), HorizontalPosition.Center, VerticalPosition.Center);
                CodeList.Add("  DC.DrawText(\"Hello Canvas!\", null, 30, FontWeight.Regular, Colour.Black, new Point(128, 192), HorizontalPosition.Center, VerticalPosition.Center);");
              }

              if (Images)
              {
                DC.DrawImage(Resources.Images.Logo, new Rect(200, 200, 48, 48));
                CodeList.Add("  DC.DrawImage(Resources.Images.Logo, new Rect(200, 200, 48, 48));");
              }

              CodeList.Add("};");

              CodePanel.Text = CodeList.AsSeparatedText("\r\n");
            };

            Action<Action> UpdateAction = (Action) =>
            {
              if (Action != null)
                Action();

              X.Draw();
            };

            var ManagePanel = T.AddControlPanel("Manage");

            var LineButton = ManagePanel.AddButton("Line");
            LineButton.SelectEvent += () => UpdateAction(() => Lines = !Lines);
            LineButton.SelectQuery += () => Lines;

            var EllipseButton = ManagePanel.AddButton("Ellipse");
            EllipseButton.SelectEvent += () => UpdateAction(() => Ellipses = !Ellipses);
            EllipseButton.SelectQuery += () => Ellipses;

            var RectangleButton = ManagePanel.AddButton("Rectangle");
            RectangleButton.SelectEvent += () => UpdateAction(() => Rectangles = !Rectangles);
            RectangleButton.SelectQuery += () => Rectangles;

            //var PolygonButton = ManagePanel.AddButton("Polygon");
            //PolygonButton.SelectEvent += () => UpdateAction(() => Polygons = !Polygons);
            //PolygonButton.SelectQuery += () => Polygons;

            var TextButton = ManagePanel.AddButton("Text");
            TextButton.SelectEvent += () => UpdateAction(() => Texts = !Texts);
            TextButton.SelectQuery += () => Texts;

            var ImageButton = ManagePanel.AddButton("Image");
            ImageButton.SelectEvent += () => UpdateAction(() => Images = !Images);
            ImageButton.SelectQuery += () => Images;

            UpdateAction(null);
          });
          #endregion

          #region Browser.
          var BrowserTopic = B.AddTopic(1, "Browser", T =>
          {
            T.Document.AddParagraph("The browser embeds the native web browser for each platform. This is used for integrating with webapps and loading html text.");

            Inv.Surface Surface = T;

            var Preview = T.SetPreviewPanel();

            var X = Surface.NewBrowser();
            Preview.SetExamplePanel(X);
            var Code = T.AddCodePanel();

            var BrowsePanel = T.AddControlPanel("Browse");

            foreach (var BrowseAddress in new[] { "https://google.com", "https://apple.com" })
            {
              var Uri = new Uri(BrowseAddress);

              var BrowseButton = BrowsePanel.AddButton(Uri.Host);
              BrowseButton.SelectEvent += () =>
              {
                X.LoadUri(Uri);
                Code.Text = "Browser.LoadUri(new Uri(" + BrowseAddress + "));";
              };
            }

            var FragmentPanel = T.AddControlPanel("Fragment");

            foreach (var FragmentText in new[] { "<b>Hello</b>", "<i>Hello</i>", "<u>Hello</u>" })
            {
              var BrowseButton = FragmentPanel.AddButton(FragmentText);
              BrowseButton.SelectEvent += () =>
              {
                X.LoadHtml("<html>" + FragmentText + "</html>");
                Code.Text = "Browser.LoadHtml(\"<html>" + FragmentText + "</html>\")";
              };
            }
          });
          #endregion
        });

        var TutorialBook = App.AddBook("Tutorials", B =>
        {
          B.Document.AddParagraph("These tutorials take you through the steps for creating a portable app that runs on multiple platforms.");
          B.Document.AddBreak();
          B.Document.AddParagraph("Prerequisites:");
          B.Document.AddBreak();
          var PrerequisiteList = B.Document.AddNumberedList
          (
            "Microsoft Windows 10 (recommended)",
            "Microsoft Visual Studio 2013 (or above) with Xamarin",
            App.IsAndroid ? "Android device via USB debugging or an Android emulator" : null,
            App.IsIOS ? "Mac computer with XCode and Xamarin installed" : null,
            App.IsIOS ? "iOS device attached to the Mac or using the iOS Simulator" : null
          );

          #region Portable App.
          B.AddTopic(2, "Portable App", T =>
          {
            T.Document.AddParagraph("The following steps will create a new portable app:");
            T.Document.AddBreak();

            var StepList = T.Document.AddNumberedList();
            StepList.AddItem("Right click on your solution | Add > New Project");
            StepList.AddItem("Go to File | New > Project");
            StepList.AddItem("Select template 'Class Library (Portable)'");
            StepList.AddItem("Name your project eg. MyProject and click OK");
            StepList.AddItem("Accept the default targets in the 'Add Portable Class Library' dialog");
            StepList.AddItem("Right click on your new project | Manage NuGet packages");
            StepList.AddItem("Install NuGet package 'Invention.Platform'");
            StepList.AddItem("Open the Class1.cs file");
            StepList.AddItem("Replace the Class1 class with this code:");

            T.Document.AddBreak();
            T.Document.AddCode(
@"namespace MyProject
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      Application.Title = ""My Project"";

      var Surface = Application.Window.NewSurface();
      Surface.Background.Set(Colour.WhiteSmoke);

      var Label = Surface.NewLabel();
      Surface.Content = Label;
      Label.Alignment.Center();
      Label.Font.Size = 20;
      Label.Text = ""Hello World"";

      Surface.Transition();
    }
  }
}");

            T.Document.AddBreak();
            T.Document.AddParagraph("You are now ready to create a platform-specific version of this app.");
          });
          #endregion

          #region Windows Desktop App.
          if (App.IsWindowsDesktop)
          {
            B.AddTopic(2, "Windows Desktop App", T =>
            {
              T.Document.AddParagraph("The following steps will create a new Windows Desktop app:");
              T.Document.AddBreak();
              var StepList = T.Document.AddNumberedList();
              StepList.AddItem("Right click on your solution | Add > New Project");
              StepList.AddItem("Select template 'WPF Application'");
              StepList.AddItem("Name your project eg. MyProjectW and click OK");
              StepList.AddItem("Delete App.config, App.xaml and MainWindow.xaml");
              StepList.AddItem("Under Properties, delete Resources.resx and Settings.settings");
              StepList.AddItem("Right click on your new project | Manage NuGet packages");
              StepList.AddItem("Install NuGet package 'Invention.Platform.W'");
              StepList.AddItem("Add a reference to your portable app project eg. MyProject");
              StepList.AddItem("Add a new code file eg. Program.cs");
              StepList.AddItem("Replace the Program class with this code:");

              T.Document.AddBreak();
              T.Document.AddCode(
  @"namespace MyProject
{
  public sealed class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.DeviceEmulation = Inv.WpfDeviceEmulation.iPad_Mini;
        Inv.WpfShell.DeviceRotation = false;
        Inv.WpfShell.FullScreenMode = false;
        Inv.WpfShell.DefaultWindowWidth = 1920;
        Inv.WpfShell.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(MyProject.Shell.Install);
      });
    }
  }
}");

              T.Document.AddBreak();
              T.Document.AddParagraph("You are now ready to run your app on Windows Desktop or inside the Visual Studio debugger. This is the preferred platform for developing your app functionality.");
            });

            // TODO: recommendations for publishing a Windows Desktop app?
          }
          #endregion

          #region Android App.
          if (App.IsAndroid)
          {
            B.AddTopic(2, "Android App", T =>
            {
              T.Document.AddParagraph("The following steps will create a new Android app:");
              T.Document.AddBreak();
              var StepList = T.Document.AddNumberedList();
              StepList.AddItem("Right click on your solution | Add > New Project");
              StepList.AddItem("Select template 'Blank App (Android)'");
              StepList.AddItem("Name your project eg. MyProjectA and click OK");
              StepList.AddItem("Right click on your new project | Properties > Application");
              StepList.AddItem("Change 'Minimum Android to target' to 'Android 4.4 (API Level 19 - Kit Kat)'");
              StepList.AddItem("Edit the project .csproj file in a text editor");
              StepList.AddItem("Change the <TargetFrameworkVersion> element from 'v6.0' to 'v7.0'");
              StepList.AddItem("Reload the project");
              StepList.AddItem("Right click on your new project | Manage NuGet packages");
              StepList.AddItem("Install NuGet package 'Invention.Platform.A'");
              StepList.AddItem("Add a reference to your portable app project eg. MyProject");
              StepList.AddItem("Open the MainActivity.cs file");
              StepList.AddItem("Replace the MainActivity class with this code:");

              T.Document.AddBreak();
              T.Document.AddCode(
  @"namespace MyProject
{
  [Activity(Label = ""My Project"", MainLauncher = true, Icon = ""@drawable/icon"")]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    public override void Install(Inv.Application Application)
    {
      MyProject.Shell.Install(Application);
    }
  }
}");

              T.Document.AddBreak();
              T.Document.AddParagraph("You are now ready to run your app on an Android device or emulator.");
            });

            // TODO: recommendations for publishing an Android app to Google Play?
          }
          #endregion

          #region iOS App.
          if (App.IsIOS)
          {
            B.AddTopic(2, "iOS App", T =>
            {
              T.Document.AddParagraph("The following steps will create a new iOS app:");
              T.Document.AddBreak();
              var StepList = T.Document.AddNumberedList();
              StepList.AddItem("Right click on your solution | Add > New Project");
              StepList.AddItem("Select template 'Blank App (iOS)'");
              StepList.AddItem("Name your project eg. MyProjectI and click OK");
              StepList.AddItem("Right click on your new project | Properties > iOS Application");
              StepList.AddItem("Change 'Deployment Target' to '8.0'");
              StepList.AddItem("Right click on your new project | Manage NuGet packages");
              StepList.AddItem("Install NuGet package 'Invention.Platform.I'");
              StepList.AddItem("Add a reference to your portable app project eg. MyProject");
              StepList.AddItem("Open the Main.cs file");
              StepList.AddItem("Replace the Application class with this code:");

              T.Document.AddBreak();
              T.Document.AddCode(
  @"namespace MyProject
{
  public sealed class Program
  {
    static void Main(string[] args)
    {
      Inv.iOSShell.Run(MyProject.Shell.Install);
    }
  }
}");

              T.Document.AddBreak();
              T.Document.AddParagraph("You are now ready to run your app on an iOS device or emulator.");
            });

            // TODO: recommendations for publishing an iOS app to Apple iTunes?
          }
          #endregion

          #region Universal Windows App.
          if (App.IsUniversalWindows)
          {
            B.AddTopic(2, "Windows Store App", T =>
            {
              T.Document.AddParagraph("The following steps will create a new Windows Store app:");
              T.Document.AddBreak();
              var StepList = T.Document.AddNumberedList();
              StepList.AddItem("Right click on your solution | Add > New Project");
              StepList.AddItem("Select template 'Blank App (Universal Apps)'");
              StepList.AddItem("Name your project eg. MyProjectU and click OK");
              StepList.AddItem("Delete the WindowsPhone project eg. MyProjectU.WindowsPhone");
              StepList.AddItem("Rename the Windows project eg. MyProjectU.Windows -> MyProjectU");
              StepList.AddItem("Move App.xaml from the Shared project to your Windows project");
              StepList.AddItem("Delete the shared project eg. MyProjectU.Shared");
              StepList.AddItem("Right click on your new project | Manage NuGet packages");
              StepList.AddItem("Install NuGet package 'Invention.Platform.U'");
              StepList.AddItem("Add a reference to your portable app project eg. MyProject");
              StepList.AddItem("Open the Main.xaml.cs file");
              StepList.AddItem("Replace the MainPage class with this code:");

              T.Document.AddBreak();
              T.Document.AddCode(
  @"namespace MyProject
{
  public sealed partial class MainPage : Page
  {
    public MainPage()
    {
      this.InitializeComponent();

      Inv.UwaShell.Attach(this, MyProject.Shell.Install);
    }
  }
}");

              T.Document.AddBreak();
              T.Document.AddParagraph("You are now ready to run your app on Windows 8 or 10.");
              T.Document.AddBreak();
              T.Document.AddParagraph("NOTE: You may need to change your build platform to x86.");
            });

            // TODO: recommendations for publishing an Universal Windows app to Windows Store?
          }
          #endregion
        });
/*
#if DEBUG
        var ReferenceBook = App.AddBook("Reference", B =>
        {
          #region Types.
          var RootType = typeof(Inv.Application);

          var TypeSet = new HashSet<Type>();

          CollectTypeSet(TypeSet, typeof(Inv.Application));

          var TypeArray = TypeSet.OrderBy(T => T.Name).ToArray();

          foreach (var Type in TypeArray.Select(T => T.GetReflectionInfo()))
          {
            var Prefix = Type.IsClass ? "class" : Type.IsEnum ? "enum" : Type.IsInterface ? "interface" : "struct";

            var TypeTopic = B.AddTopic(1, Type.Name + " " + Prefix, T =>
            {
              if (Type.IsEnum)
              {
                T.Document.AddBreak();
                T.Document.AddParagraph("Members");
                T.Document.AddBreak();

                var StringBuilder = new StringBuilder();

                foreach (var Member in Enum.GetValues(Type.AsType()))
                  StringBuilder.AppendLine(Member.ToString());

                T.Document.AddCode(StringBuilder.ToString().Trim());
              }
              else
              {
                var PropertyList = new Inv.DistinctList<System.Reflection.PropertyInfo>();
                CollectPropertyList(PropertyList, Type);
                var PropertyArray = PropertyList.
                  Where(P => (P.CanRead && P.GetMethod.IsPublic) || (P.CanWrite && P.SetMethod.IsPublic)).
                  OrderBy(P => !P.PropertyType.GetReflectionInfo().IsPrimitive && P.PropertyType != typeof(string)).
                  ThenBy(P => P.Name).ToArray();

                var FieldList = new Inv.DistinctList<System.Reflection.FieldInfo>();
                CollectFieldList(FieldList, Type);
                var FieldArray = FieldList.
                  Where(F => F.IsPublic && !F.IsStatic).
                  OrderBy(M => M.Name).ToArray();

                var MethodList = new Inv.DistinctList<System.Reflection.MethodInfo>();
                CollectMethodList(MethodList, Type);
                var MethodArray = MethodList.
                  Where(M => M.IsPublic && !M.IsStatic && (M.Attributes & System.Reflection.MethodAttributes.SpecialName) == 0 && !M.CustomAttributes.Any(A => A.AttributeType == typeof(ConditionalAttribute))).
                  OrderBy(M => M.Name).ToArray();

                T.Document.AddParagraph("<this is a placeholder for a class description>");

                if (FieldArray.Length > 0)
                {
                  T.Document.AddBreak();
                  T.Document.AddParagraph("Fields");
                  T.Document.AddBreak();

                  var StringBuilder = new StringBuilder();

                  foreach (var Field in FieldArray)
                  {
                    var FieldTypeName = FormatTypeName(Field.FieldType);

                    StringBuilder.AppendLine(FieldTypeName + " " + Field.Name + ";");
                  }

                  T.Document.AddCode(StringBuilder.ToString().Trim());
                }

                if (PropertyArray.Length > 0)
                {
                  T.Document.AddBreak();
                  T.Document.AddParagraph("Properties");
                  T.Document.AddBreak();

                  var StringBuilder = new StringBuilder();

                  foreach (var Property in PropertyArray)
                  {
                    var PropertyTypeName = FormatTypeName(Property.PropertyType);

                    var Accessor = "{ " + (Property.CanRead && Property.GetMethod.IsPublic ? "get; " : "") + (Property.CanWrite && Property.SetMethod.IsPublic ? "set; " : "") + "}";

                    StringBuilder.AppendLine(PropertyTypeName + " " + Property.Name + " " + Accessor);
                  }

                  T.Document.AddCode(StringBuilder.ToString().Trim());
                }

                if (MethodArray.Length > 0)
                {
                  T.Document.AddBreak();
                  T.Document.AddParagraph("Methods");
                  T.Document.AddBreak();

                  var StringBuilder = new StringBuilder();

                  foreach (var Method in MethodArray)
                  {
                    var MethodName = Method.Name;
                    var ReturnTypeName = FormatTypeName(Method.ReturnType);

                    var Parameters = Method.GetParameters().Select(P => FormatTypeName(P.ParameterType) + " " + P.Name).AsSeparatedText(", ");

                    StringBuilder.AppendLine(ReturnTypeName + " " + MethodName + "(" + Parameters + ");");
                  }

                  T.Document.AddCode(StringBuilder.ToString().Trim());
                }
              }
            });
          }
          #endregion
        });
#endif
*/
      };
    }

    private static void CollectTypeSet(HashSet<Type> TypeSet, Type Type)
    {
      if (Type == null || Type.Namespace != "Inv" || Type.Name == "DistinctList`1" || Type.Name == "Collection`1" || Type.Name == "Singleton`1" || Type.Name == "T")
        return;

      if (TypeSet.Add(Type))
      {
        var TypeInfo = Type.GetReflectionInfo();

        foreach (var Property in TypeInfo.GetReflectionProperties())
          CollectTypeSet(TypeSet, Property.PropertyType);

        foreach (var Method in TypeInfo.GetReflectionMethods())
        {
          foreach (var Parameter in Method.GetParameters())
            CollectTypeSet(TypeSet, Parameter.ParameterType);

          CollectTypeSet(TypeSet, Method.ReturnType);
        }
      }
    }
    private static void CollectPropertyList(Inv.DistinctList<System.Reflection.PropertyInfo> PropertyList, System.Reflection.TypeInfo Type)
    {
      if (Type == null || Type.Namespace != "Inv")
        return;

      if (Type.BaseType != null)
        CollectPropertyList(PropertyList, Type.BaseType.GetReflectionInfo());

      PropertyList.AddRange(Type.GetReflectionProperties());
    }
    private static void CollectFieldList(Inv.DistinctList<System.Reflection.FieldInfo> FieldList, System.Reflection.TypeInfo Type)
    {
      if (Type == null || Type.Namespace != "Inv")
        return;

      if (Type.BaseType != null)
        CollectFieldList(FieldList, Type.BaseType.GetReflectionInfo());

      FieldList.AddRange(Type.GetReflectionFields());
    }
    private static void CollectMethodList(Inv.DistinctList<System.Reflection.MethodInfo> MethodList, System.Reflection.TypeInfo Type)
    {
      if (Type == null || Type.Namespace != "Inv")
        return;

      if (Type.BaseType != null)
        CollectMethodList(MethodList, Type.BaseType.GetReflectionInfo());

      MethodList.AddRange(Type.GetReflectionMethods());
    }
    private static string FormatTypeName(Type Type)
    {
      if (Type == typeof(void))
        return "void";
      else if (Type == typeof(object))
        return "object";
      else if (Type == typeof(string))
        return "string";
      else if (Type == typeof(byte))
        return "byte";
      else if (Type == typeof(short))
        return "short";
      else if (Type == typeof(ushort))
        return "ushort";
      else if (Type == typeof(int))
        return "int";
      else if (Type == typeof(long))
        return "long";
      else if (Type == typeof(uint))
        return "uint";
      else if (Type == typeof(ulong))
        return "ulong";
      else if (Type == typeof(bool))
        return "bool";
      else if (Type == typeof(float))
        return "float";
      else if (Type == typeof(double))
        return "double";
      else if (Type == typeof(decimal))
        return "decimal";
      else if (Type == typeof(TimeSpan))
        return "TimeSpan";
      else if (Type == typeof(DateTime))
        return "DateTime";
      else if (Type == typeof(DateTimeOffset))
        return "DateTimeOffset";
      else if (Type == typeof(Uri))
        return "Uri";
      else if (Type == typeof(Action))
        return "Action";
      else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(Action<>))
        return "Action<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(Predicate<>))
        return "Predicate<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
        return "IEnumerable<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(IComparer<>))
        return "IComparer<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(Inv.DistinctList<>))
        return "Inv.DistinctList<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      //else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(Inv.Collection<>))
      //  return "Inv.Collection<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      //else if (Type.IsConstructedGenericType && Type.GetGenericTypeDefinition() == typeof(Inv.Singleton<>))
      //  return "Inv.Singleton<" + Type.GenericTypeArguments.Select(T => FormatTypeName(T)).AsSeparatedText(", ") + ">";
      else if (Type.IsArray)
        return FormatTypeName(Type.GetElementType()) + "[]";
      else if (Type.Namespace == "Inv")
        return Type.Name;
      else if (Type.Namespace == "System")
        return Type.Name;
      else
        return Type.Name;
    }
  }
}
