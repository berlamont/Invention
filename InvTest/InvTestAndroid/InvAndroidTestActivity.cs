﻿/*! 7 !*/
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.OS;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using Inv.Support;

namespace InvTest
{
  [Activity(Label = "InvAndroidTest", MainLauncher = true, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    public override void Install(Inv.Application Application)
    {
      InvTest.Shell.Install(Application);
    }
  }

#if DEBUG
  [Activity(Label = "InvAndroidTestNative", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class NativeActivity : Inv.AndroidDebugNativeActivity
  {  
  }

  [Activity(Label = "InvAndroidTestOpenGL", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public abstract class OpenGLActivity : Inv.AndroidDebugOpenGLActivity
  {
  }

  [Activity(Label = "InvAndroidTestRender", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class RenderActivity : Inv.AndroidDebugRenderActivity
  {
  }
#endif
}