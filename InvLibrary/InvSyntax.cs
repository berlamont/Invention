﻿/*! 14 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using Inv.Support;

namespace Inv.Syntax
{
  public static class Foundation
  {
    public static void WriteTextString(Inv.Syntax.Grammar SyntaxGrammar, out string TextString, Action<Inv.Syntax.Formatter> Action)
    {
      using (var StringWriter = new StringWriter())
      {
        var SyntaxFormatter = new Inv.Syntax.Formatter(new Inv.Syntax.TextWriteService(StringWriter, SyntaxGrammar));

        SyntaxFormatter.Start();

        Action(SyntaxFormatter);

        SyntaxFormatter.Stop();

        StringWriter.Flush();

        TextString = StringWriter.ToString();
      }
    }
    public static void ReadTextString(Inv.Syntax.Grammar SyntaxGrammar, string TextString, Action<Inv.Syntax.Extractor> Action)
    {
      using (var StringReader = new StringReader(TextString))
      {
        var SyntaxExtractor = new Inv.Syntax.Extractor(new Inv.Syntax.TextReadService(StringReader, SyntaxGrammar));

        SyntaxExtractor.Start();

        Action(SyntaxExtractor);

        SyntaxExtractor.Stop();
      }
    }
    public static void WriteTextStream(Inv.Syntax.Grammar SyntaxGrammar, StreamWriter StreamWriter, Action<Inv.Syntax.Formatter> Action)
    {
      var SyntaxFormatter = new Inv.Syntax.Formatter(new Inv.Syntax.TextWriteService(StreamWriter, SyntaxGrammar));

      SyntaxFormatter.Start();

      Action(SyntaxFormatter);

      SyntaxFormatter.Stop();

      StreamWriter.Flush();
    }
    public static void WriteTextStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Formatter> Action)
    {
      using (var StreamWriter = new StreamWriter(Stream, Encoding.UTF8, 65536, true))
      {
        WriteTextStream(SyntaxGrammar, StreamWriter, Action);
      }
    }
    public static void ReadTextStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Extractor> Action, string FileName = null)
    {
      using (var StreamReader = new StreamReader(Stream, Encoding.UTF8, true, 65536, true))
      {
        ReadTextStream(SyntaxGrammar, StreamReader, Action, FileName);
      }
    }
    public static void ReadTextStream(Inv.Syntax.Grammar SyntaxGrammar, StreamReader StreamReader, Action<Inv.Syntax.Extractor> Action, string FileName = null)
    {
      var SyntaxExtractor = new Inv.Syntax.Extractor(new Inv.Syntax.TextReadService(StreamReader, SyntaxGrammar, FileName));

      SyntaxExtractor.Start();

      Action(SyntaxExtractor);

      SyntaxExtractor.Stop();
    }

    public static void WriteBinaryBuffer(Inv.Syntax.Grammar SyntaxGrammar, out byte[] Buffer, Action<Inv.Syntax.Formatter> Action)
    {
      using (var MemoryStream = new MemoryStream())
      {
        WriteBinaryStream(SyntaxGrammar, MemoryStream, Action);

        Buffer = MemoryStream.ToArray();
      }
    }
    public static void ReadBinaryBuffer(Inv.Syntax.Grammar SyntaxGrammar, byte[] Buffer, Action<Inv.Syntax.Extractor> Action)
    {
      using (var MemoryStream = new MemoryStream(Buffer))
        ReadBinaryStream(SyntaxGrammar, MemoryStream, Action);
    }
    public static void WriteBinaryStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Formatter> Action)
    {
      using (var BinaryWriter = new System.IO.BinaryWriter(Stream, Encoding.UTF8, true))
      {
        var SyntaxFormatter = new Inv.Syntax.Formatter(new Inv.Syntax.BinaryWriteService(BinaryWriter));

        SyntaxFormatter.Start();

        Action(SyntaxFormatter);

        SyntaxFormatter.Stop();
      }
    }
    public static void ReadBinaryStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Extractor> Action)
    {
      using (var BinaryReader = new System.IO.BinaryReader(Stream, Encoding.UTF8, true))
      {
        var SyntaxExtractor = new Inv.Syntax.Extractor(new Inv.Syntax.BinaryReadService(BinaryReader));

        SyntaxExtractor.Start();

        Action(SyntaxExtractor);

        SyntaxExtractor.Stop();
      }
    }

    public static Inv.Syntax.Document WriteSyntaxDocument(Inv.Syntax.Grammar SyntaxGrammar, Action<Inv.Syntax.Formatter> Action)
    {
      var SyntaxDocument = new Inv.Syntax.Document(SyntaxGrammar);

      var SyntaxFormatter = new Inv.Syntax.Formatter(new Inv.Syntax.DocumentWriteService(SyntaxDocument));

      SyntaxFormatter.Start();

      Action(SyntaxFormatter);

      SyntaxFormatter.Stop();

      return SyntaxDocument;
    }
    public static string ConvertSyntaxDocumentToTextString(Inv.Syntax.Document SyntaxDocument)
    {
      using (var StringWriter = new StringWriter())
      {
        var TextService = new Inv.Syntax.TextWriteService(StringWriter, SyntaxDocument.Grammar);

        var WriteContract = (Inv.Syntax.WriteContract)TextService;
        WriteContract.Start();

        foreach (var SyntaxToken in SyntaxDocument.TokenList)
          WriteContract.WriteToken(SyntaxToken);

        WriteContract.Stop();

        StringWriter.Flush();

        return StringWriter.ToString();
      }
    }
    public static Inv.Syntax.Document ConvertTextStringToSyntaxDocument(Inv.Syntax.Grammar SyntaxGrammar, string TextString)
    {
      var SyntaxDocument = new Inv.Syntax.Document(SyntaxGrammar);

      using (var StringReader = new StringReader(TextString))
      {
        var TextService = new Inv.Syntax.TextReadService(StringReader, SyntaxGrammar);

        var ReadContract = (Inv.Syntax.ReadContract)TextService;
        ReadContract.Start();

        // NOTE: adding the return from ReadToken() is currently correct for the text service (FYI: binary service doesn't work this way).
        while (ReadContract.More())
        {
          SyntaxDocument.TokenList.Add(ReadContract.PeekToken());
          ReadContract.SkipToken();
        }

        ReadContract.Stop();
      }

      return SyntaxDocument;
    }
    public static Inv.Syntax.Formatter StartTextSyntaxFormatter(Inv.Syntax.Grammar SyntaxGrammar, StreamWriter StreamWriter)
    {
      var SyntaxFormatter = new Inv.Syntax.Formatter(new Inv.Syntax.TextWriteService(StreamWriter, SyntaxGrammar));

      SyntaxFormatter.Start();

      return SyntaxFormatter;
    }
    public static void StopSyntaxFormatter(Inv.Syntax.Formatter SyntaxFormatter)
    {
      SyntaxFormatter.Stop();
    }
  }

  public interface WriteContract
  {
    void Start();
    void Stop();
    void WriteToken(Inv.Syntax.Token Token);
    string Publish();
  }

  public interface ReadContract
  {
    void Start();
    void Stop();
    bool More();
    Inv.Syntax.Token PeekToken();
    void SkipToken();
    int GetPosition();
    ReadException Interrupt(string Message);
  }

  internal sealed class DocumentWriteService : WriteContract
  {
    public DocumentWriteService(Inv.Syntax.Document ActiveDocument)
    {
      this.ActiveDocument = ActiveDocument;
    }

    void WriteContract.Start()
    {
      ActiveDocument.TokenList.Clear();
      LineCount = 0;
    }
    void WriteContract.Stop()
    {
      LineCount = -1;
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      if (Token.Type == Inv.Syntax.TokenType.Newline)
        LineCount++;

      var CloneToken = new Inv.Syntax.Token();

      switch (Token.Type)
      {
        case Inv.Syntax.TokenType.Newline:
          CloneToken.Newline = Token.Newline;
          break;

        case Inv.Syntax.TokenType.Whitespace:
          CloneToken.Whitespace = Token.Whitespace;
          break;

        case Inv.Syntax.TokenType.Identifier:
          CloneToken.Identifier = Token.Identifier;
          break;

        case Inv.Syntax.TokenType.Keyword:
          CloneToken.Keyword = Token.Keyword;
          break;

        case Inv.Syntax.TokenType.Number:
          CloneToken.Number = Token.Number;
          break;

        case Inv.Syntax.TokenType.String:
          CloneToken.String = Token.String;
          break;

        case Inv.Syntax.TokenType.DateTime:
          CloneToken.DateTime = Token.DateTime;
          break;

        case Inv.Syntax.TokenType.DateTimeLegacy:
          CloneToken.DateTimeLegacy = Token.DateTimeLegacy;
          break;

        case Inv.Syntax.TokenType.Date:
          CloneToken.Date = Token.Date;
          break;

        case Inv.Syntax.TokenType.Time:
          CloneToken.Time = Token.Time;
          break;

        case Inv.Syntax.TokenType.TimePeriod:
          CloneToken.TimePeriod = Token.TimePeriod;
          break;

        case Inv.Syntax.TokenType.TimeSpan:
          CloneToken.TimeSpan = Token.TimeSpan;
          break;

        case Inv.Syntax.TokenType.Base64:
          CloneToken.Base64 = Token.Base64;
          break;

        case Inv.Syntax.TokenType.Hexadecimal:
          CloneToken.Hexadecimal = Token.Hexadecimal;
          break;

        default:
          CloneToken.Type = Token.Type;
          break;
      }

      ActiveDocument.TokenList.Add(CloneToken);
    }
    string WriteContract.Publish()
    {
      return string.Format("Line {0}", LineCount);
    }

    private Inv.Syntax.Document ActiveDocument;
    private int LineCount;
  }

  internal sealed class DocumentReadService : ReadContract
  {
    public DocumentReadService(Inv.Syntax.Document Active)
    {
      this.ActiveDocument = Active;
    }

    void ReadContract.Start()
    {
      CurrentLineIndex = 1;
      CurrentTokenIndex = -1;

      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      CurrentTokenIndex = -1;
      CurrentLineIndex = 0;
    }
    bool ReadContract.More()
    {
      return CurrentTokenIndex < ActiveDocument.TokenList.Count;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      if (CurrentTokenIndex < ActiveDocument.TokenList.Count)
        return ActiveDocument.TokenList[CurrentTokenIndex];
      else
        return null;
    }
    void ReadContract.SkipToken()
    {
      if (CurrentTokenIndex >= ActiveDocument.TokenList.Count)
        throw new Exception(string.Format("Line {0}: expected a token but found the end of the document.", CurrentLineIndex));

      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return CurrentTokenIndex;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      return new ReadException(string.Format("Line {0}: {1}", CurrentLineIndex, Message), CurrentLineIndex);
    }

    private void AdvanceToken()
    {
      CurrentTokenIndex++;

      while (CurrentTokenIndex < ActiveDocument.TokenList.Count && ActiveDocument.TokenList[CurrentTokenIndex].Type == Inv.Syntax.TokenType.Newline)
      {
        CurrentLineIndex++;

        CurrentTokenIndex++;
      }
    }

    private Inv.Syntax.Document ActiveDocument;
    private int CurrentTokenIndex;
    private int CurrentLineIndex;
  }

  internal static class TextFormat
  {
    public const string DateTimeOffsetLegacyFormat = "yyyy-MM-dd HH:mm:ss.ffffff zzz";
    public const string DateTimeOffsetFormat = "yyyy-MM-dd HH:mm:ss.fffffff zzz";
    public const string DateFormat = "yyyy-MM-dd";
    public const string TimeFormat = "HH:mm:ss.fffffff";
    public const string TimeSpanFormat = @"hh\:mm\:ss\.fffffff";
    public const string TimeLegacyFormat = "HH:mm:ss.ffffff";

    public static readonly string[] DateTimeOffsetFormatArray = new string[] { DateTimeOffsetFormat, DateTimeOffsetLegacyFormat };
    public static readonly string[] TimeFormatArray = new string[] { TimeFormat, TimeLegacyFormat, "HH:mm:ss", "HH:mm" };
    public static readonly string[] TimeSpanFormatArray = new string[] { TimeSpanFormat, "hh:mm:ss", "hh:mm" }; 
  }

  public sealed class TextWriteService : WriteContract
  {
    public TextWriteService(System.IO.TextWriter ActiveWriter, Inv.Syntax.Grammar SyntaxGrammar)
    {
      this.ActiveWriter = ActiveWriter;
      this.SyntaxGrammar = SyntaxGrammar;
    }

    void WriteContract.Start()
    {
      GrammarManager = new GrammarManager(SyntaxGrammar);

      LineCount = 1;
    }
    void WriteContract.Stop()
    {
      GrammarManager = null;

      LineCount = 0;
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      if (Token.Type == Inv.Syntax.TokenType.Newline)
      {
        for (var NewlineCount = 0; NewlineCount < Token.Newline; NewlineCount++)
        {
          ActiveWriter.WriteLine();
          LineCount++;
        }
      }
      else
      {
        string Representation;

        switch (Token.Type)
        {
          case Syntax.TokenType.Whitespace:
            Representation = Token.Whitespace;
            break;

          case Syntax.TokenType.Keyword:
            Representation = Token.Keyword;
            break;

          case Syntax.TokenType.Identifier:
            Representation = Token.Identifier;

            // NOTE: we want blank identifiers to be rendered as [] - as this is hack for the Forge Data DSL aggregates.
            if ((Representation.Length == 0 || SyntaxGrammar.AlwaysQuoteIdentifier || !SymbolTable.IdentifierCharacterSet.Conforms(Representation) || GrammarManager.ContainsKeyword(Representation) || SymbolTable.NumberCharacterSet.Contains(Representation[0]) || Representation[0] == '_') && SyntaxGrammar.IdentifierOpenQuote != '\0')
            {
              if (Representation.Contains(SyntaxGrammar.IdentifierOpenQuote, SyntaxGrammar.IdentifierCloseQuote))
                throw new Exception(string.Format("Identifier must not contain the open symbol '{0}' or close symbol '{1}'.", SyntaxGrammar.IdentifierOpenQuote, SyntaxGrammar.IdentifierCloseQuote));

              Representation = SyntaxGrammar.IdentifierOpenQuote + Representation + SyntaxGrammar.IdentifierCloseQuote;
            }
            break;

          case Syntax.TokenType.Comment:
            Representation = "//" + Token.Comment;
            break;

          case Syntax.TokenType.String:
            bool StringVerbatim;
            var TokenString = Token.String;

            if (TokenString == "")
            {
              Representation = "";
              StringVerbatim = false;
            }
            else if (SyntaxGrammar.StringEscape != null)
            {
              if (TokenString.Contains('\n') || TokenString.Contains('\r') || TokenString.Contains('\t') || TokenString.Contains(SyntaxGrammar.StringAroundQuote.Value))
              {
                var StringBuilder = new StringBuilder();

                foreach (var TokenCharacter in TokenString)
                {
                  switch (TokenCharacter)
                  {
                    case '\r':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "r");
                      break;

                    case '\n':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "n");
                      break;

                    case '\t':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "t");
                      break;

                    default:
                      if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape.Value, 1) + new string(SyntaxGrammar.StringAroundQuote.Value, 1));
                      else if (TokenCharacter == SyntaxGrammar.StringEscape)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape.Value, 2));
                      else
                        StringBuilder.Append(TokenCharacter);
                      break;
                  }
                }

                Representation = StringBuilder.ToString();

                StringVerbatim = false;
              }
              else
              {
                Representation = TokenString ?? "";
                StringVerbatim = SyntaxGrammar.StringVerbatim != null && Representation.Contains(SyntaxGrammar.StringEscape.Value);

                if (!StringVerbatim)
                  Representation = Representation.Replace(SyntaxGrammar.StringEscape.ToString(), SyntaxGrammar.StringEscape.ToString() + SyntaxGrammar.StringEscape.ToString());
              }
            }
            else
            {
              Representation = TokenString.Replace(SyntaxGrammar.StringAroundQuote.ToString(), SyntaxGrammar.StringAroundQuote.ToString() + SyntaxGrammar.StringAroundQuote.ToString());
              StringVerbatim = false;
            }

            Representation = SyntaxGrammar.StringAroundQuote + Representation + SyntaxGrammar.StringAroundQuote;

            if (StringVerbatim)
              Representation = SyntaxGrammar.StringVerbatim.ToString() + Representation;
            break;

          case Syntax.TokenType.Number:
            Representation = Token.Number;
            break;

          case Inv.Syntax.TokenType.DateTime:
            Representation = Token.DateTime.ToString(TextFormat.DateTimeOffsetFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.DateTimeLegacy:
            Representation = Token.DateTime.ToString(TextFormat.DateTimeOffsetLegacyFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.Date:
            Representation = Token.Date.ToString(TextFormat.DateFormat);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DatePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.Time:
            Representation = Token.Time.ToString(TextFormat.TimeFormat);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.TimePeriod:
            Representation = Token.TimePeriod.ToString();
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimePeriodPrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.TimeSpan:
            Representation = Token.TimeSpan.ToString(TextFormat.TimeSpanFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimeSpanPrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Syntax.TokenType.Base64:
            Representation = Convert.ToBase64String(Token.Base64.GetBuffer());
            if (SyntaxGrammar.UseBase64Prefix)
              Representation = "0b" + Representation;
            break;

          case Syntax.TokenType.Hexadecimal:
            if (SyntaxGrammar.UseHexadecimalPrefix)
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal("0x");
            else
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal();
            break;

          default:
            Representation = SymbolTable.CharacterTokenTypeIndex.GetByRight(Token.Type.Value).ToString();
            break;
        }

        ActiveWriter.Write(Representation);
      }
    }
    string WriteContract.Publish()
    {
      return string.Format("Line {0}", LineCount);
    }

    private int LineCount;
    private Inv.Syntax.Grammar SyntaxGrammar;
    private GrammarManager GrammarManager;
    private System.IO.TextWriter ActiveWriter;
  }

  public sealed class TextReadService : ReadContract
  {
    public TextReadService(TextReader ActiveReader, Inv.Syntax.Grammar SyntaxGrammar, string FilePath = null)
    {
      this.ActiveReader = ActiveReader;
      this.SyntaxGrammar = SyntaxGrammar;
      this.FilePath = FilePath;
    }

    void ReadContract.Start()
    {
      this.GrammarManager = new GrammarManager(SyntaxGrammar);
      this.TextExtractor = new Inv.TextExtractor(ActiveReader);
      this.LineCount = 1;
      this.Position = -1;

      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      this.GrammarManager = null;

      if (TextExtractor != null)
      {
        TextExtractor.Dispose();
        this.TextExtractor = null;
      }

      this.NextToken = null;
      this.LineCount = 0;
    }
    bool ReadContract.More()
    {
      return NextToken != null;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      return NextToken;
    }
    void ReadContract.SkipToken()
    {
#if DEBUG_WRITE_TOKENS
      switch (NextToken.Type.Value)
      {
        case TokenType.Identifier: Debug.Write(NextToken.Identifier); break;
        case TokenType.Keyword: Debug.Write(NextToken.Keyword); break;
        case TokenType.Number: Debug.Write(NextToken.Number); break;
        case TokenType.String: Debug.Write('\'' + NextToken.String + '\''); break;
        case TokenType.Whitespace: Debug.Write(NextToken.Whitespace); break;
        case TokenType.Newline: for (var Index = 0; Index < NextToken.Newline; Index++) Debug.WriteLine(""); break;
        case TokenType.Hexadecimal: Debug.Write("0x" + NextToken.Hexadecimal); break;
        case TokenType.DateTime: Debug.Write(NextToken.DateTime); break;
        case TokenType.Base64: Debug.Write(NextToken.Base64); break;
        case TokenType.TimeSpan: Debug.Write(NextToken.TimeSpan); break;
        case TokenType.Comment: Debug.Write("-- " + NextToken.Comment); break;
        default:
          Debug.WriteLine(SymbolTable.CharacterTokenTypeIndex.GetByRight(NextToken.Type.Value).ToString());
          break;
      }
#endif

      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return Position;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      if (FilePath != null)
        return new ReadException(string.Format("{0} Line {1}: {2}", Path.GetFileName(FilePath), LineCount, Message), LineCount);
      else
        return new ReadException(string.Format("Line {0}: {1}", LineCount, Message), LineCount);
    }

    private void AdvanceToken()
    {
      NextToken = null;

      while (NextToken == null && TextExtractor.More())
      {
        var TokenCharacter = TextExtractor.NextCharacter();

        if (SymbolTable.WhitespaceCharacterSet.ContainsValue(TokenCharacter))
        {
          var WhitespaceString = TextExtractor.ReadWhileCharacterSet(SymbolTable.WhitespaceCharacterSet);
          Position += WhitespaceString.Length;

          foreach (var WhitespaceChar in WhitespaceString)
          {
            if (WhitespaceChar == '\n')
              LineCount++;
          }

          if (SyntaxGrammar.IgnoreWhitespace)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Inv.Syntax.Token();
            NextToken.Whitespace = WhitespaceString;
          }
        }
        else if (TextExtractor.NextIsString(SyntaxGrammar.SingleLineCommentPrefix))
        {
          TextExtractor.ReadString(SyntaxGrammar.SingleLineCommentPrefix);
          Position += SyntaxGrammar.SingleLineCommentPrefix.Length;

          var SingleLineComment = TextExtractor.ReadUntilCharacterSet(SymbolTable.VerticalWhitespaceCharacterSet);
          Position += SingleLineComment.Length;

          if (SyntaxGrammar.IgnoreComment)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Inv.Syntax.Token();
            NextToken.Comment = SingleLineComment;
          }
        }
        else if (TextExtractor.NextIsString(SyntaxGrammar.MultiLineCommentStart))
        {
          TextExtractor.ReadString(SyntaxGrammar.MultiLineCommentStart);
          Position += SyntaxGrammar.MultiLineCommentStart.Length;

          var Count = 0;
          do
          {
            var MultiLineComment = TextExtractor.ReadUntilString(SyntaxGrammar.MultiLineCommentFinish);
            Position += MultiLineComment.Length;

            Count += MultiLineComment.Split(SyntaxGrammar.MultiLineCommentStart, StringSplitOptions.RemoveEmptyEntries).Length - 1;

            foreach (var MultiLineIndex in MultiLineComment)
            {
              if (MultiLineIndex == '\n')
                LineCount++;
            }

            if (SyntaxGrammar.IgnoreComment)
            {
              NextToken = null;
            }
            else
            {
              NextToken = new Inv.Syntax.Token();
              NextToken.Comment = MultiLineComment;
            }

            TextExtractor.ReadString(SyntaxGrammar.MultiLineCommentFinish);
            Position += SyntaxGrammar.MultiLineCommentFinish.Length;
            Count--;

          } while (Count >= 0);
        }
        else
        {
          NextToken = new Inv.Syntax.Token();

          if (SymbolTable.NumberCharacterSet.ContainsValue(TokenCharacter))
          {
            var NumberValue = TextExtractor.ReadWhileCharacterSet(SymbolTable.NumberCharacterSet);
            Position += NumberValue.Length;

            if (SyntaxGrammar.UseHexadecimalPrefix && NumberValue == "0" && Char.ToLower(TextExtractor.NextCharacter()) == 'x')
            {
              TextExtractor.ReadCharacter();
              Position++;

              var HexadecimalString = TextExtractor.ReadWhileCharacterSet(SymbolTable.HexadecimalCharacterSet);
              var HexadecimalLength = HexadecimalString.Length;
              var HexadecimalBuffer = new byte[HexadecimalLength / 2];
              Position += HexadecimalLength;

              for (var HexadecimalIndex = 0; HexadecimalIndex < HexadecimalBuffer.Length; HexadecimalIndex++)
                HexadecimalBuffer[HexadecimalIndex] = Convert.ToByte(HexadecimalString.Substring(HexadecimalIndex * 2, 2), 16);

              NextToken.Hexadecimal = new Inv.Binary(HexadecimalBuffer, "");
            }
            else if (SyntaxGrammar.UseBase64Prefix && NumberValue == "0" && Char.ToLower(TextExtractor.NextCharacter()) == 'b')
            {
              TextExtractor.ReadCharacter();
              Position++;

              var Base64String = TextExtractor.ReadWhileCharacterSet(SymbolTable.Base64CharacterSet);
              Position += Base64String.Length;
              var Base64Buffer = Convert.FromBase64String(Base64String);

              NextToken.Base64 = new Inv.Binary(Base64Buffer, "");
            }
            else
            {
              NextToken.Number = NumberValue;
            }
          }
          else if (TokenCharacter != '_' && SymbolTable.IdentifierCharacterSet.ContainsValue(TokenCharacter))
          {
            // NOTE: identifiers cannot start with an underscore but can include an underscore.

            var IdentifierString = TextExtractor.ReadWhileCharacterSet(SymbolTable.IdentifierCharacterSet);
            Position += IdentifierString.Length;

            if (GrammarManager.ContainsKeyword(IdentifierString))
            {
              if (SyntaxGrammar.KeywordCaseSensitive)
                NextToken.Keyword = IdentifierString;
              else
                NextToken.Keyword = IdentifierString.ToLower();
            }
            else
            {
              NextToken.Identifier = IdentifierString;
            }
          }
          else if (SyntaxGrammar.StringVerbatim != null && TokenCharacter == SyntaxGrammar.StringVerbatim)
          {
            TextExtractor.ReadCharacter(SyntaxGrammar.StringVerbatim.Value);
            TextExtractor.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position += 2;

            NextToken.String = TextExtractor.ReadUntilCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position += NextToken.String.Length;

            TextExtractor.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position++;
          }
          else if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
          {
            TextExtractor.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position++;

            var StringBuilder = new StringBuilder();

            do
            {
              if (TextExtractor.NextCharacter() == SyntaxGrammar.StringAroundQuote)
              {
                TextExtractor.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
                Position++;

                if (SyntaxGrammar.StringEscape == null && TextExtractor.NextCharacter() == SyntaxGrammar.StringAroundQuote)
                {
                  TextExtractor.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
                  Position++;

                  StringBuilder.Append(SyntaxGrammar.StringAroundQuote.Value);
                }
                else
                {
                  break;
                }
              }
              else if (TextExtractor.More())
              {
                var StringEntry = TextExtractor.ReadCharacter();
                Position++;

                switch (StringEntry)
                {
                  case '\r':
                  case '\n':
                    if (!SyntaxGrammar.AllowMultiLineString)
                      throw new Exception(string.Format("String {0}{1}{0} was not correctly terminated.", SyntaxGrammar.StringAroundQuote, StringBuilder));

                    if (StringEntry == '\n')
                      LineCount++;

                    StringBuilder.Append(StringEntry);
                    break;

                  default:
                    if (StringEntry == SyntaxGrammar.StringEscape)
                    {
                      StringEntry = TextExtractor.ReadCharacter();
                      Position++;

                      if (StringEntry == SyntaxGrammar.StringEscape)
                      {
                        StringBuilder.Append(StringEntry);
                      }
                      else
                      {
                        switch (StringEntry)
                        {
                          case 'r':
                            StringBuilder.Append('\r');
                            break;

                          case 'n':
                            StringBuilder.Append('\n');
                            break;

                          case 't':
                            StringBuilder.Append('\t');
                            break;

                          default:
                            if (StringEntry == SyntaxGrammar.StringAroundQuote)
                              StringBuilder.Append(StringEntry);
                            else
                              StringBuilder.Append("\\" + StringEntry);
                            break;
                        }
                      }
                    }
                    else
                    {
                      StringBuilder.Append(StringEntry);
                    }
                    break;
                }
              }
            }
            while (TextExtractor.More());

            NextToken.String = StringBuilder.ToString();
          }
          else if (TokenCharacter == SyntaxGrammar.IdentifierOpenQuote && SyntaxGrammar.IdentifierOpenQuote != '\0')
          {
            TextExtractor.ReadCharacter(SyntaxGrammar.IdentifierOpenQuote);
            Position++;

            var IdentifierString = TextExtractor.ReadUntilCharacter(SyntaxGrammar.IdentifierCloseQuote);
            Position += IdentifierString.Length;

            TextExtractor.ReadCharacter(SyntaxGrammar.IdentifierCloseQuote);
            Position++;

            if (SyntaxGrammar.DateTimePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.DateTimePrefix))
              NextToken.DateTime = DateTimeOffset.ParseExact(IdentifierString.Substring(SyntaxGrammar.DateTimePrefix.Length), TextFormat.DateTimeOffsetFormatArray, CultureInfo.InvariantCulture, DateTimeStyles.None);
            else if (SyntaxGrammar.DatePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.DatePrefix))
              NextToken.Date = Inv.Date.ParseExact(IdentifierString.Substring(SyntaxGrammar.DatePrefix.Length), TextFormat.DateFormat);
            else if (SyntaxGrammar.TimePeriodPrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimePeriodPrefix))
              NextToken.TimePeriod = Inv.TimePeriod.Parse(IdentifierString.Substring(SyntaxGrammar.TimePeriodPrefix.Length));
            else if (SyntaxGrammar.TimePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimePrefix))
              NextToken.Time = Inv.Time.ParseExact(IdentifierString.Substring(SyntaxGrammar.TimePrefix.Length), TextFormat.TimeFormatArray);
            else if (SyntaxGrammar.TimeSpanPrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimeSpanPrefix))
              NextToken.TimeSpan = TimeSpan.ParseExact(IdentifierString.Substring(SyntaxGrammar.TimeSpanPrefix.Length), TextFormat.TimeSpanFormatArray, CultureInfo.InvariantCulture);
            else
              NextToken.Identifier = IdentifierString;
          }
          else
          {
            TextExtractor.ReadCharacter(TokenCharacter);
            Position++;

            NextToken.Type = SymbolTable.CharacterTokenTypeIndex.GetByLeft(TokenCharacter);
          }
        }
      }
    }

    private GrammarManager GrammarManager;
    private Inv.Syntax.Grammar SyntaxGrammar;
    private Inv.TextExtractor TextExtractor;
    private TextReader ActiveReader;
    private int LineCount;
    private int Position;
    private Inv.Syntax.Token NextToken;
    private string FilePath;
  }

  internal static class BinaryFormat
  {
    static BinaryFormat()
    {
      TokenTagDictionary = new Dictionary<Inv.Syntax.TokenType, byte>()
      {
        { Inv.Syntax.TokenType.Whitespace, WhitespaceTag },
        { Inv.Syntax.TokenType.Newline, NewlineTag },
        { Inv.Syntax.TokenType.Keyword, KeywordStartTag },
        { Inv.Syntax.TokenType.Identifier, IdentifierStartTag },
        { Inv.Syntax.TokenType.String, StringTag },
        { Inv.Syntax.TokenType.Number, NumberTag },
        { Inv.Syntax.TokenType.DateTime, DateTimeTag },
        { Inv.Syntax.TokenType.Date, DateTag },
        { Inv.Syntax.TokenType.Time, TimeTag },
        { Inv.Syntax.TokenType.TimePeriod, TimePeriodTag },
        { Inv.Syntax.TokenType.TimeSpan, TimeSpanTag },
        { Inv.Syntax.TokenType.Comment, CommentTag },
        { Inv.Syntax.TokenType.Base64, Base64Tag },
        { Inv.Syntax.TokenType.Hexadecimal, HexadecimalTag },
        { Inv.Syntax.TokenType.OpenRound, 20 },
        { Inv.Syntax.TokenType.CloseRound, 21 },
        { Inv.Syntax.TokenType.OpenAngle, 22 },
        { Inv.Syntax.TokenType.CloseAngle, 23 },
        { Inv.Syntax.TokenType.OpenBrace, 24 },
        { Inv.Syntax.TokenType.CloseBrace, 25 },
        { Inv.Syntax.TokenType.Comma, 26 },
        { Inv.Syntax.TokenType.Period, 27 },
        { Inv.Syntax.TokenType.Semicolon, 28 },
        { Inv.Syntax.TokenType.Colon, 29 },
        { Inv.Syntax.TokenType.EqualSign, 30 },
        { Inv.Syntax.TokenType.ExclamationPoint, 31 },
        { Inv.Syntax.TokenType.QuestionMark, 32 },
        { Inv.Syntax.TokenType.Ampersand, 33 },
        { Inv.Syntax.TokenType.Pipe, 34 },
        { Inv.Syntax.TokenType.Plus, 35 },
        { Inv.Syntax.TokenType.Minus, 36 },
        { Inv.Syntax.TokenType.Percent, 37 },
        { Inv.Syntax.TokenType.Asterisk, 38 },
        { Inv.Syntax.TokenType.BackSlash, 39 },
        { Inv.Syntax.TokenType.ForwardSlash, 40 },
        { Inv.Syntax.TokenType.At, 41 },
        { Inv.Syntax.TokenType.Caret, 42 },
        { Inv.Syntax.TokenType.Tilde, 43 },
        { Inv.Syntax.TokenType.DollarSign, 44 },
        { Inv.Syntax.TokenType.Hash, 45 },
        { Inv.Syntax.TokenType.SingleQuote, 46 },
        { Inv.Syntax.TokenType.DoubleQuote, 48 },
        { Inv.Syntax.TokenType.BackQuote, 49 },
        { Inv.Syntax.TokenType.OpenSquare, 50 },
        { Inv.Syntax.TokenType.CloseSquare, 51 },
        { Inv.Syntax.TokenType.Underscore, 53 },
        { Inv.Syntax.TokenType.DateTimeLegacy, 54 },
      };

      if (Inv.Assert.IsEnabled)
      {
        foreach (var TokenTypeValue in EnumHelper.GetEnumerable<Inv.Syntax.TokenType>())
          Inv.Assert.Check(TokenTagDictionary.ContainsKey(TokenTypeValue), "Inv.Syntax.TokenType not handled: {0}", TokenTypeValue);
      }

      // NOTE: this will fail if there are any duplicate tags.
      TagTokenDictionary = new Dictionary<byte, Inv.Syntax.TokenType>();
      foreach (var TokenTagEntry in TokenTagDictionary)
        TagTokenDictionary.Add(TokenTagEntry.Value, TokenTagEntry.Key);
    }

    internal const byte StartTag = 0x00;
    internal const byte WhitespaceTag = 1;
    internal const byte NewlineTag = 2;
    internal const byte KeywordStartTag = 3;
    internal const byte KeywordRepeatTag = 4;
    internal const byte IdentifierStartTag = 5;
    internal const byte IdentifierRepeatTag = 6;
    internal const byte StringTag = 7;
    internal const byte NumberTag = 8;
    internal const byte DateTimeTag = 9;
    internal const byte DateTag = 10;
    internal const byte TimeTag = 11;
    internal const byte TimePeriodTag = 52;
    internal const byte TimeSpanTag = 12;
    internal const byte CommentTag = 13;
    internal const byte HexadecimalTag = 14;
    internal const byte Base64Tag = 15;
    internal const byte StopTag = 0xFF;

    internal static byte ResolveTag(Inv.Syntax.TokenType TokenType)
    {
      return TokenTagDictionary[TokenType];
    }
    internal static Inv.Syntax.TokenType ResolveToken(byte TokenTag)
    {
      return TagTokenDictionary[TokenTag];
    }

    private static Dictionary<Inv.Syntax.TokenType, byte> TokenTagDictionary;
    private static Dictionary<byte, Inv.Syntax.TokenType> TagTokenDictionary;
  }

  public sealed class BinaryWriteService : WriteContract
  {
    public BinaryWriteService(System.IO.BinaryWriter ActiveWriter)
    {
      this.ActiveWriter = ActiveWriter;
      this.KeywordDictionary = new Dictionary<string, int>();
      this.IdentifierDictionary = new Dictionary<string, int>();
    }

    void WriteContract.Start()
    {
      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();

      ActiveWriter.Write(BinaryFormat.StartTag);
    }
    void WriteContract.Stop()
    {
      ActiveWriter.Write(BinaryFormat.StopTag);

      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      switch (Token.Type)
      {
        case Inv.Syntax.TokenType.Whitespace:
          //ActiveWriter.Write(BinaryTag.Whitespace);
          //ActiveWriter.Write(Token.Whitespace);
          break;

        case Inv.Syntax.TokenType.Newline:
          //ActiveWriter.Write(BinaryFormat.NewlineTag);
          //ActiveWriter.Write(Token.Newline);
          break;

        case Inv.Syntax.TokenType.Comment:
          //ActiveWriter.Write(BinaryFormat.CommentTag);
          //ActiveWriter.Write(Token.Comment);
          break;

        case Inv.Syntax.TokenType.Identifier:
          int IdentifierID;
          var TokenIdentifier = Token.Identifier;

          if (IdentifierDictionary.TryGetValue(TokenIdentifier, out IdentifierID))
          {
            ActiveWriter.Write(BinaryFormat.IdentifierRepeatTag);
            ActiveWriter.Write(IdentifierID);
          }
          else
          {
            ActiveWriter.Write(BinaryFormat.IdentifierStartTag);
            ActiveWriter.Write(TokenIdentifier);

            IdentifierDictionary.Add(TokenIdentifier, IdentifierDictionary.Count);
          }
          break;

        case Inv.Syntax.TokenType.Keyword:
          int KeywordID;
          var TokenKeyword = Token.Keyword;

          if (KeywordDictionary.TryGetValue(TokenKeyword, out KeywordID))
          {
            ActiveWriter.Write(BinaryFormat.KeywordRepeatTag);
            ActiveWriter.Write(KeywordID);
          }
          else
          {
            ActiveWriter.Write(BinaryFormat.KeywordStartTag);
            ActiveWriter.Write(TokenKeyword);

            KeywordDictionary.Add(TokenKeyword, KeywordDictionary.Count);
          }
          break;

        case Inv.Syntax.TokenType.String:
          ActiveWriter.Write(BinaryFormat.StringTag);
          ActiveWriter.Write(Token.String);
          break;

        case Inv.Syntax.TokenType.Number:
          ActiveWriter.Write(BinaryFormat.NumberTag);
          ActiveWriter.Write(Token.Number);
          break;

        case Inv.Syntax.TokenType.DateTime:
          ActiveWriter.Write(BinaryFormat.DateTimeTag);
          ActiveWriter.Write(Token.DateTime.DateTime.Ticks);
          ActiveWriter.Write((short)Token.DateTime.Offset.TotalMinutes);
          break;

        case Inv.Syntax.TokenType.DateTimeLegacy:
          ActiveWriter.Write(BinaryFormat.DateTimeTag);
          ActiveWriter.Write(Token.DateTimeLegacy.DateTime.Ticks);
          ActiveWriter.Write((short)Token.DateTimeLegacy.Offset.TotalMinutes);
          break;

        case Inv.Syntax.TokenType.Date:
          ActiveWriter.Write(BinaryFormat.DateTag);
          ActiveWriter.Write(Token.Date.ToDateTime().Ticks);
          break;

        case Inv.Syntax.TokenType.Time:
          ActiveWriter.Write(BinaryFormat.TimeTag);
          ActiveWriter.Write(Token.Time.ToDateTime().Ticks);
          break;

        case Inv.Syntax.TokenType.TimePeriod:
          ActiveWriter.Write(BinaryFormat.TimePeriodTag);
          ActiveWriter.Write(Token.TimePeriod.ToString());
          break;

        case Inv.Syntax.TokenType.TimeSpan:
          ActiveWriter.Write(BinaryFormat.TimeSpanTag);
          ActiveWriter.Write(Token.TimeSpan.Ticks);
          break;

        case Inv.Syntax.TokenType.Hexadecimal:
          var HexadecimalBuffer = Token.Hexadecimal.GetBuffer();

          ActiveWriter.Write(BinaryFormat.HexadecimalTag);
          ActiveWriter.Write(HexadecimalBuffer.Length);
          ActiveWriter.Write(HexadecimalBuffer);
          break;

        case Inv.Syntax.TokenType.Base64:
          var Base64Buffer = Token.Base64.GetBuffer();

          ActiveWriter.Write(BinaryFormat.Base64Tag);
          ActiveWriter.Write(Base64Buffer.Length);
          ActiveWriter.Write(Base64Buffer);
          break;

        default:
          ActiveWriter.Write(BinaryFormat.ResolveTag(Token.Type.Value));
          break;
      }
    }
    string WriteContract.Publish()
    {
      return string.Format("Byte {0} of {1}", ActiveWriter.BaseStream.Position, ActiveWriter.BaseStream.Length);
    }

    private System.IO.BinaryWriter ActiveWriter;
    private Dictionary<string, int> KeywordDictionary;
    private Dictionary<string, int> IdentifierDictionary;
  }

  public sealed class BinaryReadService : ReadContract
  {
    public BinaryReadService(System.IO.BinaryReader ActiveReader)
    {
      this.ActiveReader = ActiveReader;
      this.ActiveToken = new Inv.Syntax.Token();
      this.KeywordDictionary = new Dictionary<int, string>();
      this.IdentifierDictionary = new Dictionary<int, string>();
    }

    void ReadContract.Start()
    {
      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();

      if (ActiveReader.ReadByte() != BinaryFormat.StartTag)
        throw new Exception("Expected a start tag.");

      NextToken = ActiveToken;
      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      if (NextToken != null)
      {
        if (ActiveReader.ReadByte() != BinaryFormat.StopTag)
          throw new Exception("Expected a stop tag.");

        NextToken = null;
      }

      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();
    }
    bool ReadContract.More()
    {
      return NextToken != null;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      return NextToken;
    }
    void ReadContract.SkipToken()
    {
      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return (int)ActiveReader.BaseStream.Position;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      var Position = ActiveReader.BaseStream.Position;

      return new ReadException(string.Format("Byte {0}: {1}", Position, Message), Position);
    }

    private void AdvanceToken()
    {
      var NextTag = ActiveReader.ReadByte();

      switch (NextTag)
      {
        case BinaryFormat.StartTag:
          throw new Exception("Unexpected start tag.");

        case BinaryFormat.StopTag:
          NextToken = null;
          break;

        default:
          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(NextToken == ActiveToken, "Stream must not be finished.");

          NextToken.Type = null;

          switch (NextTag)
          {
            case BinaryFormat.WhitespaceTag:
              NextToken.Whitespace = ActiveReader.ReadString();
              break;

            case BinaryFormat.KeywordStartTag:
              var TokenKeyword = ActiveReader.ReadString();

              NextToken.Keyword = TokenKeyword;
              KeywordDictionary.Add(KeywordDictionary.Count, TokenKeyword);
              break;

            case BinaryFormat.KeywordRepeatTag:
              NextToken.Keyword = KeywordDictionary[ActiveReader.ReadInt32()];
              break;

            case BinaryFormat.IdentifierStartTag:
              var TokenIdentifier = ActiveReader.ReadString();

              NextToken.Identifier = TokenIdentifier;
              IdentifierDictionary.Add(IdentifierDictionary.Count, TokenIdentifier);
              break;

            case BinaryFormat.IdentifierRepeatTag:
              NextToken.Identifier = IdentifierDictionary[ActiveReader.ReadInt32()];
              break;

            case BinaryFormat.StringTag:
              NextToken.String = ActiveReader.ReadString();
              break;

            case BinaryFormat.NumberTag:
              NextToken.Number = ActiveReader.ReadString();
              break;

            case BinaryFormat.DateTimeTag:
              var DateTimeTicks = ActiveReader.ReadInt64();
              var DateTimeOffsetTicks = ActiveReader.ReadInt16();

              NextToken.DateTime = new DateTimeOffset(DateTimeTicks, TimeSpan.FromMinutes(DateTimeOffsetTicks));
              break;

            case BinaryFormat.DateTag:
              var DateTicks = ActiveReader.ReadInt64();

              NextToken.Date = new DateTime(DateTicks).AsDate();
              break;

            case BinaryFormat.TimeTag:
              var TimeTicks = ActiveReader.ReadInt64();

              NextToken.Time = new Inv.Time(new DateTime(TimeTicks));
              break;

            case BinaryFormat.TimePeriodTag:
              var TimePeriodText = ActiveReader.ReadString();

              NextToken.TimePeriod = Inv.TimePeriod.Parse(TimePeriodText);
              break;

            case BinaryFormat.TimeSpanTag:
              NextToken.TimeSpan = new TimeSpan(ActiveReader.ReadInt64());
              break;

            case BinaryFormat.HexadecimalTag:
              var HexadecimalLength = ActiveReader.ReadInt32();
              NextToken.Hexadecimal = new Inv.Binary(ActiveReader.ReadBytes(HexadecimalLength), "");
              break;

            case BinaryFormat.Base64Tag:
              var Base64Length = ActiveReader.ReadInt32();
              NextToken.Base64 = new Inv.Binary(ActiveReader.ReadBytes(Base64Length), "");
              break;

            default:
              NextToken.Type = BinaryFormat.ResolveToken(NextTag);
              break;
          }
          break;
      }
    }

    private System.IO.BinaryReader ActiveReader;
    private Inv.Syntax.Token NextToken;
    private Inv.Syntax.Token ActiveToken;
    private Dictionary<int, string> KeywordDictionary;
    private Dictionary<int, string> IdentifierDictionary;
  }

  public sealed class Grammar
  {
    public Grammar()
    {
      this.Keyword = new Inv.DistinctList<string>();
    }

    public Inv.DistinctList<string> Keyword { get; private set; }
    public bool IgnoreWhitespace { get; set; }
    public bool IgnoreComment { get; set; }
    public bool KeywordCaseSensitive { get; set; }
    public bool AllowMultiLineString { get; set; }
    public bool UseHexadecimalPrefix { get; set; }
    public bool UseBase64Prefix { get; set; }
    public string SingleLineCommentPrefix { get; set; }
    public string MultiLineCommentStart { get; set; }
    public string MultiLineCommentFinish { get; set; }
    public char? StringAroundQuote { get; set; }
    public char? StringEscape { get; set; }
    public char? StringVerbatim { get; set; }
    public char IdentifierOpenQuote { get; set; }
    public char IdentifierCloseQuote { get; set; }
    public bool AlwaysQuoteIdentifier { get; set; }
    public string DateTimePrefix { get; set; }
    public string TimeSpanPrefix { get; set; }
    public string TimePeriodPrefix { get; set; }
    public string DatePrefix { get; set; }
    public string TimePrefix { get; set; }
  }

  public enum TokenType
  {
    Newline,
    Whitespace,
    Comment,
    Identifier,
    Keyword,
    Number,
    String,
    Date,
    DateTime,
    DateTimeLegacy,
    Time,
    TimePeriod,
    TimeSpan,
    Hexadecimal,
    Base64,
    Colon,
    Semicolon,
    Comma,
    Period,
    EqualSign,
    Plus,
    Minus,
    Asterisk,
    DollarSign,
    Caret,
    Tilde,
    ExclamationPoint,
    QuestionMark,
    Pipe,
    Ampersand,
    Percent,
    ForwardSlash,
    BackSlash,
    At,
    OpenRound,
    CloseRound,
    OpenBrace,
    CloseBrace,
    OpenAngle,
    CloseAngle,
    Hash,
    SingleQuote,
    DoubleQuote,
    BackQuote,
    OpenSquare,
    CloseSquare,
    Underscore
  }

  public sealed class Token
  {
    public Token()
    {
      this.Backing_Type = new Inv.Choice<object, Inv.Syntax.TokenType>(Sharing_TypeClassList);
    }

    public Inv.Syntax.TokenType? Type
    {
      get { return Backing_Type.Index; }
      set { Backing_Type.Index = value; }
    }
    public long Newline
    {
      get { return Backing_Type.Retrieve<long>(Inv.Syntax.TokenType.Newline); }
      set { Backing_Type.Store<long>(Inv.Syntax.TokenType.Newline, value); }
    }
    public string Whitespace
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Whitespace); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Whitespace, value); }
    }
    public string Comment
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Comment); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Comment, value); }
    }
    public string Identifier
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Identifier); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Identifier, value); }
    }
    public string Keyword
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Keyword); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Keyword, value); }
    }
    public string Number
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Number); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Number, value); }
    }
    public string String
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.String); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.String, value); }
    }
    public Inv.Date Date
    {
      get { return Backing_Type.Retrieve<Inv.Date>(Inv.Syntax.TokenType.Date); }
      set { Backing_Type.Store<Inv.Date>(Inv.Syntax.TokenType.Date, value); }
    }
    public DateTimeOffset DateTime
    {
      get { return Backing_Type.Retrieve<DateTimeOffset>(Inv.Syntax.TokenType.DateTime); }
      set { Backing_Type.Store<DateTimeOffset>(Inv.Syntax.TokenType.DateTime, value); }
    }
    public DateTimeOffset DateTimeLegacy
    {
      get { return Backing_Type.Retrieve<DateTimeOffset>(Inv.Syntax.TokenType.DateTimeLegacy); }
      set { Backing_Type.Store<DateTimeOffset>(Inv.Syntax.TokenType.DateTimeLegacy, value); }
    }
    public Inv.Time Time
    {
      get { return Backing_Type.Retrieve<Inv.Time>(Inv.Syntax.TokenType.Time); }
      set { Backing_Type.Store<Inv.Time>(Inv.Syntax.TokenType.Time, value); }
    }
    public Inv.TimePeriod TimePeriod
    {
      get { return Backing_Type.Retrieve<Inv.TimePeriod>(Inv.Syntax.TokenType.TimePeriod); }
      set { Backing_Type.Store<Inv.TimePeriod>(Inv.Syntax.TokenType.TimePeriod, value); }
    }
    public TimeSpan TimeSpan
    {
      get { return Backing_Type.Retrieve<TimeSpan>(Inv.Syntax.TokenType.TimeSpan); }
      set { Backing_Type.Store<TimeSpan>(Inv.Syntax.TokenType.TimeSpan, value); }
    }
    public Inv.Binary Hexadecimal
    {
      get { return Backing_Type.Retrieve<Inv.Binary>(Inv.Syntax.TokenType.Hexadecimal); }
      set { Backing_Type.Store<Inv.Binary>(Inv.Syntax.TokenType.Hexadecimal, value); }
    }
    public Inv.Binary Base64
    {
      get { return Backing_Type.Retrieve<Inv.Binary>(Inv.Syntax.TokenType.Base64); }
      set { Backing_Type.Store<Inv.Binary>(Inv.Syntax.TokenType.Base64, value); }
    }

    private Inv.Choice<object, Inv.Syntax.TokenType> Backing_Type;

    private static readonly Type[] Sharing_TypeClassList = new Type[] { typeof(long), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(Inv.Date), typeof(DateTimeOffset), typeof(DateTimeOffset), typeof(Inv.Time), typeof(Inv.TimePeriod), typeof(TimeSpan), typeof(Inv.Binary), typeof(Inv.Binary), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null };
  }

  public sealed class Document
  {
    public Document(Grammar Grammar)
    {
      this.Grammar = Grammar;
      this.TokenList = new Inv.DistinctList<Token>();
    }

    public Grammar Grammar { get; private set; }
    public Inv.DistinctList<Token> TokenList { get; private set; }
  }

  public sealed class GrammarManager
  {
    public GrammarManager(Inv.Syntax.Grammar SyntaxGrammar)
    {
      if (SyntaxGrammar.KeywordCaseSensitive)
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCulture);
      else
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);

      foreach (var Keyword in SyntaxGrammar.Keyword)
      {
        if (!KeywordHashSet.Add(Keyword))
          throw new System.Exception("Duplicate keyword: " + Keyword);
      }
    }

    public bool ContainsKeyword(string Keyword)
    {
      return KeywordHashSet.Contains(Keyword);
    }

    private HashSet<string> KeywordHashSet;
  }

  public sealed class Formatter
  {
    public Formatter(Inv.Syntax.WriteContract WriteContract)
    {
      this.WriteContract = WriteContract;
      this.CacheToken = new Inv.Syntax.Token();
    }

    internal Inv.Syntax.WriteContract WriteContract { get; private set; }

    public void Start()
    {
      WriteContract.Start();

      IndentLevel = 0;
    }
    public void Stop()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel == 0, "IndentLevel must return to zero on finish.");

      WriteContract.Stop();
    }

    public void IncreaseIndent()
    {
      IndentLevel++;
    }
    public void DecreaseIndent()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel > 0, "IndentLevel must be greater than zero.");

      IndentLevel--;
    }
    public void WriteToken(Inv.Syntax.TokenType SyntaxTokenType)
    {
      CacheToken.Type = SyntaxTokenType;

      WriteContract.WriteToken(CacheToken);
    }
    public void WriteIdentifierChainArray(string[] IdentifierArray, Inv.Syntax.TokenType SeparatorTokenType)
    {
      var IdentifierCount = IdentifierArray.Length;

      for (var IdentifierIndex = 0; IdentifierIndex < IdentifierCount; IdentifierIndex++)
      {
        var Identifier = IdentifierArray[IdentifierIndex];

        if (Identifier != "")
          WriteIdentifier(Identifier);

        if (IdentifierIndex != IdentifierCount - 1)
          WriteToken(SeparatorTokenType);
      }
    }
    public void WriteIdentifier(string IdentifierValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(IdentifierValue, "IdentifierValue");

      CacheToken.Identifier = IdentifierValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteKeyword(string KeywordValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(KeywordValue, "KeywordValue");

      CacheToken.Keyword = KeywordValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteString(string StringValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(StringValue, "StringValue");

      CacheToken.String = StringValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteCharacter(char CharacterValue)
    {
      WriteString(CharacterValue.ToString());
    }
    public void WriteGuid(Guid GuidValue)
    {
      WriteString(GuidValue.ToString());
    }
    public void WriteInteger(long IntegerValue)
    {
      WriteNumber(IntegerValue.ToString());
    }
    public void WriteDecimal(decimal DecimalValue)
    {
      WriteNumber(DecimalValue.ToString());
    }
    public void WriteReal(double RealValue)
    {
      WriteNumber(RealValue.ToString());
    }
    public void WriteBoolean(bool BooleanValue)
    {
      switch (BooleanValue)
      {
        case true:
          WriteIdentifier("true");
          break;

        case false:
          WriteIdentifier("false");
          break;

        default:
          throw new WriteException("Unhandled boolean value.");
      }
    }
    public void WriteDateTimeOffset(DateTimeOffset DateTimeValue)
    {
      CacheToken.DateTime = DateTimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteDateTimeOffsetLegacy(DateTimeOffset DateTimeValue)
    {
      CacheToken.DateTimeLegacy = DateTimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteDate(Inv.Date DateValue)
    {
      CacheToken.Date = DateValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTime(Inv.Time TimeValue)
    {
      CacheToken.Time = TimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTimePeriod(Inv.TimePeriod TimePeriodValue)
    {
      CacheToken.TimePeriod = TimePeriodValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTimeSpan(TimeSpan TimeSpanValue)
    {
      CacheToken.TimeSpan = TimeSpanValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteBase64(byte[] BinaryValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(BinaryValue, "BinaryValue");

      CacheToken.Base64 = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteHexadecimal(byte[] BinaryValue)
    {
      Inv.Assert.CheckNotNull(BinaryValue, "BinaryValue");

      CacheToken.Hexadecimal = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteColour(Inv.Colour Colour)
    {
      var KnownName = Colour.Name;

      if (KnownName == Colour.Hex)
        WriteHexadecimal(BitConverter.GetBytes(Colour.GetArgb()).Reverse().ToArray());
      else
        WriteIdentifier(KnownName);
    }
    public void WriteWhitespace(string WhitespaceValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(WhitespaceValue, "WhitespaceValue");

      CacheToken.Whitespace = WhitespaceValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteOpenBrace()
    {
      WriteToken(Inv.Syntax.TokenType.OpenBrace);
    }
    public void WriteCloseBrace()
    {
      WriteToken(Inv.Syntax.TokenType.CloseBrace);
    }
    public void WriteOpenSquare()
    {
      WriteToken(Inv.Syntax.TokenType.OpenSquare);
    }
    public void WriteCloseSquare()
    {
      WriteToken(Inv.Syntax.TokenType.CloseSquare);
    }
    public void WriteOpenRound()
    {
      WriteToken(Inv.Syntax.TokenType.OpenRound);
    }
    public void WriteCloseRound()
    {
      WriteToken(Inv.Syntax.TokenType.CloseRound);
    }
    public void WritePercent()
    {
      WriteToken(Inv.Syntax.TokenType.Percent);
    }
    public void WriteOpenAngle()
    {
      WriteToken(Inv.Syntax.TokenType.OpenAngle);
    }
    public void WriteCloseAngle()
    {
      WriteToken(Inv.Syntax.TokenType.CloseAngle);
    }
    public void WriteSemicolon()
    {
      WriteToken(Inv.Syntax.TokenType.Semicolon);
    }
    public void WriteColon()
    {
      WriteToken(Inv.Syntax.TokenType.Colon);
    }
    public void WriteComma()
    {
      WriteToken(Inv.Syntax.TokenType.Comma);
    }
    public void WritePeriod()
    {
      WriteToken(Inv.Syntax.TokenType.Period);
    }
    public void WriteEqualSign()
    {
      WriteToken(Inv.Syntax.TokenType.EqualSign);
    }
    public void WriteExclamationPoint()
    {
      WriteToken(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public void WriteQuestionMark()
    {
      WriteToken(Inv.Syntax.TokenType.QuestionMark);
    }
    public void WriteAt()
    {
      WriteToken(Inv.Syntax.TokenType.At);
    }
    public void WritePlus()
    {
      WriteToken(Inv.Syntax.TokenType.Plus);
    }
    public void WriteMinus()
    {
      WriteToken(Inv.Syntax.TokenType.Minus);
    }
    public void WriteHash()
    {
      WriteToken(Inv.Syntax.TokenType.Hash);
    }
    public void WriteAsterisk()
    {
      WriteToken(Inv.Syntax.TokenType.Asterisk);
    }
    public void WriteAmpersand()
    {
      WriteToken(Inv.Syntax.TokenType.Ampersand);
    }
    public void WritePipe()
    {
      WriteToken(Inv.Syntax.TokenType.Pipe);
    }
    public void WriteUnderscore()
    {
      WriteToken(Inv.Syntax.TokenType.Underscore);
    }
    public void WriteForwardSlash()
    {
      WriteToken(Inv.Syntax.TokenType.ForwardSlash);
    }
    public void WriteBackSlash()
    {
      WriteToken(Inv.Syntax.TokenType.BackSlash);
    }
    public void WriteCaret()
    {
      WriteToken(Inv.Syntax.TokenType.Caret);
    }
    public void WriteTilde()
    {
      WriteToken(Inv.Syntax.TokenType.Tilde);
    }
    public void WriteDoubleQuote()
    {
      WriteToken(Inv.Syntax.TokenType.DoubleQuote);
    }
    public void WriteSpace()
    {
      WriteWhitespace(" ");
    }
    public void WriteLine()
    {
      CacheToken.Newline = 1;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteIndent()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel >= 0, "IndentLevel must be greater than zero.");

      WriteWhitespace(new string(' ', IndentLevel * 2));
    }
    public void WriteLineAndIndent()
    {
      WriteLine();
      WriteIndent();
    }
    public void WriteOpenScope()
    {
      WriteIndent();
      WriteOpenBrace();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteCloseScope()
    {
      DecreaseIndent();
      WriteIndent();
      WriteCloseBrace();
      WriteLine();
    }
    public void WriteComment(string Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Value, "Value");

      CacheToken.Comment = Value;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteOpenScopeString(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteString(Identifier);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenScopeIdentifier(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteIdentifier(Identifier);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenScopeKeyword(string Keyword)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenCollectionKeyword(string Keyword)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteLineAndIndent();
      WriteOpenRound();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteOpenCollectionIdentifier(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteIdentifier(Identifier);
      WriteLineAndIndent();
      WriteOpenRound();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteCloseCollection()
    {
      DecreaseIndent();
      WriteIndent();
      WriteCloseRound();
      WriteLine();
    }
    public void WriteOptionalStringAttribute(string Keyword, string StringValue)
    {
      if (StringValue != null)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteString(StringValue);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalDateTimeAttribute(string Keyword, DateTimeOffset? DateTimeValue)
    {
      if (DateTimeValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteDateTimeOffset(DateTimeValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalDecimalAttribute(string Keyword, decimal? DecimalValue)
    {
      if (DecimalValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteDecimal(DecimalValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalMoneyAttribute(string Keyword, Inv.Money? MoneyValue)
    {
      if (MoneyValue != null)
        WriteOptionalDecimalAttribute(Keyword, MoneyValue.Value.GetAmount());
    }
    public void WriteOptionalIntegerAttribute(string Keyword, long? IntegerValue)
    {
      if (IntegerValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteInteger(IntegerValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalBooleanAttribute(string Keyword, bool? BooleanValue)
    {
      if (BooleanValue.HasValue && BooleanValue.Value)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteBoolean(BooleanValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalIdentifierAttribute(string Keyword, string IdentifierValue)
    {
      if (IdentifierValue != null)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteIdentifier(IdentifierValue);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteNumber(string NumberValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(NumberValue, "NumberValue");

      CacheToken.Number = NumberValue;
      WriteContract.WriteToken(CacheToken);
    }

    private int IndentLevel;
    private Inv.Syntax.Token CacheToken;
  }

  public sealed class Extractor
  {
    public Extractor(Inv.Syntax.ReadContract ReadContract)
    {
      this.ReadContract = ReadContract;
    }

    internal Inv.Syntax.ReadContract ReadContract { get; private set; }

    public void Start()
    {
      ReadContract.Start();
    }
    public void Stop()
    {
      if (ReadContract.More())
        throw ReadContract.Interrupt("The syntax document was longer than is valid.");

      ReadContract.Stop();
    }

    public ReadException Interrupt(string Message)
    {
      return ReadContract.Interrupt(Message);
    }
    public bool More()
    {
      return ReadContract.More();
    }
    public TokenType? PeekTokenType()
    {
      var Token = ReadContract.PeekToken();

      if (Token == null)
        return null;
      else
        return Token.Type;
    }
    public Token PeekToken()
    {
      return ReadContract.PeekToken();
    }
    public bool NextIsTokenType(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType;
    }
    public bool NextIsTokenTypeArray(params Inv.Syntax.TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && TokenTypeArray.Contains(SyntaxToken.Type.Value);
    }
    public void SkipToken()
    {
      ReadContract.SkipToken();
    }
    public int GetPosition()
    {
      return ReadContract.GetPosition();
    }
    public Inv.Syntax.TokenType ReadTokenTypeArray(params Inv.Syntax.TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", ")));

      var SyntaxTokenType = SyntaxToken.Type.Value;

      if (!TokenTypeArray.Contains(SyntaxTokenType))
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", "), SyntaxTokenType));

      ReadContract.SkipToken();

      return SyntaxTokenType;
    }
    public Inv.Syntax.TokenType ReadTokenType()
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt("expected a token but found the end of the document.");

      var SyntaxTokenType = SyntaxToken.Type.Value;

      ReadContract.SkipToken();

      return SyntaxTokenType;
    }
    public void ReadTokenType(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenType));

      if (TokenType != SyntaxToken.Type)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenType, SyntaxToken.Type));

      ReadContract.SkipToken();
    }
    public bool ReadOptionalTokenType(Inv.Syntax.TokenType TokenType)
    {
      var Result = NextIsTokenType(TokenType);

      if (Result)
        ReadContract.SkipToken();

      return Result;
    }
    public bool NextIsIdentifierValue(string ExpectedIdentifier)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Identifier && string.Equals(SyntaxToken.Identifier, ExpectedIdentifier, StringComparison.CurrentCultureIgnoreCase);
    }
    public bool NextIsIdentifier()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Identifier);
    }
    public bool NextIsString()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.String);
    }
    public bool NextIsColour()
    {
      return NextIsTokenType(TokenType.Hexadecimal);
    }
    public bool NextIsTime()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Time);
    }
    public bool NextIsTimePeriod()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.TimePeriod);
    }
    public bool NextIsTimeSpan()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.TimeSpan);
    }
    public bool NextIsNumber()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Number);
    }
    public bool NextIsBase64()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Base64);
    }
    public bool NextIsHexadecimal()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Hexadecimal);
    }
    public bool NextIsBackSlash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.BackSlash);
    }
    public bool NextIsPeriod()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Period);
    }
    public bool NextIsForwardSlash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public bool NextIsExclamationPoint()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public bool NextIsBoolean()
    {
      if (NextIsIdentifier())
      {
        var SyntaxToken = ReadContract.PeekToken();
        bool result;
        return bool.TryParse(SyntaxToken.Identifier, out result);
      }
      else
        return false;
    }
    public bool NextIsOpenBrace()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public bool NextIsCloseBrace()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public bool NextIsOpenAngle()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public bool NextIsCloseAngle()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public bool NextIsOpenRound()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public bool NextIsCloseRound()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public bool NextIsOpenScope()
    {
      return NextIsOpenBrace();
    }
    public bool NextIsCloseScope()
    {
      return NextIsCloseBrace();
    }
    public bool NextIsSemicolon()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public bool NextIsUnderscore()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public bool NextIsHash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Hash);
    }
    public bool NextIsColon()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Colon);
    }
    public bool NextIsEqualSign()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public string PeekIdentifier()
    {
      return PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;
    }
    public string ReadIdentifierOrKeyword()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadKeyword();
    }
    public string ReadIdentifierOrNumber()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadNumber();
    }
    public string ReadIdentifierOrString()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadString();
    }
    public string ReadIdentifier()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;

      ReadContract.SkipToken();

      return Result;
    }
    public string ReadOptionalIdentifier()
    {
      if (NextIsIdentifier())
        return ReadIdentifier();
      else
        return null;
    }
    public bool ReadOptionalIdentifierValue(string ExpectedIdentifier)
    {
      var Result = NextIsIdentifierValue(ExpectedIdentifier);

      if (Result)
        ReadContract.SkipToken();

      return Result;
    }
    public string ReadIdentifierValue(string ExpectedIdentifier)
    {
      return ReadIdentifierValueArray(ExpectedIdentifier);
    }
    public string ReadIdentifierValueArray(params string[] ExpectedIdentifierArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;

      if (!ExpectedIdentifierArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt(string.Format("expected identifier '{0}' but found identifier '{1}'.", ExpectedIdentifierArray.AsSeparatedText(" or "), Result));

      ReadContract.SkipToken();

      return Result;
    }
    public string[] ReadIdentifierChainArray(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new Inv.DistinctList<string>();

      Result.Add(ReadIdentifier());

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Add(ReadIdentifier());
      }

      return Result.ToArray();
    }
    public string ReadIdentifierChainString(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new StringBuilder(ReadIdentifier());
      var Separator = Inv.Syntax.SymbolTable.CharacterTokenTypeIndex.GetByRight(SeparatorTokenType);

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Append(Separator + ReadIdentifier());
      }

      return Result.ToString();
    }
    public List<string> ReadIdentifierChainStringList(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new List<string>();
      Result.Add(ReadIdentifier());

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Add(ReadIdentifier());
      }

      return Result;
    }
    public string PeekKeyword()
    {
      return PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;
    }
    public string PeekKeywordOrIdentifier()
    {
      if (NextIsKeyword())
        return PeekKeyword();
      else
        return PeekIdentifier();
    }
    public bool NextIsKeywordValue(string ExpectedKeyword)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Keyword && string.Equals(SyntaxToken.Keyword, ExpectedKeyword, StringComparison.CurrentCultureIgnoreCase);
    }
    public bool NextIsKeyword()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Keyword);
    }
    public bool NextIsKeywordValueArray(params string[] KeywordArray)
    {
      var NextKeywordFound = false;

      if (NextIsKeyword())
      {
        var Keyword = PeekKeyword();

        var KeywordIndex = 0;
        while (!NextKeywordFound && (KeywordIndex < KeywordArray.Length))
        {
          NextKeywordFound = string.Equals(KeywordArray[KeywordIndex], Keyword, StringComparison.CurrentCultureIgnoreCase);
          KeywordIndex++;
        }
      }

      return NextKeywordFound;
    }
    public string ReadKeyword()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;

      ReadContract.SkipToken();

      return Result;
    }
    public void ReadKeywordValue(string ExpectedKeyword)
    {
      ReadKeywordValueArray(ExpectedKeyword);
    }
    public string ReadKeywordValueArray(params string[] ExpectedKeywordArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;

      if (!ExpectedKeywordArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt(string.Format("expected keyword '{0}' but found keyword '{1}'.", ExpectedKeywordArray.AsSeparatedText(" or "), Result));

      ReadContract.SkipToken();

      return Result;
    }
    public bool NextIsNumberValue(string ExpectedNumber)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Number && string.Equals(SyntaxToken.Number, ExpectedNumber, StringComparison.CurrentCultureIgnoreCase);
    }
    public void ReadNumberValue(string ExpectedNumber)
    {
      ReadNumberValueArray(ExpectedNumber);
    }
    public string ReadNumberValueArray(params string[] ExpectedNumberArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Number).Number;

      if (!ExpectedNumberArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt(string.Format("expected number '{0}' but found number '{1}'.", ExpectedNumberArray.AsSeparatedText(" or "), Result));

      ReadContract.SkipToken();

      return Result;
    }
    public bool ReadOptionalNumberValue(string ExpectedNumber)
    {
      var Result = NextIsNumberValue(ExpectedNumber);

      if (Result)
        ReadNumberValue(ExpectedNumber);

      return Result;
    }
    public string ReadOptionalKeyword()
    {
      if (NextIsKeyword())
        return ReadKeyword();
      else
        return null;
    }
    public bool ReadOptionalKeywordValueArray(params string[] ExpectedKeywordArray)
    {
      var Result = NextIsKeywordValueArray(ExpectedKeywordArray);

      if (Result)
        ReadKeywordValueArray(ExpectedKeywordArray);

      return Result;
    }
    public bool ReadOptionalKeywordValue(string ExpectedKeyword)
    {
      var Result = NextIsKeywordValue(ExpectedKeyword);

      if (Result)
        ReadKeywordValue(ExpectedKeyword);

      return Result;
    }
    public string ReadOptionalString()
    {
      if (NextIsString())
        return ReadString();
      else
        return null;
    }
    public Inv.Colour ReadOptionalColour()
    {
      if (NextIsColour())
        return ReadColour();
      else
        return null;
    }
    public bool ReadOptionalAmpersand()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Ampersand);
    }
    public bool ReadOptionalAsterisk()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Asterisk);
    }
    public bool ReadOptionalAt()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.At);
    }
    public bool ReadOptionalComma()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Comma);
    }
    public bool ReadOptionalColon()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Colon);
    }
    public bool ReadOptionalPipe()
    {
      return ReadOptionalTokenType(TokenType.Pipe);
    }
    public bool ReadOptionalExclamationPoint()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public bool ReadOptionalPeriod()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Period);
    }
    public bool ReadOptionalHash()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Hash);
    }
    public bool ReadOptionalQuestionMark()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.QuestionMark);
    }
    public bool ReadOptionalPlus()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Plus);
    }
    public bool ReadOptionalMinus()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Minus);
    }
    public bool ReadOptionalOpenRound()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public bool ReadOptionalCloseRound()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public bool ReadOptionalOpenSquare()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenSquare);
    }
    public bool ReadOptionalCloseSquare()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseSquare);
    }
    public bool ReadOptionalOpenBrace()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public bool ReadOptionalCloseBrace()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public bool ReadOptionalOpenAngle()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public bool ReadOptionalCloseAngle()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public bool ReadOptionalTilde()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Tilde);
    }
    public bool ReadOptionalSemicolon()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public bool ReadOptionalEqualSign()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public bool ReadOptionalForwardSlash()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public bool ReadOptionalUnderscore()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public string ReadString()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.String).String;

      ReadContract.SkipToken();

      return Result;
    }
    public char ReadCharacter()
    {
      return ReadString()[0];
    }
    public Guid ReadGuid()
    {
      Guid Value;
      var StringValue = ReadString();

      if (!Guid.TryParse(StringValue, out Value))
        ReadContract.Interrupt(string.Format("String '{0}' is not a value Guid value.", StringValue));

      return Value;
    }
    public System.DateTimeOffset ReadDateTimeOffset()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.DateTime).DateTime;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Date ReadDate()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Date).Date;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Time ReadTime()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Time).Time;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.TimePeriod ReadTimePeriod()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.TimePeriod).TimePeriod;

      ReadContract.SkipToken();

      return Result;
    }
    public System.TimeSpan ReadTimeSpan()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.TimeSpan).TimeSpan;

      ReadContract.SkipToken();

      return Result;
    }
    public bool ReadBoolean()
    {
      var Identifier = ReadIdentifier();

      if ("true".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return true;

      if ("false".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return false;

      throw ReadContract.Interrupt(string.Format("Identifier '{0}' is not an expected boolean value.", Identifier));
    }
    public string ReadNumber()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Number).Number;

      ReadContract.SkipToken();

      return Result;
    }
    public double ReadReal()
    {
      var RealText = ReadNumber();

      if (ReadOptionalPeriod())
        RealText += "." + ReadNumber();

      return Convert.ToDouble(RealText);
    }
    public long ReadInteger64()
    {
      return Convert.ToInt64(ReadOptionalMinus() ? "-" + ReadNumber() : ReadNumber());
    }
    public int ReadInteger32()
    {
      return Convert.ToInt32(ReadOptionalMinus() ? "-" + ReadNumber() : ReadNumber());
    }
    public decimal ReadDecimal()
    {
      var Sign = 1;
      if (ReadOptionalTokenType(Inv.Syntax.TokenType.Minus))
        Sign = -1;

      var DecimalText = ReadNumber();
      if (ReadOptionalPeriod())
        DecimalText += "." + ReadNumber();

      return Convert.ToDecimal(DecimalText) * Sign;
    }
    public byte[] ReadBase64()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Base64).Base64.GetBuffer();

      ReadContract.SkipToken();

      return Result;
    }
    public byte[] ReadHexadecimal()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Hexadecimal).Hexadecimal.GetBuffer();

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Colour ReadColour()
    {
      if (NextIsIdentifier())
      {
        return Inv.Colour.FromName(ReadIdentifier());
      }
      else
      {
        var Hex = ReadHexadecimal();

        if (Hex.Length == 4)
          return Inv.Colour.FromArgb(BitConverter.ToInt32(Hex.Reverse().ToArray(), 0));
        else
          return Inv.Colour.FromArgb(0);
      }
    }
    public void ReadOpenBrace()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public void ReadCloseBrace()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public void ReadOpenSquare()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenSquare);
    }
    public void ReadCloseSquare()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseSquare);
    }
    public void ReadOpenAngle()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public void ReadCloseAngle()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public void ReadOpenRound()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public void ReadCloseRound()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public void ReadSemicolon()
    {
      ReadTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public void ReadColon()
    {
      ReadTokenType(Inv.Syntax.TokenType.Colon);
    }
    public void ReadPercent()
    {
      ReadTokenType(Inv.Syntax.TokenType.Percent);
    }
    public void ReadEqualSign()
    {
      ReadTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public void ReadExclamationPoint()
    {
      ReadTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public void ReadQuestionMark()
    {
      ReadTokenType(Inv.Syntax.TokenType.QuestionMark);
    }
    public void ReadUnderscore()
    {
      ReadTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public void ReadAmpersand()
    {
      ReadTokenType(Inv.Syntax.TokenType.Ampersand);
    }
    public void ReadPipe()
    {
      ReadTokenType(Inv.Syntax.TokenType.Pipe);
    }
    public void ReadComma()
    {
      ReadTokenType(Inv.Syntax.TokenType.Comma);
    }
    public void ReadPeriod()
    {
      ReadTokenType(Inv.Syntax.TokenType.Period);
    }
    public void ReadPlus()
    {
      ReadTokenType(Inv.Syntax.TokenType.Plus);
    }
    public void ReadMinus()
    {
      ReadTokenType(Inv.Syntax.TokenType.Minus);
    }
    public void ReadHash()
    {
      ReadTokenType(Inv.Syntax.TokenType.Hash);
    }
    public void ReadForwardSlash()
    {
      ReadTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public void ReadBackSlash()
    {
      ReadTokenType(Inv.Syntax.TokenType.BackSlash);
    }
    public void ReadCaret()
    {
      ReadTokenType(Inv.Syntax.TokenType.Caret);
    }
    public void ReadTilde()
    {
      ReadTokenType(Inv.Syntax.TokenType.Tilde);
    }
    public void ReadAsterisk()
    {
      ReadTokenType(Inv.Syntax.TokenType.Asterisk);
    }
    public void ReadAt()
    {
      ReadTokenType(Inv.Syntax.TokenType.At);
    }
    public void ReadDollarSign()
    {
      ReadTokenType(Inv.Syntax.TokenType.DollarSign);
    }
    public string ReadWhiteSpace()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Whitespace).Whitespace;

      ReadContract.SkipToken();

      return Result;
    }
    public string ReadOpenScopeString(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadString();
      ReadOpenScope();
      return Identifier;
    }
    public string ReadOpenScopeIdentifier(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadIdentifier();
      ReadOpenScope();
      return Identifier;
    }
    public void ReadOpenScopeKeyword(string Keyword)
    {
      ReadKeywordValue(Keyword);
      ReadOpenScope();
    }
    public bool ReadOptionalOpenScope()
    {
      return ReadOptionalOpenBrace();
    }
    public void ReadOpenScope()
    {
      ReadOpenBrace();
    }
    public void ReadCloseScope()
    {
      ReadCloseBrace();
    }
    public void ReadOpenCollectionKeyword(string Keyword)
    {
      ReadKeywordValue(Keyword);
      ReadOpenRound();
    }
    public string ReadOpenCollectionIdentifier(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadIdentifier();
      ReadOpenRound();
      return Identifier;
    }
    public string ReadOptionalStringAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var StringValue = ReadString();
        ReadSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public string ReadOptionalIdentifierAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var StringValue = ReadIdentifier();
        ReadSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public DateTimeOffset? ReadOptionalDateTimeAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var DateTimeValue = ReadDateTimeOffset();
        ReadSemicolon();
        return DateTimeValue;
      }
      else
      {
        return null;
      }
    }
    public decimal? ReadOptionalDecimalAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var DecimalValue = ReadDecimal();
        ReadSemicolon();
        return DecimalValue;
      }
      else
      {
        return null;
      }
    }
    public Inv.Money? ReadOptionalMoneyAttribute(string Keyword)
    {
      var Result = ReadOptionalDecimalAttribute(Keyword);

      if (Result == null)
        return null;
      else
        return new Inv.Money(Result.Value);
    }
    public long? ReadOptionalInteger64Attribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var IntegerValue = ReadInteger32();
        ReadSemicolon();
        return IntegerValue;
      }
      else
      {
        return null;
      }
    }
    public bool? ReadOptionalBooleanAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var BooleanValue = ReadBoolean();
        ReadSemicolon();
        return BooleanValue;
      }
      else
      {
        return null;
      }
    }

    private Inv.Syntax.Token PeekToken(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found the end of the document.", TokenType));

      if (SyntaxToken.Type != TokenType)
        throw ReadContract.Interrupt(string.Format("expected token '{0}' but found token '{1}'.", TokenType, SyntaxToken.Type));

      return SyntaxToken;
    }
  }

  public sealed class WriteException : System.Exception
  {
    public WriteException(string message)
      : base(message)
    {
    }
  }

  public sealed class ReadException : System.Exception
  {
    public ReadException(string message, long? Location)
      : base(message)
    {
      this.Location = Location;
    }
    public ReadException(string message, long? Location, Exception innerException)
      : base(message, innerException)
    {
      this.Location = Location;
    }

    public long? Location { get; private set; }
  }

  public static class SymbolTable
  {
    public static readonly Inv.Bidictionary<char, Syntax.TokenType> CharacterTokenTypeIndex = new Inv.Bidictionary<char, Syntax.TokenType>();
    public static readonly Inv.CharacterSet WhitespaceCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet IdentifierCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet NumberCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet HexadecimalCharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet Base64CharacterSet = new Inv.CharacterSet();
    public static readonly Inv.CharacterSet VerticalWhitespaceCharacterSet = new Inv.CharacterSet();

    static SymbolTable()
    {
      CharacterTokenTypeIndex.Add('(', Inv.Syntax.TokenType.OpenRound);
      CharacterTokenTypeIndex.Add(')', Inv.Syntax.TokenType.CloseRound);
      CharacterTokenTypeIndex.Add('<', Inv.Syntax.TokenType.OpenAngle);
      CharacterTokenTypeIndex.Add('>', Inv.Syntax.TokenType.CloseAngle);
      CharacterTokenTypeIndex.Add('{', Inv.Syntax.TokenType.OpenBrace);
      CharacterTokenTypeIndex.Add('}', Inv.Syntax.TokenType.CloseBrace);
      CharacterTokenTypeIndex.Add(',', Inv.Syntax.TokenType.Comma);
      CharacterTokenTypeIndex.Add('.', Inv.Syntax.TokenType.Period);
      CharacterTokenTypeIndex.Add(';', Inv.Syntax.TokenType.Semicolon);
      CharacterTokenTypeIndex.Add(':', Inv.Syntax.TokenType.Colon);
      CharacterTokenTypeIndex.Add('=', Inv.Syntax.TokenType.EqualSign);
      CharacterTokenTypeIndex.Add('!', Inv.Syntax.TokenType.ExclamationPoint);
      CharacterTokenTypeIndex.Add('?', Inv.Syntax.TokenType.QuestionMark);
      CharacterTokenTypeIndex.Add('&', Inv.Syntax.TokenType.Ampersand);
      CharacterTokenTypeIndex.Add('|', Inv.Syntax.TokenType.Pipe);
      CharacterTokenTypeIndex.Add('+', Inv.Syntax.TokenType.Plus);
      CharacterTokenTypeIndex.Add('-', Inv.Syntax.TokenType.Minus);
      CharacterTokenTypeIndex.Add('%', Inv.Syntax.TokenType.Percent);
      CharacterTokenTypeIndex.Add('*', Inv.Syntax.TokenType.Asterisk);
      CharacterTokenTypeIndex.Add('\\', Inv.Syntax.TokenType.BackSlash);
      CharacterTokenTypeIndex.Add('/', Inv.Syntax.TokenType.ForwardSlash);
      CharacterTokenTypeIndex.Add('@', Inv.Syntax.TokenType.At);
      CharacterTokenTypeIndex.Add('^', Inv.Syntax.TokenType.Caret);
      CharacterTokenTypeIndex.Add('~', Inv.Syntax.TokenType.Tilde);
      CharacterTokenTypeIndex.Add('$', Inv.Syntax.TokenType.DollarSign);
      CharacterTokenTypeIndex.Add('#', Inv.Syntax.TokenType.Hash);
      CharacterTokenTypeIndex.Add('\'', Inv.Syntax.TokenType.SingleQuote);
      CharacterTokenTypeIndex.Add('"', Inv.Syntax.TokenType.DoubleQuote);
      CharacterTokenTypeIndex.Add('`', Inv.Syntax.TokenType.BackQuote);
      CharacterTokenTypeIndex.Add('[', Inv.Syntax.TokenType.OpenSquare);
      CharacterTokenTypeIndex.Add(']', Inv.Syntax.TokenType.CloseSquare);
      CharacterTokenTypeIndex.Add('_', Inv.Syntax.TokenType.Underscore);

      WhitespaceCharacterSet.AddRange('\x0000', ' ');
      IdentifierCharacterSet.AddRange('a', 'z');
      IdentifierCharacterSet.AddRange('A', 'Z');
      IdentifierCharacterSet.AddRange('0', '9');
      IdentifierCharacterSet.Add('_');
      NumberCharacterSet.AddRange('0', '9');
      VerticalWhitespaceCharacterSet.Add('\n');
      VerticalWhitespaceCharacterSet.Add('\r');

      HexadecimalCharacterSet.AddRange('0', '9');
      HexadecimalCharacterSet.AddRange('A', 'F');
      HexadecimalCharacterSet.AddRange('a', 'f');

      Base64CharacterSet.AddRange('0', '9');
      Base64CharacterSet.AddRange('A', 'Z');
      Base64CharacterSet.AddRange('a', 'z');
      Base64CharacterSet.Add('+');
      Base64CharacterSet.Add('/');
      Base64CharacterSet.Add('=');
    }
  }
}