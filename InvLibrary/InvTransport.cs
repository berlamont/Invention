﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public interface TransportFactory
  {
    TransportConnection JoinConnection();
  }

  public interface TransportConnection
  {
    TransportFlow Flow { get; }
    void Drop();
  }

  public sealed class TransportLink : IDisposable
  {
    public TransportLink(Inv.TransportFactory TransportFactory)
    {
      this.TransportFactory = TransportFactory;
      this.ConnectionPool = new Pool<TransportConnection>(() =>
      {
        var Connection = TransportFactory.JoinConnection(); // join the connection.

        if (JoinEvent != null)
          JoinEvent(Connection);

        return Connection;
      }, C =>
      {
        C.Drop(); // drop the connection.

        if (DropEvent != null)
          DropEvent(C);
      });
      this.FaultedWaitHandle = new EventWaitHandle(true, EventResetMode.AutoReset);
      this.EstablishedWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
      this.FaultCriticalSection = new Inv.ExclusiveCriticalSection("Inv.TransportLink-Broker");
      this.ExchangeCriticalSection = new Inv.ReaderWriterCriticalSection("Inv.TransportLink-Exchange");
      ExchangeCriticalSection.NoTimeout();
    }
    public void Dispose()
    {
      ExchangeCriticalSection.Dispose();
      FaultedWaitHandle.Dispose();
      EstablishedWaitHandle.Dispose();
    }

    public bool IsActive { get; private set; }
    public event Action ConnectEvent;
    public event Action DisconnectEvent;
    public event Action ReconnectEvent;
    public event Action<Inv.TransportConnection> JoinEvent;
    public event Action<Inv.TransportConnection> DropEvent;
    public event Action<Exception> FaultBeginEvent;
    public event Action<Exception> FaultContinueEvent;
    public event Action<Exception> FaultEndEvent;

    public void Connect()
    {
      if (!IsActive)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");

        var Connection = ConnectionPool.Take();
        try
        {
          if (ConnectEvent != null)
            ConnectEvent();
        }
        catch (Exception Exception)
        {
          Connection.Drop();

          throw Exception.Preserve();
        }
        ConnectionPool.Return(Connection);

        this.IsActive = true;
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (DisconnectEvent != null)
          DisconnectEvent();

        ConnectionPool.Drain(); // disposes all active connections.

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");
      }
    }
    public Inv.TransportConnection TakeConnection()
    {
      return ConnectionPool.Take();
    }
    public void ReturnConnection(Inv.TransportConnection Connection)
    {
      ConnectionPool.Return(Connection);
    }
    public void Exchange(Action<Inv.TransportConnection> Action)
    {
      Exception FaultException = null;

      do
      {
        using (ExchangeCriticalSection.EnterReadLock())
        {
          try
          {
            var Connection = ConnectionPool.Take();
            try
            {
              Action(Connection);
            }
            finally
            {
              ConnectionPool.Return(Connection);
            }
          }
          catch (TransportException TransportException)
          {
            throw TransportException.Preserve();
          }
          catch (Exception Exception)
          {
            // NOTE: just I/O?
            FaultException = Exception;
          }
        }

        if (FaultException != null)
          FaultRecovery(FaultException);
      }
      while (IsActive && FaultException != null);
    }
    public T Exchange<T>(Func<Inv.TransportConnection, T> Func)
    {
      var Result = default(T);

      Exchange((Action<Inv.TransportConnection>)(Connection => Result = Func(Connection)));

      return Result;
    }
    public void SimulateFault(TimeSpan TimeSpan)
    {
      var StartTime = DateTime.Now;

      Action FaultDelay = () =>
      {
        // NOTE: we want the reconnection to be attempted until the end of the simulation time.
        if (StartTime + TimeSpan > DateTime.Now)
          throw new Exception("Simulated unable to reconnect fault.");
      };

      Inv.TaskGovernor.RunActivity("AdvTransportLink.SimulateFault", () =>
      {
        ReconnectEvent += FaultDelay;
        try
        {
          FaultRecovery(new Exception("Simulated remote connection fault."));
        }
        finally
        {
          ReconnectEvent -= FaultDelay;
        }
      });
    }

    private void FaultRecovery(Exception FaultException)
    {
      bool IsFirstFaultThread;

      using (FaultCriticalSection.Lock())
      {
        IsFirstFaultThread = FaultedWaitHandle.WaitOne(0);

        if (IsFirstFaultThread)
          EstablishedWaitHandle.Reset();
      }

      if (!IsFirstFaultThread)
      {
        // Block this thread until reconnection is complete.
        EstablishedWaitHandle.WaitOne();
      }
      else
      {
        using (ExchangeCriticalSection.EnterWriteLock())
        {
          try
          {
            var FaultWaitHandleSet = false;

            try
            {
              if (FaultBeginEvent != null)
                FaultBeginEvent(FaultException);

              var Reconnecting = true;
              while (Reconnecting)
              {
                try
                {
                  if (IsActive)
                  {
                    if (ReconnectEvent != null)
                      ReconnectEvent();
                  }

                  Reconnecting = false;
                }
                catch (TransportException ContinueException)
                {
                  if (FaultContinueEvent != null)
                    FaultContinueEvent(ContinueException);

                  // wait one second before trying again.
                  Inv.TaskGovernor.Sleep(1000);

                  Reconnecting = true;
                }
              }

              if (FaultEndEvent != null)
                FaultEndEvent(FaultException);

              FaultWaitHandleSet = true;
              FaultedWaitHandle.Set();
            }
            catch (Exception TerminalException)
            {
              // NOTE: safety measure to ensure the fault management can't completely fail.
              if (!FaultWaitHandleSet)
                FaultedWaitHandle.Set();

              throw TerminalException.Preserve();
            }
          }
          finally
          {
            // Allow subsequent faulting threads to continue.
            EstablishedWaitHandle.Set();
          }
        }
      }
    }

    private Inv.TransportFactory TransportFactory;
    private Inv.Pool<Inv.TransportConnection> ConnectionPool;
    private Inv.ReaderWriterCriticalSection ExchangeCriticalSection;
    private Inv.ExclusiveCriticalSection FaultCriticalSection;
    private EventWaitHandle FaultedWaitHandle;
    private EventWaitHandle EstablishedWaitHandle;
  }

  public sealed class TransportPacket
  {
    public TransportPacket(byte[] Buffer)
    {
      this.Buffer = Buffer;
    }

    public byte[] Buffer { get; private set; }

    public TransportReader ToReader()
    {
      return new TransportReader(this);
    }
  }

  public sealed class TransportFlow
  {
    public TransportFlow(System.IO.Stream Stream)
      : this(Stream, Stream)
    {
    }
    public TransportFlow(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    public System.IO.Stream InputStream { get; private set; }
    public System.IO.Stream OutputStream { get; private set; }

    public void SendPacket(Inv.TransportPacket RoutePacket)
    {
      if (!TrySendPacket(RoutePacket))
        throw new Inv.TransportException("Send connection lost.");
    }
    public bool TrySendPacket(Inv.TransportPacket RoutePacket)
    {
      var Buffer = RoutePacket.Buffer;
      return WriteByteArray(Buffer, 0, Buffer.Length);
    }
    public Inv.TransportPacket ReceivePacket()
    {
      var Result = TryReceivePacket();

      if (Result == null)
        throw new Inv.TransportException("Receive connection lost.");

      return Result;
    }
    public Inv.TransportPacket TryReceivePacket()
    {
      // TODO: more efficient buffered socket reading?

      var DataBuffer = new byte[4];

      if (!ReadByteArray(DataBuffer, 0, 4))
        return null;

      var DataLength = BitConverter.ToInt32(DataBuffer, 0);

      if (DataLength > 0)
      {
        Array.Resize(ref DataBuffer, DataLength);

        if (!ReadByteArray(DataBuffer, 4, DataLength))
          return null;

        return new Inv.TransportPacket(DataBuffer);
      }
      else
      {
        return null;
      }
    }

    [DebuggerNonUserCode]
    private bool WriteByteArray(byte[] Buffer, int Offset, int Length)
    {
      try
      {
        OutputStream.Write(Buffer, Offset, Length);
      }
      catch
      {
        return false;
      }

      return true;
    }
    [DebuggerNonUserCode]
    private bool ReadByteArray(byte[] Buffer, int Offset, int Length)
    {
      // NOTE: don't break on exception in here as socket disconnections are expected.
      //       downstream code must cope with ReceivePacket returning null.
      try
      {
        var Index = Offset;
        while (Index < Length)
        {
          var Actual = InputStream.Read(Buffer, Index, Length - Index);

          if (Actual == 0)
            return false;

          Index += Actual;
        }
      }
      catch
      {
        return false;
      }

      return true;
    }
  }

  public sealed class TransportException : Exception
  {
    public TransportException(string Message)
      : base(Message)
    {
    }
    public TransportException(Exception Exception)
      : base(Exception.Message)
    {
      this.Type = Exception.GetType().FullName;
    }
    public TransportException(string Type, string Message, string Source, string StackTrace)
      : base(Message)
    {
      this.Type = Type;
      this.SourceField = Source;
      this.StackTraceField = StackTrace;
    }

    public string Type { get; private set; }
    public override string Source
    {
      get { return SourceField; }
    }
    public override string StackTrace
    {
      get { return StackTraceField; }
    }

    private string SourceField;
    private string StackTraceField;
  }

  public sealed class TransportProtocol
  {
    public TransportProtocol()
    {
      this.InterceptDictionary = new Dictionary<Type, TransportIntercept>();
    }

    internal TransportIntercept GetIntercept<T>()
    {
      return InterceptDictionary.GetValueOrDefault(typeof(T));
    }
    public void AddIntercept<T>(Action<Inv.TransportWriter, T> WriterAction, Func<Inv.TransportReader, T> ReaderFunc)
    {
      InterceptDictionary.Add(typeof(T), new TransportIntercept(typeof(T), (Writer, Context) => WriterAction(Writer, (T)Context), (Reader) => ReaderFunc(Reader)));
    }

    private Dictionary<Type, TransportIntercept> InterceptDictionary;
  }

  internal sealed class TransportIntercept
  {
    internal TransportIntercept(Type Type, Action<Inv.TransportWriter, object> WriterAction, Func<Inv.TransportReader, object> ReaderFunc)
    {
      this.Type = Type;
      this.WriterAction = WriterAction;
      this.ReaderFunc = ReaderFunc;
    }

    public Type Type { get; private set; }
    public Action<Inv.TransportWriter, object> WriterAction { get; private set; }
    public Func<Inv.TransportReader, object> ReaderFunc { get; private set; }
  }

  public sealed class TransportWriter : IDisposable
  {
    public TransportWriter()
    {
      this.MemoryStream = new MemoryStream();
      WriteInt32(0); // reserve four bytes for the packet length.
    }
    public void Dispose()
    {
      this.MemoryStream.Dispose();
    }

    public void SetProtocol(TransportProtocol Protocol)
    {
      this.Protocol = Protocol;
    }
    public void Reset()
    {
      MemoryStream.Position = 0;
      MemoryStream.SetLength(0);
      WriteInt32(0);
    }
    public void WriteStream(Action<Stream> Action)
    {
      Action(MemoryStream);
    }
    public void WriteDataObject<T>(T Value)
    {
      var Intercept = Protocol != null ? Protocol.GetIntercept<T>() : null;

      if (Intercept != null)
      {
        Intercept.WriterAction(this, Value);
      }
      else
      {
        // TODO: inline data contract serializers not working?
        //using (var XmlBinaryWriter = XmlDictionaryWriter.CreateBinaryWriter(Stream, null, null, false))
        //  new DataContractSerializer(typeof(T)).WriteObject(XmlBinaryWriter, Value);

        using (var MemoryStream = new MemoryStream())
        {
          using (var XmlBinaryWriter = XmlDictionaryWriter.CreateBinaryWriter(MemoryStream))
            new DataContractSerializer(typeof(T)).WriteObject(XmlBinaryWriter, Value);

          MemoryStream.Flush();

          WriteByteArray(MemoryStream.ToArray());
        }
      }
    }
    public void WriteBoolean(bool Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteCharacter(char Value)
    {
      var Buffer = BitConverter.GetBytes(Value);
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteDateTimeOffset(DateTimeOffset Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value.DateTime.ToBinary()));
      WriteBuffer(BitConverter.GetBytes(Value.Offset.Ticks));
    }
    public void WriteDateTimeOffsetNullable(DateTimeOffset? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteDateTimeOffset(Value.Value);
    }
    public void WriteGuid(Guid Value)
    {
      WriteBuffer(Value.ToByteArray());
    }
    public void WriteUInt8(byte Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteUInt16(ushort Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteUInt32(uint Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteInt32(int Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteByte(byte Value)
    {
      WriteBuffer(new byte[1] { Value });
    }
    public void WriteInt32Nullable(int? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteInt32(Value.Value);
    }
    public void WriteInt64(long Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteInt64Nullable(long? Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
        WriteInt64(Value.Value);
    }
    public void WriteDecimal(decimal Value)
    {
      // NOTE: there is no BitConverter for decimal.
      var Bits = Decimal.GetBits(Value);

      //Inv.Assert.Check(Bits.Length == 4, "Decimal bits expected to be 4.");

      var Buffer = new byte[16];
      var lo = Bits[0];
      var mid = Bits[1];
      var hi = Bits[2];
      var flags = Bits[3];

      Buffer[0] = (byte)lo;
      Buffer[1] = (byte)(lo >> 8);
      Buffer[2] = (byte)(lo >> 16);
      Buffer[3] = (byte)(lo >> 24);

      Buffer[4] = (byte)mid;
      Buffer[5] = (byte)(mid >> 8);
      Buffer[6] = (byte)(mid >> 16);
      Buffer[7] = (byte)(mid >> 24);

      Buffer[8] = (byte)hi;
      Buffer[9] = (byte)(hi >> 8);
      Buffer[10] = (byte)(hi >> 16);
      Buffer[11] = (byte)(hi >> 24);

      Buffer[12] = (byte)flags;
      Buffer[13] = (byte)(flags >> 8);
      Buffer[14] = (byte)(flags >> 16);
      Buffer[15] = (byte)(flags >> 24);

      WriteBuffer(Buffer);
    }
    public void WriteFloat(float Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteDouble(double Value)
    {
      WriteBuffer(BitConverter.GetBytes(Value));
    }
    public void WriteString(string Value)
    {
      WriteBoolean(Value == null);

      if (Value != null)
      {
        var Buffer = Encoding.UTF8.GetBytes(Value);

        WriteInt32(Buffer.Length);
        WriteBuffer(Buffer);
      }
    }
    public void WriteBinary(Inv.Binary Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteImage(Inv.Image Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteSound(Inv.Sound Value)
    {
      var Buffer = Value.GetBuffer();

      WriteString(Value.GetFormat());
      WriteInt32(Buffer.Length);
      WriteBuffer(Buffer);
    }
    public void WriteByteArray(byte[] Value)
    {
      WriteInt32(Value.Length);
      WriteBuffer(Value);
    }
    public void WriteInt32Array(int[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteInt32(Value[Index]);
    }
    public void WriteInt64Array(long[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteInt64(Value[Index]);
    }
    public void WriteStringArray(string[] Value)
    {
      WriteInt32(Value.Length);

      for (var Index = 0; Index < Value.Length; Index++)
        WriteString(Value[Index]);
    }
    public void WriteColour(Inv.Colour Value)
    {
      WriteInt32(Value.RawValue);
    }
    public Inv.TransportPacket ToPacket()
    {
      MemoryStream.Position = 0;
      WriteInt32((int)MemoryStream.Length);

      return new Inv.TransportPacket(MemoryStream.ToArray());
    }

    private void WriteBuffer(byte[] Buffer)
    {
      MemoryStream.Write(Buffer, 0, Buffer.Length);
    }

    private MemoryStream MemoryStream;
    private TransportProtocol Protocol;
  }

  public sealed class TransportReader : IDisposable
  {
    public TransportReader(Inv.TransportPacket Packet)
    {
      this.Stream = new MemoryStream(Packet.Buffer);

      var PacketLength = ReadInt32();
      if (PacketLength != Packet.Buffer.Length)
        throw new Exception("Packet length is invalid.");
    }
    public void Dispose()
    {
      this.Stream.Dispose();
    }

    public bool EndOfPacket
    {
      get { return Stream.Position >= Stream.Length; }
    }

    public void SetProtocol(TransportProtocol Protocol)
    {
      this.Protocol = Protocol;
    }
    public void ReadStream(Action<Stream> Action)
    {
      Action(Stream);
    }
    public T ReadDataObject<T>()
    {
      var Intercept = Protocol != null ? Protocol.GetIntercept<T>() : null;

      if (Intercept != null)
      {
        return (T)Intercept.ReaderFunc(this);
      }
      else
      {
        // TODO: inline data contract serializers not working?
        //using (var BinaryReader = XmlDictionaryReader.CreateBinaryReader(Stream, XmlDictionaryReaderQuotas.Max))
        //  return (T)new DataContractSerializer(typeof(T)).ReadObject(BinaryReader);

        var Result = default(T);

        var ReadBuffer = ReadByteArray();

        using (var MemoryStream = new MemoryStream(ReadBuffer))
        using (var BinaryReader = XmlDictionaryReader.CreateBinaryReader(MemoryStream, XmlDictionaryReaderQuotas.Max))
          Result = (T)new DataContractSerializer(typeof(T)).ReadObject(BinaryReader);

        return Result;
      }
    }
    public bool ReadBoolean()
    {
      return BitConverter.ToBoolean(ReadBuffer(1), 0);
    }
    public char ReadCharacter()
    {
      var Length = ReadInt32();
      var Buffer = ReadBuffer(Length);

      return BitConverter.ToChar(Buffer, 0);
    }
    public DateTimeOffset ReadDateTimeOffset()
    {
      var DateTimeBinary = ReadInt64();
      var OffsetTicks = ReadInt64();

      return new DateTimeOffset(DateTime.FromBinary(DateTimeBinary), TimeSpan.FromTicks(OffsetTicks));
    }
    public DateTimeOffset? ReadDateTimeOffsetNullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadDateTimeOffset();
    }
    public byte ReadUInt8()
    {
      return ReadBuffer(1)[0];
    }
    public ushort ReadUInt16()
    {
      return BitConverter.ToUInt16(ReadBuffer(2), 0);
    }
    public uint ReadUInt32()
    {
      return BitConverter.ToUInt32(ReadBuffer(4), 0);
    }
    public byte ReadByte()
    {
      return ReadBuffer(1)[0];
    }
    public int ReadInt32()
    {
      return BitConverter.ToInt32(ReadBuffer(4), 0);
    }
    public int? ReadInt32Nullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadInt32();
    }
    public long ReadInt64()
    {
      return BitConverter.ToInt64(ReadBuffer(8), 0);
    }
    public long? ReadInt64Nullable()
    {
      if (ReadBoolean())
        return null;
      else
        return ReadInt64();
    }
    public decimal ReadDecimal()
    {
      var Buffer = ReadBuffer(16);

      var lo = ((int)Buffer[0]) | ((int)Buffer[1] << 8) | ((int)Buffer[2] << 16) | ((int)Buffer[3] << 24);
      var mid = ((int)Buffer[4]) | ((int)Buffer[5] << 8) | ((int)Buffer[6] << 16) | ((int)Buffer[7] << 24);
      var hi = ((int)Buffer[8]) | ((int)Buffer[9] << 8) | ((int)Buffer[10] << 16) | ((int)Buffer[11] << 24);
      var flags = ((int)Buffer[12]) | ((int)Buffer[13] << 8) | ((int)Buffer[14] << 16) | ((int)Buffer[15] << 24);

      return new Decimal(new int[] { lo, mid, hi, flags });
    }
    public float ReadFloat()
    {
      return BitConverter.ToSingle(ReadBuffer(4), 0);
    }
    public double ReadDouble()
    {
      return BitConverter.ToDouble(ReadBuffer(4), 0);
    }
    public string ReadString()
    {
      if (ReadBoolean())
        return null;

      var Length = ReadInt32();
      var Buffer = ReadBuffer(Length);

      return Encoding.UTF8.GetString(Buffer, 0, Length);
    }
    public Guid ReadGuid()
    {
      return new Guid(ReadBuffer(16));
    }
    public Inv.Colour ReadColour()
    {
      return Inv.Colour.FromArgb(ReadInt32());
    }
    public Inv.Binary ReadBinary()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Binary(Bytes, Format);
    }
    public Inv.Image ReadImage()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Image(Bytes, Format);
    }
    public Inv.Sound ReadSound()
    {
      var Format = ReadString();
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return new Inv.Sound(Bytes, Format);
    }
    public byte[] ReadByteArray()
    {
      var Count = ReadInt32();
      var Bytes = ReadBuffer(Count);

      return Bytes;
    }
    public int[] ReadInt32Array()
    {
      var Count = ReadInt32();

      var Result = new int[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadInt32();

      return Result;
    }
    public long[] ReadInt64Array()
    {
      var Count = ReadInt32();

      var Result = new long[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadInt64();

      return Result;
    }
    public string[] ReadStringArray()
    {
      var Count = ReadInt32();

      var Result = new string[Count];

      for (var Index = 0; Index < Count; Index++)
        Result[Index] = ReadString();

      return Result;
    }

    private byte[] ReadBuffer(int Length)
    {
      var Result = new byte[Length];

      Stream.Read(Result, 0, Length);

      return Result;
    }

    private MemoryStream Stream;
    private TransportProtocol Protocol;
  }
}