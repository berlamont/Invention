﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public static class Assert
  {
    static Assert()
    {
      // Assertions are enabled by default for DEBUG mode and NUGET packages.
#if DEBUG || NUGET
      IsEnabled = true;
#else
      IsEnabled = false;
#endif
    }

    public static bool IsEnabled { get; private set; }

    public static void Enable()
    {
      IsEnabled = true;
    }
    public static void Disable()
    {
      IsEnabled = false;
    }

    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Check(bool Condition, string Message)
    {
      if (IsEnabled && !Condition)
        throw new Exception(Message);
    }
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Check(bool Condition, string Format, params object[] ArgumentArray)
    {
      if (IsEnabled && !Condition)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, Format, ArgumentArray));
    }
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Fail(string Message)
    {
      if (IsEnabled)
        throw new Exception(Message);
    }
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CheckNotNull(object ArgumentObject, string ArgumentName)
    {
      if (IsEnabled && ArgumentObject == null)
        throw new ArgumentNullException(ArgumentName);
    }
  }
}
