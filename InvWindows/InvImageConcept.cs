﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Windows.Media;
using System.Windows;
using Inv.Support;

namespace Inv
{
  public interface ImageConcept
  {
    ImageSource Access(int Width, int Height);
  }

  public struct ImageSize
  {
    public ImageSize(int Width, int Height)
      : this()
    {
      this.Width = Width;
      this.Height = Height;
    }

    public int Width { get; private set; }
    public int Height { get; private set; }
  }

  internal sealed class MemoryImageConcept : ImageConcept
  {
    internal MemoryImageConcept(Inv.Image Image)
    {
      this.Image = Image;
    }

    System.Windows.Media.ImageSource Inv.ImageConcept.Access(int Width, int Height)
    {
      return Image.ConvertToMediaBitmapAndFreeze(Width, Height);
    }

    private Inv.Image Image;
  }

  public sealed class VectorImageConcept : ImageConcept
  {
    internal VectorImageConcept(ResourceCrate ResourceCrate, string VectorName, string VectorPath, Brush FillBrush, Pen StrokePen)
    {
      Debug.Assert(FillBrush == null || FillBrush.IsFrozen, "FillBrush must be frozen.");
      Debug.Assert(StrokePen == null || StrokePen.IsFrozen, "StrokePen must be frozen.");

      if (FillBrush != null && !FillBrush.IsFrozen)
        FillBrush = (Brush)FillBrush.GetAsFrozen();

      if (StrokePen != null && !StrokePen.IsFrozen)
        StrokePen = (Pen)StrokePen.GetAsFrozen();

      this.ResourceCrate = ResourceCrate;
      this.VectorName = VectorName;
      this.VectorPath = VectorPath;
      this.FillBrush = FillBrush ?? Brushes.White;
      this.StrokePen = StrokePen ?? new Pen() { Brush = Brushes.White, Thickness = 1.0 }.FreezeIt();
    }

    public Pen StrokePen { get; private set; }
    public Brush FillBrush { get; private set; }

    ImageSource Inv.ImageConcept.Access(int Width, int Height)
    {
      return ResourceCrate.AccessVectorSource(VectorName, VectorPath, FillBrush, StrokePen);
    }

    private ResourceCrate ResourceCrate;
    private string VectorName;
    private string VectorPath;
  }

  public sealed class OverlayImageConcept : ImageConcept
  {
    public OverlayImageConcept()
    {
      this.LayerList = new DistinctList<OverlayImageLayer>();
    }

    public event Action ChangeEvent;

    public void RemoveClient()
    {
      LayerList.RemoveAll(Layer => Layer.Left == 0 && Layer.Right == 1.0 && Layer.Top == 0 && Layer.Bottom == 1.0);

      ChangeInvoke();
    }
    public void AddClient(Inv.ImageConcept Concept)
    {
      InsertLayer(0, Concept, 0, 0, 1.0, 1.0);
    }
    public void AddCustom(Inv.ImageConcept Concept, double Left, double Top, double Right, double Bottom)
    {
      AddLayer(Concept, Left, Top, Right, Bottom);
    }
    public void AddLeftHalf(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.0, 0.0, 0.5, 1.0);
    }
    public void AddRightHalf(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.5, 0.0, 1.0, 1.0);
    }
    public void AddTopHalf(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.0, 0.0, 1.0, 0.5);
    }
    public void AddBottomHalf(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.0, 0.5, 1.0, 1.0);
    }
    public void AddUpperLeftQuadrant(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.0, 0.0, 0.5, 0.5);
    }
    public void AddUpperRightQuadrant(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.5, 0.0, 1.0, 0.5);
    }
    public void AddLowerLeftQuadrant(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.0, 0.5, 0.5, 1.0);
    }
    public void AddLowerRightQuadrant(Inv.ImageConcept Concept)
    {
      AddLayer(Concept, 0.5, 0.5, 1.0, 1.0);
    }

    ImageSource Inv.ImageConcept.Access(int Width, int Height)
    {
      // shortcut for single client layer.
      if (LayerList.Count == 1)
      {
        var Layer = LayerList.Single();
        if (Layer.Left == 0.0 && Layer.Top == 0.0 && Layer.Right == 1.0 && Layer.Bottom == 1.0)
          return Layer.Concept.Access(Width, Height);
      }

      // otherwise, render the layers.
      var BitmapCanvas = new DrawingVisual();

      using (var BitmapContext = BitmapCanvas.RenderOpen())
      {
        foreach (var Layer in LayerList)
        {
          var Rectangle = new System.Windows.Rect(Width * Layer.Left, Height * Layer.Top, Width * (Layer.Right - Layer.Left), Height * (Layer.Bottom - Layer.Top));
          var Image = Layer.Concept.Access((int)Rectangle.Width, (int)Rectangle.Height);

          BitmapContext.DrawImage(Image, Rectangle);
        }
      }

      var Result = new RenderTargetBitmap(Width, Height, 96, 96, PixelFormats.Pbgra32);
      Result.Render(BitmapCanvas);
      Result.Freeze();

      return Result;
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
    private void InsertLayer(int Index, Inv.ImageConcept Concept, double Left, double Top, double Right, double Bottom)
    {
      Debug.Assert(Concept != null, "Concept must be specified.");
      Debug.Assert(new[] { Left, Right, Top, Bottom }.All(Bound => Bound >= 0.0 && Bound <= 1.0) && Left < Right && Top < Bottom, "Bounds must be valid.");

      LayerList.Insert(Index, new OverlayImageLayer(Concept, Left, Top, Right, Bottom));

      ChangeInvoke();
    }
    private void AddLayer(Inv.ImageConcept Concept, double Left, double Top, double Right, double Bottom)
    {
      InsertLayer(LayerList.Count, Concept, Left, Top, Right, Bottom);
    }

    private Inv.DistinctList<OverlayImageLayer> LayerList;
  }

  public sealed class ElementImageConcept : ImageConcept
  {
    public ElementImageConcept(FrameworkElement Element)
    {
      this.Element = Element;
    }

    ImageSource Inv.ImageConcept.Access(int Width, int Height)
    {
      return Element.ScaledRenderToBitmapSource(Width, Height);
    }

    private FrameworkElement Element;
  }

  internal sealed class OverlayImageLayer
  {
    internal OverlayImageLayer(Inv.ImageConcept Concept, double Left, double Top, double Right, double Bottom)
    {
      this.Concept = Concept;
      this.Left = Left;
      this.Top = Top;
      this.Right = Right;
      this.Bottom = Bottom;
    }

    public Inv.ImageConcept Concept { get; private set; }

    public double Left { get; private set; }
    public double Top { get; private set; }
    public double Right { get; private set; }
    public double Bottom { get; private set; }
  }

  internal sealed class BitmapSourceProxy : ImageConcept
  {
    internal BitmapSourceProxy(BitmapSource BitmapSource)
    {
      this.BitmapSource = BitmapSource;
    }

    ImageSource Inv.ImageConcept.Access(int Width, int Height)
    {
      return BitmapSource;
    }

    private BitmapSource BitmapSource;
  }
}
