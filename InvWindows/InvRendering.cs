﻿/*! 1 !*/
// #define ALLOW_UNSAFE

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Diagnostics;

// This file uses unsafe {}.

//
// This will probably be reworked into something that at least pretends to hide the fact that we're basically flinging byte arrays around.
// 
// Well, maybe. Anyone working on or calling into this code should probably be aware of basic fundamentals of computer image processing, convolution kernels,
// subpixel ordering, bitmap strides, and so on. Naming schemes for the effects are based on Photoshop's names, for convenience 
// (.. of those who have used Photoshop to edit photos, anyhow).
//
// Much of this code is performance-sensitive in the inner loops. Expect to see a proliferation of the usual performance optimisations (loop unrolls, pointer
// arithmetic instead of array indexing, etc), and possibly some of the lesser-seen ones at some point (calling out to a C++/CLI DLL so we can use SSE is
// somewhere in this code's future), since applying these effects on larger (> 256x256@32) images needs to be fast.
//
namespace Inv.Rendering
{
  public interface IAdvRenderable
  {
    BitmapSource Render();
    void ResampleIntoBoundingBox(int width, int height);
    void ConvertToGrayscale();

    int GetWidth();
    int GetHeight();
  }

  /// <summary>
  /// Interface representing image rendering code. Use CanUseThisFactory(), IsHardwareAccelerated() and IsNativeCode() to pick which
  /// of the available factories you wish to use; factories returning true to either of IsHardwareAcclerated() or IsNativeCode() should be used
  /// ahead of factories that return false to both to ensure the best performance.
  /// </summary>
  public interface IAdvRenderableFactory
  {
    /// <summary>
    /// True if this factory can be used on this system. May return false if the factory relies on a hardware (CPU or GPU) feature not present
    /// on this system.
    /// </summary>   
    bool CanUseThisFactory();

    /// <summary>
    /// True if this factory provides hardware-accelerated rendering on this system using a GPU.
    /// </summary>

    bool IsHardwareAcclerated();

    /// <summary>
    /// True if this factory is native code (ie, compiled C++, hand-rolled assembly, whatever) and can be expected to outperform CLR-based code.
    /// </summary>
    bool IsNativeCode();

    IAdvRenderable RenderableFromImage(BitmapSource image);

    IAdvRenderable RenderableFromHBitmap(IntPtr hBitmap);
  }

  public enum LinearKernelDirection
  {
    Horizontal,
    Vertical,
    BothSeparable,
    BothInseparable
  }

  public enum LayerBlendModes
  {
    Normal,
    Subtract,
    Multiply,
    Screen,
    Additive,
    Difference
  }

  public enum ResampleOptions
  {
    Fast,
    Slow
  }

  internal class ClrRenderableFactory : IAdvRenderableFactory
  {
    internal ClrRenderableFactory()
    {
#if !ALLOW_UNSAFE
      Debug.WriteLine("Using WPF for image resizing");
#endif
    }

    IAdvRenderable IAdvRenderableFactory.RenderableFromImage(BitmapSource image)
    {
#if ALLOW_UNSAFE
      return new ClrRenderable(image);
#else
      return new PassthroughRenderable(image);
#endif
    }

    IAdvRenderable IAdvRenderableFactory.RenderableFromHBitmap(IntPtr hBitmap)
    {
      return new PassthroughRenderable(hBitmap);
    }

    bool IAdvRenderableFactory.CanUseThisFactory()
    {
      return true;
    }

    bool IAdvRenderableFactory.IsHardwareAcclerated()
    {
      return false;
    }

    bool IAdvRenderableFactory.IsNativeCode()
    {
      return false;
    }
  }

  internal class PassthroughRenderable : IAdvRenderable
  {
    private BitmapSource _image;
    internal PassthroughRenderable(BitmapSource image)
    {
      _image = image;
    }

    internal PassthroughRenderable(IntPtr hBitmap)
    {
      _image = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
    }

    BitmapSource IAdvRenderable.Render()
    {
      var frozen = _image.Clone();
      
      frozen.Freeze();
      return frozen;
    }

    void IAdvRenderable.ResampleIntoBoundingBox(int width, int height)
    {
      double aspectRatio = (double)_image.PixelWidth / (double)_image.PixelHeight;

      double widthRatio = (double)width / (double)_image.PixelWidth;
      double heightRatio = (double)height / (double)_image.PixelHeight;
      double factor;
      if (widthRatio > heightRatio)
      {
        factor = widthRatio;
      }
      else
      {
        factor = heightRatio;
      }

      _image = new TransformedBitmap(_image, new ScaleTransform(factor, factor));
    }

    void IAdvRenderable.ConvertToGrayscale()
    {
      var ConvertedBitmap = new FormatConvertedBitmap();
      ConvertedBitmap.BeginInit();
      ConvertedBitmap.Source = _image;
      ConvertedBitmap.DestinationFormat = PixelFormats.Gray32Float;
      ConvertedBitmap.EndInit();
      ConvertedBitmap.Freeze();

      _image = ConvertedBitmap;
    }

    int IAdvRenderable.GetWidth()
    {
      return _image.PixelWidth;
    }

    int IAdvRenderable.GetHeight()
    {
      return _image.PixelHeight;
    }
  }

  public class RenderableFactoryFilter : IAdvRenderableFactory
  {
    internal RenderableFactoryFilter(IAdvRenderableFactory NativeFactory)
    {
      this.NativeFactory = NativeFactory;
      this.ClrFactory = new ClrRenderableFactory();
    }

    bool IAdvRenderableFactory.CanUseThisFactory()
    {
      return true;
    }

    bool IAdvRenderableFactory.IsHardwareAcclerated()
    {
      if (this.NativeFactory != null)
        return this.NativeFactory.IsHardwareAcclerated();

      return false;
    }

    bool IAdvRenderableFactory.IsNativeCode()
    {
      if (this.NativeFactory != null)
        return this.NativeFactory.IsNativeCode();

      return false;
    }

    // For performance reasons, don't use the high quality renderer for images over 512x512 pixels in size
    // as it requires quite a bit of RAM which may not always be readily available in a 32-bit process.
    IAdvRenderable IAdvRenderableFactory.RenderableFromImage(BitmapSource image)
    {
      if (this.NativeFactory != null && image.PixelWidth * image.PixelHeight < (512 * 512))
        return NativeFactory.RenderableFromImage(image);

      return ClrFactory.RenderableFromImage(image);
    }

    IAdvRenderable IAdvRenderableFactory.RenderableFromHBitmap(IntPtr hBitmap)
    {
      if (this.NativeFactory != null)
        return this.NativeFactory.RenderableFromHBitmap(hBitmap);

      return ClrFactory.RenderableFromHBitmap(hBitmap);
    }

    IAdvRenderableFactory NativeFactory;
    IAdvRenderableFactory ClrFactory;
  }

  public static class RenderableFactory
  {
    static IAdvRenderableFactory _factory;

    static RenderableFactory()
    {
      IAdvRenderableFactory Factory = null;
      var AssemblyPath = "";

      try
      {
        // Try to load a native renderable factory.
        var RenderingAssemblyName = "Kestral.AdvRendering." + (Environment.Is64BitProcess ? "x64" : "x86") + ".dll";

        // Try loading from embedded resources first.
        var thisAssembly = typeof(RenderableFactory).Assembly;
        var relevantResources = thisAssembly.GetManifestResourceNames().Where(x => x.Contains(RenderingAssemblyName)).FirstOrDefault();
        if (relevantResources != null)
        {
          var embeddedDllStream = thisAssembly.GetManifestResourceStream(relevantResources);
          var embeddedDll = new byte[embeddedDllStream.Length];
          embeddedDllStream.Read(embeddedDll, 0, (int)embeddedDllStream.Length);

          var tempDllName = Path.GetTempFileName();
          File.WriteAllBytes(tempDllName, embeddedDll);

          var embeddedAssembly = Assembly.LoadFrom(tempDllName);

          // Flag the dll to be deleted on restart so we don't clutter up the temp folder any more than usual.
          Win32.Kernel32.MoveFileEx(tempDllName, null, Win32.Kernel32.MoveFileFlags.MOVEFILE_DELAY_UNTIL_REBOOT);

          var FactoryType = embeddedAssembly.GetType("Inv.Rendering.Native.NativeRenderableFactory");
          Factory = Activator.CreateInstance(FactoryType) as IAdvRenderableFactory;
          AssemblyPath = thisAssembly.CodeBase;
        }
        else
        {
          // Look for a file.
          var codeBase = thisAssembly.CodeBase;
          var uri = new UriBuilder(codeBase);
          var path = Uri.UnescapeDataString(uri.Path);
          AssemblyPath = Path.Combine(Path.GetDirectoryName(path), RenderingAssemblyName);

          if (!File.Exists(AssemblyPath))
            AssemblyPath = Path.Combine(Path.GetDirectoryName(path), "Kestral.AdvRendering.dll");

          if (File.Exists(AssemblyPath))
          {
            var assemblyName = AssemblyName.GetAssemblyName(AssemblyPath);
            // Make sure it's signed with the same key as this assembly (or both are unsigned, in case of internal development)
            var assemblyToken = assemblyName.GetPublicKeyToken();
            var ourToken = thisAssembly.GetName().GetPublicKeyToken();

            if ((assemblyToken != null && ourToken != null) ||
                ourToken == null)
            {
              var tokensMatch = true;

              if (assemblyToken != null)
                for (var i = 0; i < assemblyToken.Length; i++)
                  tokensMatch &= assemblyToken[i] == ourToken[i];

              if (tokensMatch)
              {
                var NativeAssembly = Assembly.LoadFrom(AssemblyPath);
                var FactoryType = NativeAssembly.GetType("Inv.Rendering.Native.NativeRenderableFactory");
                Factory = Activator.CreateInstance(FactoryType) as IAdvRenderableFactory;
              }
              else
              {
                Debug.WriteLine("Warning: {0} exists but is not signed with the same key as {1}. Refusing to load it.", AssemblyPath, Path.GetFileName(path));
              }
            }
            else
            {
              Debug.WriteLine("Warning: {0} exists but is not signed and the loading library is. Refusing to load it.", AssemblyPath);
            }
          }
        }
      }
      catch (Exception)
      {

      }

      if (Factory != null && Factory.CanUseThisFactory())
      {
        Debug.WriteLine(string.Format("Using native code in {0} for image resizing.", AssemblyPath));
        _factory = new RenderableFactoryFilter(Factory);
        return;
      }

      Debug.WriteLine("Using IL code for image resizing (this will be slower)");
      _factory = new ClrRenderableFactory();
    }

    public static IAdvRenderable RenderableFromImage(BitmapSource image)
    {
      return _factory.RenderableFromImage(image);
    }

    public static IAdvRenderable RenderableFromHBitmap(IntPtr hBitmap)
    {
      return _factory.RenderableFromHBitmap(hBitmap);
    }
  }
}
