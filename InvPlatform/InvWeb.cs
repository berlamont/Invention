﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;
using System.IO;

namespace Inv
{
  public sealed class Web
  {
    internal Web(Application Application)
    {
      this.Application = Application;
    }

    public void Launch(Uri Uri)
    {
      Application.Platform.WebLaunchUri(Uri);
    }
    public WebDownload GetDownload(Uri Uri)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(Uri);
      RemoteFileRequest.Method = "GET";

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public WebClient NewClient(string Host, int Port, byte[] CertHash)
    {
      return new WebClient(this, Host, Port, CertHash);
    }
    public WebServer NewServer(string Host, int Port, byte[] CertHash)
    {
      return new WebServer(this, Host, Port, CertHash);
    }
    public WebBroker NewBroker(string BaseUri)
    {
      return new WebBroker(BaseUri);
    }
    public WebJson NewJson()
    {
      return new WebJson();
    }

    internal Application Application { get; private set; }
  }

  public sealed class WebDownload : IDisposable
  {
    internal WebDownload(long Length, System.IO.Stream Stream)
    {
      this.Length = Length;
      this.Stream = Stream;
    }
    public void Dispose()
    {
      Stream.Dispose();
    }

    public long Length { get; private set; }
    public System.IO.Stream Stream { get; private set; }
  }

  public sealed class WebProtocol
  {
    internal WebProtocol(Inv.Persist.Governor Governor)
    {
      this.Governor = Governor;
    }

    public byte[] Encode<TData>(TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        Governor.Save(Data, MemoryStream);

        MemoryStream.Flush();

        return MemoryStream.ToArray();
      }
    }
    public TData Decode<TData>(byte[] DataBuffer)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        return (TData)Governor.Load(MemoryStream);
    }
    public void Decode<TData>(byte[] DataBuffer, TData Data)
    {
      using (var MemoryStream = new System.IO.MemoryStream(DataBuffer))
        Governor.Load(Data, MemoryStream);
    }

    private Inv.Persist.Governor Governor;
  }

  internal enum WebContent
  {
    TextPlain,
    TextCsv,
    ApplicationJson
  }

  public sealed class WebClient
  {
    internal WebClient(Web Web, string Host, int Port, byte[] CertHash)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
    }

    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebClientConnect(this);
        this.IsActive = true;
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebClientDisconnect(this);
      }
    }

    public Web Web { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public byte[] CertHash { get; private set; }
    public Stream InputStream { get; private set; }
    public Stream OutputStream { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    private bool IsActive;
  }

  public sealed class WebServer
  {
    internal WebServer(Web Web, string Host, int Port, byte[] CertHash)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
      this.ChannelDictionary = new Dictionary<object, WebChannel>();
    }

    public event Action<WebChannel> AcceptEvent;
    public event Action<WebChannel> RejectEvent;

    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebServerConnect(this);
        this.IsActive = true;
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebServerDisconnect(this);
      }
    }

    public Web Web { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public byte[] CertHash { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }
    internal Action<object> DropDelegate { get; set; }

    internal void AcceptChannel(object Node, System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      var Result = new WebChannel(this);

      lock (ChannelDictionary)
      {
        Result.Node = Node;
        Result.SetStreams(InputStream, OutputStream);

        ChannelDictionary.Add(Node, Result);
      }

      AcceptInvoke(Result);
    }
    internal void RejectChannel(object Node)
    {
      WebChannel Result;

      lock (ChannelDictionary)
      {
        Result = ChannelDictionary.GetValueOrDefault(Node);

        if (Result != null)
        {
          ChannelDictionary.Remove(Node);

          Result.SetStreams(null, null);
        }
      }

      if (Result != null)
        RejectInvoke(Result);
    }
    internal void DropChannel(WebChannel WebChannel)
    {
      if (DropDelegate != null)
        DropDelegate(WebChannel.Node);
    }

    private void AcceptInvoke(WebChannel WebChannel)
    {
      if (AcceptEvent != null)
        AcceptEvent(WebChannel);
    }
    private void RejectInvoke(WebChannel WebChannel)
    {
      if (RejectEvent != null)
        RejectEvent(WebChannel);
    }

    private bool IsActive;
    private Dictionary<object, WebChannel> ChannelDictionary;
  }

  public sealed class WebChannel
  {
    internal WebChannel(WebServer WebServer)
    {
      this.WebServer = WebServer;
    }

    public Stream InputStream { get; private set; }
    public Stream OutputStream { get; private set; }

    public void Drop()
    {
      WebServer.DropChannel(this);
    }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    private WebServer WebServer;
  }

  public sealed class WebBroker
  {
    public WebBroker(string BaseUri)
    {
      this.BaseUri = BaseUri;
      this.HeaderDictionary = new Dictionary<string,string>();
      this.Json = new WebJson();

      //System.Net.WebRequest.DefaultWebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    public Dictionary<string, string> HeaderDictionary { get; private set; }

    public WebDownload GetDownload(string QueryText)
    {
      var RemoteFileRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      RemoteFileRequest.Method = "GET";
      ApplyHeaders(RemoteFileRequest);

      var Response = RemoteFileRequest.GetResponse();

      return new WebDownload(Response.ContentLength, Response.GetResponseStream());
    }
    public string GetPlainText(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";
      ApplyHeaders(WebRequest);

      using (var WebResponse = WebRequest.GetResponse())
      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
        return StreamReader.ReadToEnd().Trim();
    }
    public IEnumerable<string> GetPlainTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "GET";
      ApplyHeaders(WebRequest);

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      {
        while (!ResponseReader.EndOfStream)
          yield return ResponseReader.ReadLine();
      }
    }
    public IEnumerable<string[]> GetCsvTextLines(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "GET";
      ApplyHeaders(WebRequest);

      using (var Response = WebRequest.GetResponse())
      using (var ResponseStream = Response.GetResponseStream())
      using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
      using (var CsvReader = new Inv.CsvReader(ResponseReader))
      {
        while (!ResponseReader.EndOfStream)
          yield return CsvReader.ReadRecord().ToArray();
      }
    }
    public void PostCsvTextLine(string QueryText, string[] Text)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextCsv);
      WebRequest.Method = "POST";
      ApplyHeaders(WebRequest);

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
      using (var CsvWriter = new Inv.CsvWriter(RequestWriter))
        CsvWriter.WriteRecord(Text);

      WebRequest.GetResponse().Dispose();
    }
    public void PostPlainTextLine(string QueryText, string Line)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(new Uri(BaseUri + QueryText));
      WebRequest.ContentType = WebContentType(WebContent.TextPlain);
      WebRequest.Method = "POST";
      ApplyHeaders(WebRequest);

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.WriteLine(Line);

      WebRequest.GetResponse().Dispose();
    }
    public T GetTextJsonObject<T>(string QueryText)
    {
      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.ContentType = WebContentType(WebContent.ApplicationJson);
      WebRequest.Method = "GET";
      ApplyHeaders(WebRequest);

      return ReadObject<T>(WebRequest);
    }
    public void PostTextJsonObject(string QueryText, object Request)
    {
      PostTextJsonObject<object>(QueryText, Request);
    }
    public TResponse PostTextJsonObject<TResponse>(string QueryText, object Request)
    {
      var RequestText = Json.Save(Request);
      //Debug.WriteLine(RequestText);

      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.Method = "POST";
      WebRequest.ContentType = WebContentType(WebContent.ApplicationJson);
      ApplyHeaders(WebRequest);
      // TODO: ContentLength?

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.Write(RequestText);

      return ReadObject<TResponse>(WebRequest);
    }
    public Newtonsoft.Json.Linq.JArray PostTextJsonObjectAsJArray(string QueryText, object Request)
    {
      var RequestText = Json.Save(Request);
      //Debug.WriteLine(RequestText);

      var WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(BaseUri + QueryText);
      WebRequest.Method = "POST";
      WebRequest.ContentType = WebContentType(WebContent.ApplicationJson);
      ApplyHeaders(WebRequest);
      // TODO: ContentLength?

      using (var RequestStream = WebRequest.GetRequestStream())
      using (var RequestWriter = new System.IO.StreamWriter(RequestStream))
        RequestWriter.Write(RequestText);

      var ResponseText = GetResponseText(WebRequest);

      return Newtonsoft.Json.Linq.JArray.Parse(ResponseText);
    }

    private void ApplyHeaders(System.Net.HttpWebRequest WebRequest)
    {
      foreach (var HeaderKeyValue in HeaderDictionary)
        WebRequest.Headers[HeaderKeyValue.Key] = HeaderKeyValue.Value;
    }
    private string GetResponseText(System.Net.HttpWebRequest WebRequest)
    {
      System.Net.HttpWebResponse WebResponse;
      bool WebFailed;
      try
      {
        WebResponse = (System.Net.HttpWebResponse)WebRequest.GetResponse();
        WebFailed = false;
      }
      catch (System.Net.WebException WebException)
      {
        WebResponse = (System.Net.HttpWebResponse)WebException.Response;
        WebFailed = true;
      }
      catch (System.ObjectDisposedException)
      {
        WebResponse = null;
        WebFailed = true;
      }
      /*
      catch (Exception Exception)
      {
        if (Exception.Message == "The service is unavailable.") // thanks Azure!
        {
          WebResponse = null;
          WebFailed = true;
        }
        else
        {
          throw;
        }
      }*/

      if (WebResponse == null)
        return null;

      using (var StreamReader = new System.IO.StreamReader(WebResponse.GetResponseStream()))
      {
        var ResponseText = StreamReader.ReadToEnd();

        if (WebFailed)
        {
          throw new Exception(ResponseText);
          //Debug.WriteLine(ResponseText);
          //return default(T);
        }

        return ResponseText;
      }
    }
    private T ReadObject<T>(System.Net.HttpWebRequest WebRequest)
    {
      var ResponseText = GetResponseText(WebRequest);

      if (string.IsNullOrEmpty(ResponseText))
        return default(T);

      try
      {
        return Deserialize<T>(ResponseText);
      }
      catch (Exception Exception)
      {
        throw new AggregateException("Failed to deserialize: " + ResponseText, Exception);
      }
    }
    private T Deserialize<T>(string Text)
    {
      //Debug.WriteLine(Text);

      try
      {
        var Result = Json.Load<T>(Text);

        //Debug.WriteLine(Result);

        return Result;
      }
      catch (Newtonsoft.Json.JsonReaderException)
      {
#if DEBUG
        throw;
#else

        return default(T);
#endif
      }
    }
    private string WebContentType(WebContent Content)
    {
      switch (Content)
      {
        case WebContent.TextCsv:
          return "text/csv";

        case WebContent.TextPlain:
          return "text/plain";

        case WebContent.ApplicationJson:
          return "application/json";

        default:
          throw new Exception("HttpContent not handled: " + Content);
      }
    }

    private string BaseUri;
    private WebJson Json;
  }

  public sealed class WebJson
  {
    internal WebJson()
    {
      this.IgnoreErrors = true;
    }

    public bool IgnoreErrors { get; set; }

    public T Load<T>(string Text)
    {
      try
      {
        var Result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Text, WebJsonHelper.GetBaseSetting());

        return Result;
      }
      catch (Newtonsoft.Json.JsonReaderException)
      {
        if (!IgnoreErrors)
          throw;

        return default(T);
      }
    }
    public string Save<T>(T ObjectValue)
    {
      return Save((object)ObjectValue);
    }
    public string Save(object ObjectValue)
    {
      try
      {        
        var Result = Newtonsoft.Json.JsonConvert.SerializeObject(ObjectValue, WebJsonHelper.GetBaseSetting());

        return Result;
      }
      catch (Newtonsoft.Json.JsonWriterException)
      {
        if (!IgnoreErrors)
          throw;

        return string.Empty;
      }
    }
  }

  public static class WebJsonHelper
  {
    static WebJsonHelper()
    {
      InvConverter = new WebJsonInvConverter();
      All = new List<Newtonsoft.Json.JsonConverter>() { InvConverter };
    }

    public static readonly WebJsonInvConverter InvConverter;
    public static readonly IList<Newtonsoft.Json.JsonConverter> All;

    public static Newtonsoft.Json.JsonSerializerSettings GetBaseSetting()
    {
      var Settings = new Newtonsoft.Json.JsonSerializerSettings()
      {
        Converters = WebJsonHelper.All,
        Formatting = Newtonsoft.Json.Formatting.Indented,
      };

      return Settings;
    }
  }

  public class WebJsonInvConverter : Newtonsoft.Json.JsonConverter
  {
    public override void WriteJson(Newtonsoft.Json.JsonWriter Writer, object Value, Newtonsoft.Json.JsonSerializer Serializer)
    {
      if (Value is Inv.Colour)
      {
        var DateValue = (Inv.Colour)Value;

        Writer.WriteValue(DateValue.GetArgb());
      }
      else if (Value is Inv.Date)
      {
        var DateValue = (Inv.Date)Value;

        Writer.WriteValue(DateValue.ToDateTime());
      }
      else if (Value is Inv.Time)
      {
        var TimeValue = (Inv.Time)Value;

        Writer.WriteValue(TimeValue.ToDateTime());
      }
      else
      {
        Writer.WriteValue(Value);
      }
    }
    public override object ReadJson(Newtonsoft.Json.JsonReader Reader, Type ObjectType, object ExistingValue, Newtonsoft.Json.JsonSerializer Serializer)
    {
      if (Reader.ValueType == typeof(int) && ObjectType == (typeof(Inv.Colour)))
      {
        return Inv.Colour.FromArgb((int)Reader.Value);
      }
      else if (Reader.ValueType == typeof(DateTime))
      {
        var DateTimeValue = (DateTime)Reader.Value;

        if (ObjectType == (typeof(Inv.Time)))
          return new Inv.Time(DateTimeValue);
        else if (ObjectType == (typeof(Inv.Date)))
          return new Inv.Date(DateTimeValue);

        throw new Exception("Invalid JSON.");
      }
      else
      {
        throw new Exception("Invalid JSON.");
      }
    }
    public override bool CanConvert(Type ObjectType)
    {
      return ObjectType == typeof(Inv.Date) || ObjectType == typeof(Inv.Time) || ObjectType == typeof(Inv.Colour);
    }
  }
}